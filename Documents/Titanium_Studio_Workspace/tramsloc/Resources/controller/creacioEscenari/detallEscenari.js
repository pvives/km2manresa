var win = Ti.UI.currentWindow;

var widthpantalla = Ti.Platform.displayCaps.platformWidth;
var heightpantalla = Ti.Platform.displayCaps.platformHeight;
var formulaWidth = widthpantalla / 320;
var formulaHeight = heightpantalla / 480;
var statusBarHeight = Ti.Platform.displayCaps.logicalDensityFactor * 25;
var escenariDescarregat = false;



	
	
//idiomes
if (Titanium.App.Properties.getString("language", "undefined") == 'ca') {
	Titanium.include('/controller/translations.js');
} else if (Titanium.App.Properties.getString("language", "undefined") == 'es') {
	Titanium.include('/controller/translations_es.js');
} else if (Titanium.App.Properties.getString("language", "undefined") == 'en') {
	Titanium.include('/controller/translations_en.js');
} else if (Titanium.App.Properties.getString("language", "undefined") == 'pt') {
	Titanium.include('/controller/translations_pt.js');
} else if (Titanium.App.Properties.getString("language", "undefined") == 'pt-PT') {
	Titanium.include('/controller/translations_pt.js');
} else if (Titanium.App.Properties.getString("language", "undefined") == 'eus') {
	Titanium.include('/controller/translations_eus.js');
} else {
	Titanium.include('/controller/translations_en.js');
}

if (Titanium.Platform.osname === 'android') {
	var isAndroid = true;
}
var statusBarHeight = isAndroid ? Ti.Platform.displayCaps.logicalDensityFactor * 25 : 20;

var customFont = 'Anna Beta';
 var url = win.urlBase + '/api/objectes/' + win.id+'/es/'+Ti.App.entorn;
// var url = win.urlBase + '/mobile/objectes/' + win.id;

Titanium.Geolocation.getCurrentPosition(function(e) {
	if (e.error) {
		Titanium.UI.createAlertDialog({
			title : alerta_current_location_title,
			message : alerta_current_location_message
		}).show();
	} else {
		Ti.App.latitude = e.coords.latitude;
		Ti.App.longitude = e.coords.longitude;
		url += e.coords.latitude + "/" + e.coords.longitude + "/0/" + Ti.App.maxAnotacions;
	}
	});
	
win.addEventListener("android:back", function() {
	win.close();
});

//comprovo si lescenari esta descarregat
var posicioEscenariDescarregat = -1;
var arrayObjectesEscenarisOffline = Titanium.App.Properties.getList("arrayObjectesEscenarisOffline", []);
var arrayEscenarisOffline = Titanium.App.Properties.getList("arrayEscenarisOffline", []);

//creacio fitxa detall

var detallView = Ti.UI.createView({
	width : 320 * formulaWidth,
	zIndex : 3,
	height : isAndroid ? 480 * formulaHeight - statusBarHeight : 480 * formulaHeight - statusBarHeight,
	top : 0 * formulaHeight,
	backgroundColor : '#3e3d40',
	layout : 'vertical',
	left : 0 * formulaWidth
});
win.add(detallView);

//barra superior
var barraSuperior = Titanium.UI.createView({
	// backgroundImage : '/images/topbar.png',
	backgroundColor : '#353535',
	top : 0 * formulaHeight,
	height : isAndroid ? 44 * formulaHeight : 44,
	zIndex : 2,
	width : 320 * formulaWidth,
	left : 0 * formulaWidth
});
detallView.add(barraSuperior);

var backButton = Ti.UI.createButton({
	width : 44 * formulaWidth,
	height : isAndroid ? 44 * formulaHeight : 44,
	zIndex : 2,
	left : 0 * formulaWidth,
	top : 0 * formulaHeight,
	backgroundImage : '/images/back.png'

});
barraSuperior.add(backButton);

backButton.addEventListener('click', function(e) {

	var animation = Ti.UI.createAnimation({
		left : 320 * formulaWidth,
		duration : 1000
	});

	win.close(animation);
});

//boto descarregar
var botoDescarregar = require('/controller/creacioEscenari/botoDescarregarEscenari');
var botoDescarregarInstance = new botoDescarregar(escenariDescarregat, url, arrayObjectesEscenarisOffline, arrayEscenarisOffline, detallView, downloaderWindowInstance, win.urlBase, win);
barraSuperior.add(botoDescarregarInstance);

if (!isAndroid) {
	var botoFB = require('/controller/creacioEscenari/botoFB');
	var botoFBInstance = new botoFB(win);
	barraSuperior.add(botoFBInstance);

	var botoTwitter = require('/controller/creacioEscenari/botoTwitter');
	var botoTwitterInstance = new botoTwitter(win);
	barraSuperior.add(botoTwitterInstance);
} else {
	var botoCompartir = require('/controller/creacioEscenari/botoCompartir');
	var botoCompartirInstance = new botoCompartir(win);

	barraSuperior.add(botoCompartirInstance);
}

var downloaderWindow = require('/controller/offline/DownloaderWindow');
var downloaderWindowInstance = new downloaderWindow(carregant);

for (var i = 0; i < arrayEscenarisOffline.length && !escenariDescarregat; i++) {
	var escenarioffline = JSON.parse(arrayEscenarisOffline[i]);

	if (win.escenariJSON.id == escenarioffline.id) {

		escenariDescarregat = true;
		posicioEscenariDescarregat = i;
	}

}

//boto obrir + info view
var infoEscenari = require('/controller/creacioEscenari/infoDetallEscenari');
var infoEscenariInstance = new infoEscenari(escenariDescarregat, win, url, detallView, downloaderWindowInstance, posicioEscenariDescarregat, arrayObjectesEscenarisOffline);
detallView.add(infoEscenariInstance);

var liniaSeparacio = Titanium.UI.createView({
	backgroundColor : Ti.App.colorPrincipal,
	top : 10 * formulaHeight,
	height : isAndroid ? 1 * formulaHeight : 1,
	zIndex : 2,
	width : 280 * formulaWidth,
	left : 20 * formulaWidth
});
detallView.add(liniaSeparacio);



//descripcio
var scrollView = Ti.UI.createScrollView({
	width : 320 * formulaWidth,
height:160* formulaHeight,
	layout : 'vertical',
	top : 5 * formulaHeight,
	 backgroundColor : '#3e3d40',

	left : 0 * formulaWidth,
	contentHeight : Ti.UI.SIZE
});
if (isAndroid) {
	scrollView.height = isAndroid ? 480 * formulaHeight - statusBarHeight - 44 * formulaHeight - infoEscenariInstance.size.height - 25 : 480 * formulaHeight - statusBarHeight - 44 - infoEscenariInstance.size.height - 20;

	detallView.add(scrollView);

} else {
	infoEscenariInstance.addEventListener("postlayout", function(e) {
		scrollView.height = isAndroid ? 480 * formulaHeight - statusBarHeight - 44 * formulaHeight - infoEscenariInstance.size.height - 20 : 480 * formulaHeight - statusBarHeight - 44 - infoEscenariInstance.size.height-25;

		detallView.add(scrollView);

	});
}

var imatgeEscenari = Ti.UI.createImageView({
	top : 10 * formulaHeight,

	width : 280 * formulaWidth,
	height : isAndroid?140 * formulaHeight:140,
	left : 20 * formulaWidth,
	backgroundColor : '#3e3d40'

});



if (win.imatge != null) {

	imatgeEscenari.image = win.urlBase + win.imatge;
} else {
	imatgeEscenari.image = '/images/imagendefectoficha.png';
}
scrollView.add(imatgeEscenari);

if (win.escenariJSON.descripcio != null) {
	//descripcio
	var labelDescripcio = Ti.UI.createLabel({
		text : win.escenariJSON.descripcio,
		top : 4 * formulaHeight,
		left : 20 * formulaWidth,
		height : Ti.UI.SIZE,
		color : '#cccccb',
		textAlign : 'left',
		width : 280 * formulaWidth,
		font : {
			fontSize :  14+Ti.App.paddingLabels+'sp'
		}
	});

	scrollView.add(labelDescripcio);
}
var liniaSeparacio2 = Titanium.UI.createView({
	backgroundColor : Ti.App.colorPrincipal,
	top : 10 * formulaHeight,
	height : isAndroid ? 1 * formulaHeight : 1,
	zIndex : 2,
	width : 280 * formulaWidth,
	left : 20 * formulaWidth
});
scrollView.add(liniaSeparacio2);

var viewActualitzacio = Titanium.UI.createView({

	top : 10 * formulaHeight,
	height : Ti.UI.SIZE,
	zIndex : 2,
	layout : 'horizontal',
	width : 280 * formulaWidth,
	left : 20 * formulaWidth
});
scrollView.add(viewActualitzacio);

var dataActualitzacio = win.escenariJSON.modified.date;
dataActualitzacio = dataActualitzacio.split(" ");
dataActualitzacio = dataActualitzacio[0];
dataActualitzacio = dataActualitzacio.split("-");

var labelActualitzacio = Ti.UI.createLabel({
	text : ultima_actualitzacio_escenari + ': ',
	left : 0 * formulaWidth,
	verticalAlign : Ti.UI.TEXT_VERTICAL_ALIGNMENT_TOP,
	height : Ti.UI.SIZE,
	width : Ti.UI.SIZE,

	color : Ti.App.colorPrincipal,
	//backgroundColor : 'green',
	font : {
		fontSize : 15+Ti.App.paddingLabels+'sp' ,
	},
	top : 0
});
viewActualitzacio.add(labelActualitzacio);

var labelActualitzacioInfo = Ti.UI.createLabel({
	text : dataActualitzacio[2] + "\/" + dataActualitzacio[1] + "\/" + dataActualitzacio[0],
	left : 3 * formulaWidth,
	verticalAlign : Ti.UI.TEXT_VERTICAL_ALIGNMENT_TOP,
	height : Ti.UI.SIZE,
	width : Ti.UI.SIZE,

	color :'#cccccb',
	//backgroundColor : 'green',
	font : {
		fontSize :  15+Ti.App.paddingLabels+'sp' ,
	},
	top : 0
});
viewActualitzacio.add(labelActualitzacioInfo);

if (win.escenariJSON.ambit != null) {

	var viewambitGeografic = Titanium.UI.createView({

		top : 0 * formulaHeight,
		height : Ti.UI.SIZE,
		zIndex : 2,
		layout : 'horizontal',
		width : 280 * formulaWidth,
		left : 20 * formulaWidth
	});
	scrollView.add(viewambitGeografic);

	var ambitGeografic = Ti.UI.createLabel({
		text : ambit_geografic + ': ',
		left : 0 * formulaWidth,
		verticalAlign : Ti.UI.TEXT_VERTICAL_ALIGNMENT_TOP,

		height : Ti.UI.SIZE,
		width : Ti.UI.SIZE,
		color : Ti.App.colorPrincipal,
		//backgroundColor : 'blue',
		font : {

			fontSize :15+Ti.App.paddingLabels+'sp'  ,
		},
		top : 0 * formulaWidth
	});
	viewambitGeografic.add(ambitGeografic);

	var ambitGeograficInfo = Ti.UI.createLabel({
		text : win.escenariJSON.ambit,
		left : 3 * formulaWidth,
		verticalAlign : Ti.UI.TEXT_VERTICAL_ALIGNMENT_TOP,

		height : Ti.UI.SIZE,
		width : Ti.UI.SIZE,
		color : '#cccccb',
		//backgroundColor : 'blue',
		font : {

			fontSize :15+Ti.App.paddingLabels+'sp'  ,
		},
		top : 0 * formulaWidth
	});
	viewambitGeografic.add(ambitGeograficInfo);
}
if (win.escenariJSON.escola != null) {

	var viewEscola = Titanium.UI.createView({

		top : 0 * formulaHeight,
		height : Ti.UI.SIZE,
		zIndex : 2,
		layout : 'horizontal',
		width : 280 * formulaWidth,
		left : 20 * formulaWidth
	});
	scrollView.add(viewEscola);
	var escola = Ti.UI.createLabel({
		text : centre_educatiu+': ',
		verticalAlign : Ti.UI.TEXT_VERTICAL_ALIGNMENT_TOP,
		left : 0 * formulaWidth,
		height : Ti.UI.SIZE,
		width : Ti.UI.SIZE,

		color : Ti.App.colorPrincipal,
		//backgroundColor : 'blue',
		font : {

			fontSize : 15+Ti.App.paddingLabels+'sp'  ,
		},
		top : 0 * formulaWidth
	});
	viewEscola.add(escola);

	var escolaInfo = Ti.UI.createLabel({
		text : win.escenariJSON.escola,
		verticalAlign : Ti.UI.TEXT_VERTICAL_ALIGNMENT_TOP,
		left : 3 * formulaWidth,
		height : Ti.UI.SIZE,
		width : Ti.UI.SIZE,

		color : '#cccccb',
		//backgroundColor : 'blue',
		font : {

			fontSize :15+Ti.App.paddingLabels+'sp' ,
		},
		top : 0 * formulaWidth
	});
	viewEscola.add(escolaInfo);
}
if (win.escenariJSON.idioma) {

	var viewIdioma = Titanium.UI.createView({

		top : 0 * formulaHeight,
		height : Ti.UI.SIZE,
		zIndex : 2,
		layout : 'horizontal',
		width : 280 * formulaWidth,
		left : 20 * formulaWidth
	});
	scrollView.add(viewIdioma);

	var idioma = Ti.UI.createLabel({
		text : labelIdiomaTextMinuscula+ ': ',
		left : 0 * formulaWidth,
		verticalAlign : Ti.UI.TEXT_VERTICAL_ALIGNMENT_TOP,
		height : Ti.UI.SIZE,
		width : Ti.UI.SIZE,
		color : Ti.App.colorPrincipal,
		font : {

			fontSize : 15+Ti.App.paddingLabels+'sp'  ,
		},
		top : 0 * formulaWidth
	});
	viewIdioma.add(idioma);

	var idiomaInfo = Ti.UI.createLabel({
		text : win.escenariJSON.idioma,
		left : 3 * formulaWidth,
		verticalAlign : Ti.UI.TEXT_VERTICAL_ALIGNMENT_TOP,
		height : Ti.UI.SIZE,
		width : Ti.UI.SIZE,
		color : '#cccccb',
		font : {

			fontSize : 15+Ti.App.paddingLabels+'sp'  ,
		},
		top : 0 * formulaWidth
	});
	viewIdioma.add(idiomaInfo);
}

Ti.App.clicat = false;

