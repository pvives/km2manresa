function filtre(mapView, viewEscenari, data) {
	if (Titanium.Platform.osname === 'android') {
		var isAndroid = true;
	}
	Ti.App.ArrayCapesCategories = [];
	var widthpantalla = Ti.Platform.displayCaps.platformWidth;
	var heightpantalla = Ti.Platform.displayCaps.platformHeight;
	var formulaWidth = widthpantalla / 320;
	var formulaHeight = heightpantalla / 480;
	var statusBarHeight = Ti.Platform.displayCaps.logicalDensityFactor * 25;
	var table;
	var rowCategoria;
	var filtreObert = false;
	var row;
	var viewOK;
	var arrayRows = [];
	var rowCategoriaNova;
	var activeCategoriesArray = [],
		activeLayersArray = [];

	firstTime = false;
	var dataFiltre = require('/controller/creacioEscenari/filtreNetworkController');
	var dataFiltreInstance = new dataFiltre(win.urlBase + '/api/grups/' + win.id, creaFiltre);

	function creaFiltre(data) {

		arrayRows = [];

		for (var i = 0; i < data.length; i++) {

			var viewSwitch = Ti.UI.createView({
				width : 68 * formulaWidth,
				height : isAndroid ? 40 * formulaHeight : 40,
				right : 10 * formulaWidth,
				activat : false,
				clicat : false,
				bubbleParent : true,
				top : 2 * formulaHeight,
				// backgroundColor : '#353535'
				backgroundImage : '/images/ondes.png'
			});

			viewSwitch.addEventListener('click', function(e) {

				e.source.clicat = true;
				if (e.source.activat) {
					e.source.activat = false;

					e.source.backgroundImage = '/images/ondes.png';
				} else {
					e.source.activat = true;

					e.source.backgroundImage = '/images/on.png';
				}

			});

			row = Ti.UI.createTableViewRow({
				height : isAndroid ? 44 * formulaWidth : 44,
				width : 320 * formulaWidth,
				borderRadius : 0,
				bubbleParent : true,
				primeraVegada : true,
				plegat : true,
				rowsCategories : [],
				viewSwitch : viewSwitch,
				selectedBackgroundColor : '#dedede',
				numRow : i,
				activat : false,
				font : {
					fontSize : '16sp'
				},
				layerId: data[i].capa_ID,
				numCategories : data[i].categories.length,
				isCategoria : false,
				color : '#2f3e47',
				leftImage : '/images/mas.png',
				categories : data[i].categories,
				titol : data[i].capa_nom,
				 title : data[i].capa_nom,
				bubbleParent : true,
				borderWidth : 0,
				className : 'default'

			});
alert('entroooooo');
			row.add(viewSwitch)

			arrayRows.push(row);

			row.addEventListener('click', function(e) {
				var rowsCategoriesGuardades = [];
				var rowsCategoriesGuardadesNoves = [];
				if (e.row.plegat == true) {
					if (!e.row.viewSwitch.clicat || e.row.viewSwitch.activat) {
						if (e.row.primeraVegada) {
							e.row.primeraVegada = false;
							e.row.plegat = false;
							e.row.activat = true;
							e.row.leftImage = '/images/menos.png';
							for (var j = 0; j < e.row.numCategories; j++) {

								e.row.primeraVegada = false;
								rowCategoria = Ti.UI.createTableViewRow({
									height : isAndroid ? 44 * formulaWidth : 44,
									width : 320 * formulaWidth,
									borderRadius : 1,
									rowCapa : row,
									selectedBackgroundColor : '#dedede',
									borderColor : '#999999',
									color : '#2f3e47',
									font : {
										fontSize : '14sp'
									},
									rightImage : e.row.viewSwitch.activat ? '/images/ok.png' : 'none',
									isCategoria : true,
									idCategoria:e.row.categories[j].id,
									categoriaActivada : true,
									title : e.row.categories[j].nom,
									titol : e.row.categories[j].nom,
									bubbleParent : true,
									borderWidth : 1,
									className : 'default'

								});
								// viewOK = Ti.UI.createView({
								// width : 44 * formulaWidth,
								// height : isAndroid ? 26 * formulaHeight : 44,
								// right : 10 * formulaWidth,
								// top : 0 * formulaHeight,
								// categoriaActivada:true,
								// // backgroundColor : '#353535'
								// backgroundImage : e.row.viewSwitch.activat ? '/images/ok.png' : 'none'
								// });
								// rowCategoria.add(viewOK);

								rowCategoria.addEventListener('click', function(e) {

									if (e.row.categoriaActivada) {

										e.row.categoriaActivada = false;
										e.row.rightImage = '/images/okdes.png';
									} else {
										e.row.categoriaActivada = true;
										e.row.rightImage = '/images/ok.png';
									}
								});
								rowsCategoriesGuardades.push(rowCategoria);

								table.insertRowAfter(e.index, rowCategoria, true);

							}

							e.row.rowsCategories = rowsCategoriesGuardades;
						} else {
							e.row.plegat = false;
							e.row.leftImage = '/images/menos.png';
							e.row.activat = true;
							for (var j = 0; j < e.row.numCategories; j++) {
								var rowCategoriaNova = Ti.UI.createTableViewRow({
									height : isAndroid ? 44 * formulaWidth : 44,
									width : 320 * formulaWidth,
									borderRadius : 1,
									rowCapa : row,
									selectedBackgroundColor : '#dedede',
									borderColor : '#999999',
									color : '#2f3e47',
									font : {
										fontSize : '16sp'
									},
									rightImage : e.row.rowsCategories[j].categoriaActivada ? '/images/ok.png' : '/images/okdes.png',
									isCategoria : true,
idCategoria:e.row.categories[j].id,
									categoriaActivada : e.row.rowsCategories[j].categoriaActivada,
									title : e.row.categories[j].nom,
									bubbleParent : true,
									borderWidth : 1,
									className : 'default'

								});
								if (!e.row.viewSwitch.activat) {
									rowCategoriaNova.rightImage = 'none';
								}
								rowCategoriaNova.addEventListener('click', function(e) {

									if (e.source.categoriaActivada) {
										e.source.categoriaActivada = false;
										e.source.rightImage = '/images/okdes.png';
									} else {
										e.source.categoriaActivada = true;
										e.source.rightImage = '/images/ok.png';
									}
								});
								rowsCategoriesGuardadesNoves.push(rowCategoriaNova);
								table.insertRowAfter(e.index, rowCategoriaNova, true);
							}
							e.row.rowsCategories = rowsCategoriesGuardadesNoves;
						}
					}
				} else {

					e.row.plegat = true;
					e.row.leftImage = '/images/mas.png';
					for (var z = 0; z < e.row.numCategories; z++) {

						table.deleteRow(e.index + 1, true);
					}
				}

				//inici prova
				if (!e.row.viewSwitch.activat) {
					e.row.activat = false;
				} else {
					e.row.activat = true;
				}
				//final prova
				e.row.viewSwitch.clicat = false;
			});
		}
		table = Ti.UI.createTableView({
			separatorColor : '#999999',
			left : isAndroid ? 10 * formulaWidth : 10,
			data : arrayRows,
			borderColor : '#999999',
			visible : false,
			width : isAndroid ? 300 * formulaWidth : 300,
			height : isAndroid ? 480 * formulaHeight - 44 * formulaHeight - statusBarHeight - 20 * formulaHeight : 480 * formulaHeight - 44 - 20 - 20,
			top : isAndroid ? 54 * formulaHeight : 44 + 10,
			zIndex : 10,
			left : isAndroid ? 10 * formulaWidth : 10,
			backgroundColor : 'white'
		});
		viewEscenari.add(table);



	}
		var filtreButton = Ti.UI.createButton({
			width : isAndroid ? 44 * formulaWidth : 44,
			height : isAndroid ? 44 * formulaHeight : 44,
			top : 0 * formulaHeight,
			zIndex : 10,
			backgroundImage : '/images/filter.png',
			right : 5 * formulaWidth
		});
		viewEscenari.add(filtreButton);

		filtreButton.addEventListener('click', function(e) {

		if (filtreObert) {
			
			table.hide();
			filtreObert = false;

			//si totes les capes estan desactivades no hi ha filtre i mostro totes les anotacions
			var totesCapesDesactivades = true;
			var trobat;
			for (var j = 0; j < arrayRows.length; j++) {
				if (arrayRows[j].activat == true && !trobat) {
					trobat = true;
					totesCapesDesactivades = false;
				}
			}
			
			if (totesCapesDesactivades == false) {
			
				mapView.removeAllAnnotations();
				for(var i=0; i<mapView.routes.length; i++)
				{
					mapView.removeRoute(mapView.routes[i]);
				}
				mapView.routes = [];


				activeCategoriesArray = [];
				activeLayersArray = [];

				for(var i=0; i<arrayRows.length; i++)
				{
					if(arrayRows[i].activat === true)
					{
						if(activeLayersArray.indexOf(arrayRows[i].layerId) == -1)
						{
							alert('layers: '+arrayRows[i].layerId);
							activeLayersArray.push(arrayRows[i].layerId);
						}
						if(typeof arrayRows[i].rowsCategories != "undefined")
						{
							for(var j=0; j<arrayRows[i].rowsCategories.length; j++)
							{
								if(arrayRows[i].rowsCategories[j].categoriaActivada === true)
								{
									var categoriaToAdd = arrayRows[i].rowsCategories[j].idCategoria;
									alert('categoria: '+categoriaToAdd);
									if(activeCategoriesArray.indexOf(categoriaToAdd) == -1)
									{
										activeCategoriesArray.push(categoriaToAdd);
									}
								}
							}
						}
					}
				}
				
				var postData = {
					categories: activeCategoriesArray.toString(),
					capes: activeLayersArray.toString()
				};
alert('catgories: '+activeCategoriesArray[0]);
				var url = Ti.App.urlBase + '/api/filtre/' + Ti.App.idEscenari + '/ca/trams/' + Ti.App.latitude + "/" + Ti.App.longitude + "/0/" + Ti.App.maxAnotacions;
				Ti.API.info(Ti.App.urlBase + '/api/filtre/' + Ti.App.idEscenari + '/ca/trams/' + Ti.App.latitude + "/" + Ti.App.longitude + "/0/" + Ti.App.maxAnotacions);
				var postNetworkController = require("/controller/creacioEscenari/Mod_PostNetworkController");
				var networkControllerFilter = new postNetworkController(url, postData, function(e){
					var newAnnotations = JSON.parse(e);
					alert('len: '+newAnnotations.length);
					var annotations = require('/controller/creacioEscenari/Mod_createAnnotations');
					new annotations(newAnnotations, afegirAnotacions);
				});
				
				function afegirAnotacions(annotations, routes) {
					mapView.addAnnotations(annotations);
					mapView.routes = routes;
					for(var i=0; i<routes.length; i++)
					{
						mapView.addRoute(routes[i]);
					}
				}

			} else {
				alert('entro2');
				mapView.removeAllAnnotations();
				for(var i=0; i<mapView.routes.length; i++)
				{
					mapView.removeRoute(mapView.routes[i]);
				}


				var url = Ti.App.urlBase + '/mobile/objectes/' + Ti.App.idEscenari + '/ca/geoemotion/' + Ti.App.latitude + "/" + Ti.App.longitude + "/0/" + Ti.App.maxAnotacions;

				var postNetworkController = require("/controller/creacioEscenari/Mod_PostNetworkController");
				var networkControllerFilter = new postNetworkController(url, {}, function(e){
					var newAnnotations = JSON.parse(e);
					//Ti.API.info("post response: " + e);
					var annotations = require('/controller/creacioEscenari/Mod_createAnnotations');
					new annotations(newAnnotations, afegirAnotacions);
				});

	

				function afegirAnotacions(annotations, routes) {
					mapView.addAnnotations(annotations);
					mapView.routes = routes;
					for(var i=0; i<routes.length; i++)
					{
						mapView.addRoute(routes[i]);
					}
				}

			}

		} else {
			filtreObert = true;
			table.show();
			
		}


		});
}

module.exports = filtre;
