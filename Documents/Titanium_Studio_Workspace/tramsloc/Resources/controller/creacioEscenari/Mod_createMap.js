function createMap(annotations, region, rightView, callback, viewEscenari, urlBase, routes) {
	if (Titanium.Platform.osname === 'android') {
		var isAndroid = true;
	}
	var widthpantalla = Ti.Platform.displayCaps.platformWidth;
	var heightpantalla = Ti.Platform.displayCaps.platformHeight;
	var formulaWidth = widthpantalla / 320;
	var formulaHeight = heightpantalla / 480;
	var statusBarHeight = isAndroid ? Ti.Platform.displayCaps.logicalDensityFactor * 25 : 20;
	var fitxaPareInstance;
	var botoInici;
	var MapModule = require('ti.map');
	if (Titanium.Platform.osname === 'android') {
		var isAndroid = true;
			var rc = MapModule.isGooglePlayServicesAvailable()
	switch (rc) {
		case MapModule.SUCCESS:
			Ti.API.info('Google Play services is installed.');
			break;
		case MapModule.SERVICE_MISSING:
			alert('Google Play services is missing. Please install Google Play services from the Google Play store.');
			break;
		case MapModule.SERVICE_VERSION_UPDATE_REQUIRED:
			Ti.API.info('Google Play services is out of date. Please update Google Play services.');
			break;
		case MapModule.SERVICE_DISABLED:
			Ti.API.info('Google Play services is disabled. Please enable Google Play services.');
			break;
		case MapModule.SERVICE_INVALID:
			Ti.API.info('Google Play services cannot be authenticated. Reinstall Google Play services.');
			break;
		default:
			Ti.API.info('Unknown error.');
			break;
	}
	}

	//creacio vista mapa
	var mapView = MapModule.createView({
		mapType : MapModule.SATELLITE_TYPE,
		top : isAndroid ? 44 * formulaHeight : 44,
		region : region,
		left : 0 * formulaWidth,
		animate : true,
		height : isAndroid ? 480 * formulaHeight - statusBarHeight - 44 * formulaHeight : 480 * formulaHeight - 44 - statusBarHeight,
		// regionFit : true,
		userLocation : true,
		annotations : annotations,
		userLocationButton: false,
		routes: routes
	});

	for(var i=0; i<routes.length; i++)
	{
		mapView.addRoute(routes[i]);
	}

	var viewTornarMapa = Ti.UI.createView({
		width : 320 * formulaWidth,
		height : 480 * formulaHeight,
		left : 0 * formulaWidth,
		opacity : 0.5,
		zIndex : 2,
		height : isAndroid ? 480 * formulaHeight - statusBarHeight : 480 * formulaHeight,
		visible : false,
		top : 0 * formulaHeight,
		backgroundColor : 'black'

	});

	viewTornarMapa.addEventListener('click', function(e) {
		if (fitxaPareInstance != null) {
			viewEscenari.remove(fitxaPareInstance);
			

		}
		viewTornarMapa.visible = false;
		if (!isAndroid) {
			rightView.rightView.animate({
				right : -270 * formulaWidth,
				duration : 500
			});
		} else {
			rightView.rightView.right = -270 * formulaWidth;
		}
	});

	var creuTornarMapa = Ti.UI.createView({
		width : isAndroid ? 36 * formulaWidth : 36,
		height : isAndroid ? 36 * formulaHeight : 36,
		left : isAndroid ? 4 * formulaWidth : 4,

		zIndex : 11,

		top : isAndroid ? 4 * formulaHeight : 4,
		backgroundImage : '/images/cruz.png'

	});
	rightView.rightView.add(creuTornarMapa);
	creuTornarMapa.addEventListener('click', function(e) {
		if (fitxaPareInstance != null) {
			viewEscenari.remove(fitxaPareInstance);
			// fitxaPareInstance.esborraBotoInici();

		}
		viewTornarMapa.visible = false;
		if (!isAndroid) {
			rightView.rightView.animate({
				right : -270 * formulaWidth,
				duration : 500
			});
		} else {
			rightView.rightView.right = -270 * formulaWidth;
		}
	});
	mapView.addEventListener('click', function(evt) {

		if (evt.clicksource == 'rightButton' || evt.clicksource == 'rightPane' || evt.clicksource == 'title' || evt.clicksource == 'infoWindow') {

			viewTornarMapa.visible = true;

			if (evt.annotation.tipus == 'punt') {

				rightView.createFitxaPunt(evt.annotation.annotationData);
				if (!isAndroid) {
					rightView.rightView.animate({
						right : 25 * formulaWidth,
						duration : 500
					});
				} else {
					rightView.rightView.right = 25 * formulaWidth;
				}

			} else if (evt.annotation.tipus == 'jocpista') {

				var fitxaPare = require('controller/objectes/Mod_objectePare');
				fitxaPareInstance = new fitxaPare(evt.annotation.annotationData, '/images/defectopista.png', urlBase, rightView, 'jocpista', viewEscenari);

				viewEscenari.add(fitxaPareInstance);

			} else if (evt.annotation.tipus == 'maquina') {
				var fitxaPare = require('controller/objectes/Mod_objectePare');
				fitxaPareInstance = new fitxaPare(evt.annotation.annotationData, '/images/defectotiempo.png', urlBase, rightView, 'maquina', viewEscenari);

				viewEscenari.add(fitxaPareInstance);

			} else if (evt.annotation.tipus == 'trivial') {
				var fitxaPare = require('controller/objectes/Mod_objectePare');
				fitxaPareInstance = new fitxaPare(evt.annotation.annotationData, '/images/defectopreguntas.png', urlBase, rightView, 'trivial', viewEscenari);

				viewEscenari.add(fitxaPareInstance);
			} else if(evt.annotation.tipus == "ruta") {
				var fitxaPare = require('controller/objectes/Mod_objectePare');
				fitxaPareInstance = new fitxaPare(evt.annotation.annotationData, '/images/defectopreguntas.png', urlBase, rightView, 'ruta', viewEscenari);
				viewEscenari.add(fitxaPareInstance);
			}
		}

	});

	// var showFitxaIncidencia = function (){
	// viewTornarMapa.visible = true;
	// rightView.createFitxaIncidencia();
	// rightView.rightView.animate({
	// right : 0,
	// duration : 500
	// });
	// }

	//afegeixo botons sat map

	callback(mapView, rightView, viewTornarMapa);

}

module.exports = createMap;
