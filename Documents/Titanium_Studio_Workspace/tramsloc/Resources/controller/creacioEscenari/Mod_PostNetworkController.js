function createNetworkController(url, postData, callback) {
	if(Titanium.App.Properties.getString("language", "undefined")=='ca'){
		Titanium.include('/controller/translations.js');
	}
	else if(Titanium.App.Properties.getString("language", "undefined")=='es'){
		Titanium.include('/controller/translations_es.js');
	}
	else if(Titanium.App.Properties.getString("language", "undefined")=='en'){
		Titanium.include('/controller/translations_en.js');
	}
	else if (Titanium.App.Properties.getString("language", "undefined") == 'pt') {
		Titanium.include('/controller/translations_pt.js');
	}
	else if (Titanium.App.Properties.getString("language", "undefined") == 'pt-PT') {
		Titanium.include('/controller/translations_pt.js');
	}
	else if (Titanium.App.Properties.getString("language", "undefined") == 'eus') {
		Titanium.include('/controller/translations_eus.js');
	} 
	else {
		Titanium.include('/controller/translations_en.js');
	}

	var firsttime=true;
	var error=false;
	var xhr = Titanium.Network.createHTTPClient();

	xhr.onload = function() {
		try
		{
			//var data = JSON.parse(this.responseText);
			//crida a createAnnotations
			callback(this.responseText);
			alert('response: '+this.responseText);
			//alert(this.responseText);
		}
		catch(e)
		{
			
			if (firsttime == true) {
				var alertDialog = Titanium.UI.createAlertDialog({
					title : xhr_error_title,
					message : xhr_error_message,
					buttonNames : ['ok']
				});
				alertDialog.show();
				firsttime = false;
				error=true;
			}
			return error;
		}	
	};

	xhr.open('POST', url);
	//xhr.setRequestHeader("Content-Type", "application/json-rpc");	
	xhr.send(postData);

	xhr.onerror = function(e) {
			//this fires if Titanium/the native SDK cannot successfully retrieve a resource
			
			if (firsttime == true) {
				var alertDialog = Titanium.UI.createAlertDialog({
					title : xhr_error_title,
					message : xhr_error_message,
					buttonNames : ['ok']
				});
				alertDialog.show();
				firsttime = false;
				error=true;
			}
		};
		return error;
	}

	module.exports = createNetworkController; 