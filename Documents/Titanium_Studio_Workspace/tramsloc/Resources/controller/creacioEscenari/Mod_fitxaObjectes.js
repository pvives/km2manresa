function createFitxaObjectes(annotations, region, urlBase, callback, viewEscenari, win, routes) {

	//Crea les fitxes de tots els objectes
	var widthpantalla = Ti.Platform.displayCaps.platformWidth;
	var heightpantalla = Ti.Platform.displayCaps.platformHeight;
	var formulaWidth = widthpantalla / 320;
	var formulaHeight = heightpantalla / 480;
var statusBarHeight =isAndroid? Ti.Platform.displayCaps.logicalDensityFactor * 25:20;
	if (Titanium.Platform.osname === 'android') {
		var isAndroid = true;
	}
	// var statusBarHeight = Ti.Platform.displayCaps.logicalDensityFactor * 25;

	var returnObject = {};

	var rightView = Ti.UI.createView({
		width : 270 * formulaWidth,
		top : isAndroid?5* formulaHeight:10 * formulaHeight,
		borderRadius:10,
		height : isAndroid ? 480 * formulaHeight - statusBarHeight-20 * formulaHeight: 480 * formulaHeight- statusBarHeight-20,
		zIndex : 10,
		right : -270 * formulaWidth
	});

	//fitxa punt amb barra superior i la funcio que crea el punt

	var fitxaPunt = require('/controller/objectes/Mod_objectePunt');
	var fitxaPuntInstance = new fitxaPunt(urlBase);

	rightView.add(fitxaPuntInstance);
	

	function createFitxaPunt(dataPunt) {
		fitxaPuntInstance.visible = true;
		fitxaJocPistesInstance.visible = false;
		fitxaMaquinaInstance.visible = false;
		fitxaTrivialInstance.visible = false;
		// fitxaIncidenciaInstance.visible = false;

		fitxaPuntInstance.createPunt(dataPunt, urlBase);

	}

	//fitxa joc pistes amb barra superior i la funcio que crea el joc

	var fitxaJocPistes = require('controller/objectes/Mod_objecteJocPistes');
	var fitxaJocPistesInstance = new fitxaJocPistes();

	rightView.add(fitxaJocPistesInstance);

	var lastIdJocPistes = -1;
	function createFitxaJocPistes(dataJocPistes) {

		fitxaPuntInstance.visible = false;
		fitxaJocPistesInstance.visible = true;
		fitxaMaquinaInstance.visible = false;
		fitxaTrivialInstance.visible = false;
		// fitxaIncidenciaInstance.visible = false;

		//si el id clicat coincideix amb lultim no torno a crear objecte
		if (dataJocPistes != null) {
			if (lastIdJocPistes != dataJocPistes.id) {
				
				fitxaJocPistesInstance.refreshJocPistes(dataJocPistes, urlBase);
				lastIdJocPistes = dataJocPistes.id;
			}
		}

	}

	//fitxa Maquina
	var fitxaMaquina = require('controller/objectes/Mod_objecteMaquina');
	var fitxaMaquinaInstance = new fitxaMaquina();

	rightView.add(fitxaMaquinaInstance);

	var lastIdMaquina = -1;
	function createFitxaMaquina(dataMaquina) {

		fitxaPuntInstance.visible = false;
		fitxaJocPistesInstance.visible = false;
		fitxaMaquinaInstance.visible = true;
		fitxaTrivialInstance.visible = false;
		// fitxaIncidenciaInstance.visible = false;

		//si el id clicat coincideix amb lultim no torno a crear objecte
		if (dataMaquina != null) {
			if (lastIdMaquina != dataMaquina.id) {
				fitxaMaquinaInstance.refreshMaquina(dataMaquina, urlBase);
				lastIdMaquina = dataMaquina.id;
			}
		}

	}


	// //fitxa Trivial
	var fitxaTrivial = require('controller/objectes/Mod_objecteTrivial');
	var fitxaTrivialInstance = new fitxaTrivial();

	rightView.add(fitxaTrivialInstance);

	var lastIdTrivial = -1;
	function createFitxaTrivial(dataTrivial) {

		fitxaPuntInstance.visible = false;
		fitxaJocPistesInstance.visible = false;
		fitxaMaquinaInstance.visible = false;
		fitxaTrivialInstance.visible = true;
		// fitxaIncidenciaInstance.visible = false;

		//si el id clicat coincideix amb lultim no torno a crear objecte
		if (dataTrivial != null) {
			if (lastIdTrivial != dataTrivial.id) {
				fitxaTrivialInstance.refreshTrivial(dataTrivial, urlBase);
				lastIdTrivial = dataTrivial.id;
			}
		}
	}



	// INICI FITXA OBJECTE INCIDENCIA

	// var fitxaIncidencia = require('controller/objectes/Mod_objecteIncidencia');
	// var fitxaIncidenciaInstance = new fitxaIncidencia();
// 
	// rightView.add(fitxaIncidenciaInstance);
// 
	// function createFitxaIncidencia() {
		// fitxaPuntInstance.visible = false;
		// fitxaJocPistesInstance.visible = false;
		// fitxaMaquinaInstance.visible = false;
		// fitxaTrivialInstance.visible = false;
		// fitxaIncidenciaInstance.visible = true;
// 
		// fitxaIncidenciaInstance.createIncidencia(viewEscenari, win);
		// alert('despres incidencia');
	// }

	// FINAL FITXA OBJECTE INCIDENCIA


	returnObject.rightView = rightView;

	returnObject.createFitxaPunt = createFitxaPunt;
	returnObject.createFitxaJocPistes = createFitxaJocPistes;
	returnObject.createFitxaMaquina = createFitxaMaquina;
	returnObject.createFitxaTrivial = createFitxaTrivial;
	// returnObject.createFitxaIncidencia = createFitxaIncidencia;

	callback(annotations, region, returnObject, routes);

	return returnObject;

}

module.exports = createFitxaObjectes;
