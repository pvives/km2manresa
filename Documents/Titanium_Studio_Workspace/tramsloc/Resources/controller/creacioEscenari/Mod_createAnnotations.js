function createAnnotations(data, callback) {
	//creo anotacions
	//cal definir els camps de la anotacio per cada projecte

	var annotations = [],
		routes = [];
	if (Titanium.Platform.osname === 'android') {
		var isAndroid = true;
	}
	var MapModule = require('ti.map');
	if (data != null) {

		for (var i = 0; i < data.length; i++) {
			//// Codi propi del projecte eduloc o geoemotion ////
			if (data[i].tipus == 'punt') {
				if (data[i].capaprincipal != null && data[i].categoriaprincipal != null) {
					if (data[i].capaprincipal.color != null) {
						var anotacio = MapModule.createAnnotation({
							latitude : data[i].lat,
							longitude : data[i].lon,
							tipus : data[i].tipus,
							descripcio : data[i].descripcio,
							rightButton : isAndroid ? '/images/flechaburbuja.png' : '/images/flechaburbuja.png',
							image : '/images/iconesMapa/' + data[i].capaprincipal.color + data[i].categoriaprincipal.icona,
							title : data[i].nom,
							myid : data[i].id,
							annotationData : data[i]
						});
						annotations.push(anotacio);
					}

					if (isAndroid) {
						if ((Titanium.Platform.displayCaps.density == "low") || (Titanium.Platform.displayCaps.density == "medium")) {
							anotacio.image = '/iconesMapa/mdpi/' + data[i].capaprincipal.color + data[i].categoriaprincipal.icona;
							anotacio.rightButton = '/iconesMapa/mdpi/flechaburbuja.png';
						} else if (Titanium.Platform.displayCaps.density == "high") {
							anotacio.image = '/iconesMapa/hdpi/' + data[i].capaprincipal.color + data[i].categoriaprincipal.icona;
							anotacio.rightButton = '/iconesMapa/hdpi/flechaburbuja.png';
						} else if (Titanium.Platform.displayCaps.density == "xhigh"||Titanium.Platform.displayCaps.density == "xxhigh") {
							anotacio.image = '/iconesMapa/xhdpi/' + data[i].capaprincipal.color + data[i].categoriaprincipal.icona;

							anotacio.rightButton = '/iconesMapa/xhdpi/flechaburbuja.png';
						}
					}
				}
			} else if (data[i].tipus == 'jocpista') {
				//es mostra nomes la primera pista del joc
				if (data[i].capaprincipal != null && data[i].categoriaprincipal != null) {
					var anotacio = MapModule.createAnnotation({
						latitude : data[i].lat,
						longitude : data[i].lon,
						rightButton : isAndroid ? '/images/flechaburbuja.png' : '/images/flechaburbuja.png',
						tipus : data[i].tipus,
						image : '/images/iconesMapa/' + data[i].capaprincipal.color + 'pista.png',
						title : data[i].nom,
						myid : data[i].id,
						annotationData : data[i]
					});
					annotations.push(anotacio);

					if (isAndroid) {
					if ((Titanium.Platform.displayCaps.density == "low") || (Titanium.Platform.displayCaps.density == "medium")) {
					anotacio.image = '/iconesMapa/mdpi' + data[i].capaprincipal.color + 'pista.png';
					anotacio.rightButton = '/iconesMapa/mdpi/flechaburbuja.png';
					
					} else if (Titanium.Platform.displayCaps.density == "high") {
					anotacio.image = '/iconesMapa/hdpi' + data[i].capaprincipal.color + 'pista.png';
					anotacio.rightButton = '/iconesMapa/hdpi/flechaburbuja.png';
					
					} else if (Titanium.Platform.displayCaps.density == "xhigh"||Titanium.Platform.displayCaps.density == "xxhigh") {
					anotacio.image = '/iconesMapa/xhdpi/pista.png';
					anotacio.rightButton ='/iconesMapa/xhdpi' + data[i].capaprincipal.color + 'pista.png';
					
					}
					}
				}
			} else if (data[i].tipus == 'maquina') {
				if (data[i].capaprincipal != null && data[i].categoriaprincipal != null) {
					var anotacio = MapModule.createAnnotation({
						latitude : data[i].lat,
						longitude : data[i].lon,
						tipus : data[i].tipus,
						rightButton : isAndroid ? '/images/flechaburbuja.png' : '/images/flechaburbuja.png',
						image : '/images/iconesMapa/' + data[i].capaprincipal.color + 'tiempo.png',
						title : data[i].nom,
						myid : data[i].id,
						annotationData : data[i]
					});
					annotations.push(anotacio);
					if (isAndroid) {
					if ((Titanium.Platform.displayCaps.density == "low") || (Titanium.Platform.displayCaps.density == "medium")) {
					anotacio.image = '/iconesMapa/mdpi' + data[i].capaprincipal.color + 'tiempo.png';
					anotacio.rightButton = '/iconesMapa/mdpi/flechaburbuja.png';
					
					} else if (Titanium.Platform.displayCaps.density == "high") {
					anotacio.image = '/iconesMapa/hdpi' + data[i].capaprincipal.color + 'tiempo.png';
					anotacio.rightButton = '/iconesMapa/hdpi/flechaburbuja.png';
					
					} else if (Titanium.Platform.displayCaps.density == "xhigh"||Titanium.Platform.displayCaps.density == "xxhigh") {
					anotacio.image =  '/iconesMapa/xhdpi' + data[i].capaprincipal.color + 'tiempo.png';
					anotacio.rightButton = '/iconesMapa/xhdpi/flechaburbuja.png';
					
					}
					}
				}
			} else if (data[i].tipus == 'trivial') {
				// if (data[i].capaprincipal != null && data[i].categoriaprincipal != null) {
					var anotacio = MapModule.createAnnotation({
						latitude : data[i].latitud,
						longitude : data[i].longitud,
						tipus : data[i].tipus,
						rightButton : isAndroid ? '/images/flechaburbuja.png' : '/images/flechaburbuja.png',
						image : '/images/iconesMapa/' + data[i].capaprincipal.color + 'pregunta.png',
						title : data[i].nom,
						myid : data[i].id,
						annotationData : data[i]
					});
					annotations.push(anotacio);
					if (isAndroid) {
					if ((Titanium.Platform.displayCaps.density == "low") || (Titanium.Platform.displayCaps.density == "medium")) {
					anotacio.image =  '/iconesMapa/mdpi' + data[i].capaprincipal.color + 'pregunta.png';
					anotacio.rightButton = '/iconesMapa/mdpi/flechaburbuja.png';
					
					} else if (Titanium.Platform.displayCaps.density == "high") {
					anotacio.image = '/iconesMapa/hdpi' + data[i].capaprincipal.color + 'pregunta.png';
					anotacio.rightButton = '/iconesMapa/hdpi/flechaburbuja.png';
					
					} else if (Titanium.Platform.displayCaps.density == "xhigh"||Titanium.Platform.displayCaps.density == "xxhigh") {
					anotacio.image =  '/iconesMapa/xhdpi' + data[i].capaprincipal.color + 'pregunta.png';
					anotacio.rightButton = '/iconesMapa/xhdpi/flechaburbuja.png';
					
					}
					}
				// }
			}
else if (data[i].tipus == "ruta") {

				/*  1 - Convertir coordenades al format que el mòbil admet
					2 - Crear un punt amb l'inici de la ruta
					3 - Crear la ruta amb el track de punts
					4 - Afegir punt i ruta als respectius Arrays
				*/

				var routeConverter = require("/controller/creacioEscenari/lineStringToRoute");
				var routeAndFirstPoint = new routeConverter(data[i]);
				annotations.push(routeAndFirstPoint.initialPoint);
				routes.push(routeAndFirstPoint.routeObject);
				
			}
		}

		//// Fi codi del projecte eduloc o geoemotion ////

	}

	//crida a centerMap
	callback(annotations, routes);

	return annotations;

}

module.exports = createAnnotations;

