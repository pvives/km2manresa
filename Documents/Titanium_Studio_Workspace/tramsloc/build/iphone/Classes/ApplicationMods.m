/**
 * Appcelerator Titanium Mobile
 * Copyright (c) 2009-2014 by Appcelerator, Inc. All Rights Reserved.
 * Licensed under the terms of the Apache Public License
 * Please see the LICENSE included with this distribution for details.
 *
 * WARNING: This is generated code. Do not modify. Your changes *will* be lost.
 */

#import "ApplicationMods.h"

@implementation ApplicationMods

+ (NSArray*) compiledMods
{
	NSMutableArray *modules = [NSMutableArray array];

	
		[modules addObject:[NSDictionary
			dictionaryWithObjectsAndKeys:@"iosloginhash",
			@"name",
			@"cat.itinerarium.loginhash",
			@"moduleid",
			@"1.0",
			@"version",
			@"f251294a-57d1-47ec-8162-58484d491aab",
			@"guid",
			@"",
			@"licensekey",
			nil
		]];
		
		[modules addObject:[NSDictionary
			dictionaryWithObjectsAndKeys:@"map",
			@"name",
			@"ti.map",
			@"moduleid",
			@"2.0.2",
			@"version",
			@"fee93b77-8eb3-418c-8f04-013664c4af83",
			@"guid",
			@"",
			@"licensekey",
			nil
		]];
		
		[modules addObject:[NSDictionary
			dictionaryWithObjectsAndKeys:@"nappdrawer",
			@"name",
			@"dk.napp.drawer",
			@"moduleid",
			@"1.0.3",
			@"version",
			@"2a446559-1d59-4808-aefc-7d02d3130ebb",
			@"guid",
			@"",
			@"licensekey",
			nil
		]];
		
		[modules addObject:[NSDictionary
			dictionaryWithObjectsAndKeys:@"titanium-social-module",
			@"name",
			@"de.marcelpociot.social",
			@"moduleid",
			@"1.0.2",
			@"version",
			@"61aa3517-3020-4af2-a0d3-8928f3009ba8",
			@"guid",
			@"",
			@"licensekey",
			nil
		]];
		

	return modules;
}

@end