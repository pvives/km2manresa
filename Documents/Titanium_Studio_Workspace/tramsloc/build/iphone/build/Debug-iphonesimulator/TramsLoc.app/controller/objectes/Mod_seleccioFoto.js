function camera(){

	var callbackFunction;

	var returnObject={};

	var cropEnabled = false;

	var dialogTranslations = {
		'en' : ['Take a picture', 'Select from library', 'Cancel'],
		'es' : ['Tomar una fotografía', 'Seleccionar de la biblioteca', 'Cancelar'],
		'ca' : ['Fer una fotografia', 'Seleccionar de la biblioteca', 'Cancel·lar']
	};

	var titleTranslations = {
		'en' : 'Select an option:',
		'es' : 'Selecciona una opción:',
		'ca' : 'Selecciona una opció:'
	};

	function dialogInCurrentLanguage(){
		if(Ti.Locale.currentLanguage == 'en' || Ti.Locale.currentLanguage == 'es' || Ti.Locale.currentLanguage == 'ca'){
			return dialogTranslations[Ti.Locale.currentLanguage];
		}
		else{
			return dialogTranslations['en'];
		}
	}

	function titleInCurrentLanguage(){
		if(Ti.Locale.currentLanguage == 'en' || Ti.Locale.currentLanguage == 'es' || Ti.Locale.currentLanguage == 'ca'){
			return titleTranslations[Ti.Locale.currentLanguage];
		}
		else{
			return titleTranslations['en'];
		}
	}


	function crop(baseImage){

		var cropView = Titanium.UI.createView({
			width:270, height:82
		});
		cropView.add(baseImage);
		var croppedImage = cropView.toImage();
		callbackFunction(cropView);
	}


	var dialog = Titanium.UI.createOptionDialog({
		options: dialogInCurrentLanguage(),
		cancel: 2,
		title: titleInCurrentLanguage()
	});
	dialog.addEventListener('click',function(e){
		if(e.index == 0){
			Titanium.Media.showCamera({
				allowEditing: true,
				saveToPhotoGallery: false,
				success: function(e){
					if(cropEnabled){
						crop(e.media);
					}
					else {
						callbackFunction(e.media);
					}
				}
			});
		}else if(e.index == 1){
			Ti.Media.openPhotoGallery({
				allowEditing: true,
				saveToPhotoGallery: false,
				success: function(e){
					if(cropEnabled){
						crop(e.media);
					}
					else {
						callbackFunction(e.media);
					}
				}
			});
		}
	});
	
	function showPhotoDialog(cropEnable, callback)
	{
		cropEnabled = cropEnable;
		dialog.show();
		callbackFunction = callback;
	}

	returnObject.showPhotoDialog = showPhotoDialog;
	returnObject.camera= camera;
	return returnObject;
};

module.exports=camera;