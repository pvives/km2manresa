function meus(url, callback, actInd) {
	if (Titanium.App.Properties.getString("language", "undefined") == 'ca') {
		Titanium.include('/controller/translations.js');
	} else if (Titanium.App.Properties.getString("language", "undefined") == 'es') {
		Titanium.include('/controller/translations_es.js');
	} else if (Titanium.App.Properties.getString("language", "undefined") == 'en') {
		Titanium.include('/controller/translations_en.js');
	} else if (Titanium.App.Properties.getString("language", "undefined") == 'pt') {
		Titanium.include('/controller/translations_pt.js');
	} else if (Titanium.App.Properties.getString("language", "undefined") == 'pt-PT') {
		Titanium.include('/controller/translations_pt.js');
	} 
	else if (Titanium.App.Properties.getString("language", "undefined") == 'eus') {
		Titanium.include('/controller/translations_eus.js');
	} else {
		Titanium.include('/controller/translations_en.js');
	}

	var primeraVegada = true;
	var xhr = Titanium.Network.createHTTPClient();

	var saltedPassword = Titanium.App.Properties.getString("saltedPassword", null);
	var userName = Titanium.App.Properties.getString("user");

	var wsse = require('controller/llistesEscenaris/wsse');
	var wsseHeader = wsse.wsseHeader(userName, saltedPassword);

	xhr.onload = function() {

		var data = JSON.parse(this.responseText);

		//crida a createAnnotations
		callback(data);
	};

	xhr.open('POST', url);
	xhr.setRequestHeader('X-WSSE', wsseHeader);
	xhr.onerror = function(e) {
		//this fires if Titanium/the native SDK cannot successfully retrieve a resource
		if (primeraVegada == true) {
			if(actInd!=null){
				fonsActInd.hide();
				actInd.hide();
			}
			
			var alertDialog = Titanium.UI.createAlertDialog({
				title : xhr_error_title,
				message : xhr_error_message,
				buttonNames : ['Ok']
			});
			alertDialog.show();
			primeraVegada = false;
		}
	};

	xhr.send({

		'filter_order_by' : Ti.App.ordenacio,
	});

}

module.exports = meus;
