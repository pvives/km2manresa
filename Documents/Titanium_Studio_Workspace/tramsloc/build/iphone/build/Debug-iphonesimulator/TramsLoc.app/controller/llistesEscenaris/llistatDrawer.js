function llistatDrawer(urlBase, win) {
	if (Titanium.Platform.osname === 'android') {
		var isAndroid = true;
	}

	var widthpantalla = Ti.Platform.displayCaps.platformWidth;
	var heightpantalla = Ti.Platform.displayCaps.platformHeight;
	var formulaWidth = widthpantalla / 320;
	var formulaHeight = heightpantalla / 480;
	var statusBarHeight = isAndroid ? Ti.Platform.displayCaps.logicalDensityFactor * 25 : 20;

	var customFont = 'Anna Beta';
	if (Ti.Platform.osname == 'android') {
		// on Android, use the "base name" of the file (name without extension)
		customFont = 'AnnaBeta-Regular';
	}
	var firstTimeLogin = true;
	var data = [];
	var titols = [{
		titol : biblioteca,
		icona : '/images/biblioteca.png'
	}, {
		titol : destacats,
		icona : '/images/destacados.png'

	}, {
		titol : meus_escenaris,
		icona : '/images/misescenarios.png'
	}, {
		titol : descarregats,
		icona : '/images/descargas.png'
	}, {
		titol : configuracio,
		icona : '/images/login.png'
	}];

	for (var i = 0; i < titols.length; i++) {
		var row = Ti.UI.createTableViewRow({
			height : isAndroid ? 44 * formulaWidth : 44,
			top : 0,
			left : 0,
			selectedBackgroundColor : '#cbcbca',
			className : 'default',
			width : isAndroid ? 320 * formulaWidth : 320,

		});

		var labelTitol = Ti.UI.createLabel({
			text : titols[i].titol,
			left : 50 * formulaWidth,
			textAlign : 'left',
			height : isAndroid ? 44 * formulaWidth : 44,
			width : 270 * formulaWidth,
			color : 'white',
			top : 1 * formulaHeight,
			font : {
				fontSize : 21 + Ti.App.paddingLabels + 'sp',
						 fontFamily : Ti.App.entorn=='eduloc'?customFont:'Helvetica'

			},
		});
		row.add(labelTitol);

		var icona = Ti.UI.createView({
			width : 44 * formulaWidth,
			height : isAndroid ? 44 * formulaWidth : 44,
			top : 0 * formulaHeight,

			backgroundImage : titols[i].icona,
			left : 0 * formulaWidth
		});
		row.add(icona);

		data[i] = row;
	}

	var tableView = Ti.UI.createTableView({
		separatorColor : '#272727',
		backgroundColor : '#353535',
		color : 'white',
		data : data
	});
	if (iOS7) {
		tableView.top = 20;
	}
	tableView.addEventListener("click", function(e) {
		if (viewOkCancel != null) {
			viewOkCancel.animate({
				bottom : -260 * formulaHeight,
				duration : 500
			});

		}
		if (!isAndroid) {
			taParaulaClau.blur();
			taAutor.blur();
			taPoblacio.blur();
			taIdioma.blur();
			taNomEscola.blur();
		}

		if (tableViewBibliotecaInstance != null) {

			winCentral.remove(tableViewBibliotecaInstance);
			tableViewBibliotecaInstance = null;

		}
		if (tableViewMeusInstance != null) {

			winCentral.remove(tableViewMeusInstance);
			tableViewMeusInstance = null;
		}
		if (tableViewDestacatsInstance != null) {

			winCentral.remove(tableViewDestacatsInstance);
			tableViewDestacatsInstance = null;
		}
		if (tableViewOfflineInstance != null) {

			winCentral.remove(tableViewOfflineInstance);
			tableViewOfflineInstance = null;
		}
		if (fitxaLoginInstance != null) {

			fitxaLoginInstance.visible = false;

		}
		if (viewFiltreInstance != null) {

			winCentral.remove(viewFiltreInstance);
			viewFiltreInstance = null;
		}
		if (e.index == 1) {
			if (filtreInstance != null) {
				filtreInstance.animate({
					top : isAndroid ? -480 * formulaHeight - 44 * formulaHeight : -480 * formulaHeight - 44,
					duration : 500
				});
			}

			filtreObert = false;
			
			botoInfoDescarregats.visible = false;

			fonsActInd.show();
			actInd.show();

			canviaTitol(destacats);
			filterButton.visible = false;
			labelEscenaris.visible = false;
			
			drawer.toggleLeftWindow();

			Ti.App.paraula_clau2 = Ti.App.paraula_clau;
			Ti.App.nom_escenari2 = Ti.App.nom_escenari;
			Ti.App.poblacio2 = Ti.App.poblacio;
			Ti.App.idioma2 = Ti.App.idioma;
			Ti.App.area_coneixement2 = Ti.App.area_coneixement;
			Ti.App.public_objectiu2 = Ti.App.public_objectiu;
			Ti.App.edat_autor2 = Ti.App.edat_autor;
			Ti.App.nom_centre2 = Ti.App.nom_centre;
			Ti.App.autor2 = Ti.App.autor;

			Ti.App.paraula_clau = '';
			Ti.App.nom_escenari = '';
			Ti.App.poblacio = '';
			Ti.App.idioma = '';
			Ti.App.area_coneixement = '';
			Ti.App.public_objectiu = '';
			Ti.App.edat_autor = '';
			Ti.App.nom_centre = '';
			Ti.App.autor = '';

			var dataDestacats = require('controller/llistesEscenaris/llistats_network_Controller');
			if (Ti.App.entorn == 'eduloc') {
				var dataDestacatsInstance = new dataDestacats(urlBase + '/mobile/scenario/library', callbackLlistatDestacats, actInd);

			} else {
				var dataDestacatsInstance = new dataDestacats(urlBase + '/api/scenario/library/' + Ti.App.entorn + '/es', callbackLlistatDestacats, actInd);

			}

			function callbackLlistatDestacats(data) {
				Ti.App.vincDestacats = true;
				Ti.App.vincMeus = false;
				Ti.App.vincDescarregats = false;

				var tableViewDestacats = require('controller/llistesEscenaris/Mod_llistats');
				tableViewDestacatsInstance = new tableViewDestacats(urlBase, data);
				fonsActInd.hide();
				actInd.hide();

				winCentral.add(tableViewDestacatsInstance);

			}

		} else if (e.index == 0) {
			if (Ti.App.vincDestacats) {
				Ti.App.paraula_clau = Ti.App.paraula_clau2;
				Ti.App.nom_escenari = Ti.App.nom_escenari2;
				Ti.App.poblacio = Ti.App.poblacio2;
				Ti.App.idioma = Ti.App.idioma2;
				Ti.App.area_coneixement = Ti.App.area_coneixement2;
				Ti.App.public_objectiu = Ti.App.public_objectiu2;
				Ti.App.edat_autor = Ti.App.edat_autor2;
				Ti.App.nom_centre = Ti.App.nom_centre2;
				Ti.App.autor = Ti.App.autor2;
			}
			fonsActInd.show();
			actInd.show();

			drawer.toggleLeftWindow();
			createLlistatBiblioteca();
			function createLlistatBiblioteca() {
				if (!isAndroid) {
					filtreInstance.animate({
						top : isAndroid ? -480 * formulaHeight - 44 * formulaHeight : -480 * formulaHeight - 44,
						duration : 500
					});
				}

				filtreObert = false;
				
				botoInfoDescarregats.visible = false;
				labelEscenaris.visible = true;
				canviaTitol(biblioteca);

				filterButton.visible = true;
				
				var dataBiblioteca = require('controller/llistesEscenaris/llistats_network_Controller');
				
				if (Ti.App.entorn == 'eduloc') {
				var dataBibliotecaInstance = new dataBiblioteca(urlBase + '/mobile/scenario/library', callbackLlistatBiblioteca, actInd);

			} else {
				var dataBibliotecaInstance = new dataBiblioteca(urlBase + '/api/scenario/library/'+Ti.App.entorn+'/es', callbackLlistatBiblioteca, actInd);

			}
			

				function callbackLlistatBiblioteca(data) {
					labelEscenaris.text = data.length + escenaris2;

					Ti.App.vincDestacats = false;
					Ti.App.vincMeus = false;
					Ti.App.vincDescarregats = false;

					var tableViewBiblioteca = require('controller/llistesEscenaris/Mod_llistats');
					tableViewBibliotecaInstance = new tableViewBiblioteca(urlBase, data);
					fonsActInd.hide();
					actInd.hide();

					winCentral.add(tableViewBibliotecaInstance);

				}

			}

		} else if (e.index == 2) {
			if (Titanium.App.Properties.getString("loggedIn") == "true") {
				if (filtreInstance != null) {
					filtreInstance.animate({
						top : isAndroid ? -480 * formulaHeight - 44 * formulaHeight : -480 * formulaHeight - 44,
						duration : 500
					});
					filtreObert = false;
				}

				
				botoInfoDescarregats.visible = false;
				filterButton.visible = false;
				labelEscenaris.visible = false;
				
				canviaTitol(meus_escenaris);

				drawer.toggleLeftWindow();
				fonsActInd.show();
				actInd.show();

				var dataMisEscenarios = require('controller/llistesEscenaris/meusNetworkController');
				var dataMisEscenariosInstance = new dataMisEscenarios(urlBase + '/api/scenario/my-list', callbackMisEscenarios, actInd);

				function callbackMisEscenarios(dataMisEscenariosInstance) {

					Ti.App.vincDestacats = false;
					Ti.App.vincMeus = true;
					Ti.App.vincDescarregats = false;

					var tableViewMeus = require('controller/llistesEscenaris/Mod_llistats');
					tableViewMeusInstance = new tableViewMeus(urlBase, dataMisEscenariosInstance, actInd);
					actInd.hide();
					fonsActInd.hide();
					winCentral.add(tableViewMeusInstance);

				};

			} else {
				var alertDialog = Titanium.UI.createAlertDialog({
					title : cal_estar_loguejat,
					buttonNames : ['Ok']
				});
				alertDialog.show();
			}

		} else if (e.index == 3) {
			var arrayEscenarisOffline = Titanium.App.Properties.getList("arrayEscenarisOffline", []);
			if (arrayEscenarisOffline.length != 0) {
				botoInfoDescarregats.visible = true;
				if (filtreInstance != null) {
					filtreInstance.animate({
						top : isAndroid ? -480 * formulaHeight - 44 * formulaHeight : -480 * formulaHeight - 44,
						duration : 500
					});
					filtreObert = false;
				}

				
				canviaTitol(descarregats);
				filterButton.visible = false;

				
				labelEscenaris.visible = false;
				drawer.toggleLeftWindow();

				var arrayEscenarisOffline = Titanium.App.Properties.getList("arrayEscenarisOffline", []);

				Ti.App.vincDestacats = false;
				Ti.App.vincMeus = false;
				Ti.App.vincDescarregats = true;

				var tableViewOffline = require('controller/llistesEscenaris/Mod_llistats');
				tableViewOfflineInstance = new tableViewOffline(urlBase, arrayEscenarisOffline);
				winCentral.add(tableViewOfflineInstance);
			} else {
				var alertDialog = Titanium.UI.createAlertDialog({
					title : no_escenaris_descarregats,
					message : no_escenaris_descarregats_message,
					buttonNames : ['Ok']
				});
				alertDialog.show();
			}
		} else if (e.index == 4) {
			if (!isAndroid) {
				filtreInstance.animate({
					top : isAndroid ? -480 * formulaHeight - 44 * formulaHeight : -480 * formulaHeight - 44,
					duration : 500
				});
			}

			filtreObert = false;
			

			canviaTitol(configuracio);
			filterButton.visible = false;
			labelEscenaris.visible = false;
			
			drawer.toggleLeftWindow();
			botoInfoDescarregats.visible = false;
			if (firstTimeLogin) {
				var fitxaLogin = require('controller/llistesEscenaris/fitxaLogin');
				fitxaLoginInstance = new fitxaLogin(urlBase);
				winCentral.add(fitxaLoginInstance);

				firstTimeLogin = false;
			}
			if (fitxaLoginInstance != null) {
				fitxaLoginInstance.visible = true;
			}

		}

	});

	win.add(tableView);

	return win;

}

module.exports = llistatDrawer;
