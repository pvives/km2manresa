function botoDescarregarEscenari(win) {
	if (Titanium.Platform.osname === 'android') {
		var isAndroid = true;
	}

	var widthpantalla = Ti.Platform.displayCaps.platformWidth;
	var heightpantalla = Ti.Platform.displayCaps.platformHeight;
	var formulaWidth = widthpantalla / 320;
	var formulaHeight = heightpantalla / 480;
	var statusBarHeight = Ti.Platform.displayCaps.logicalDensityFactor * 25;

	if (Titanium.App.Properties.getString("language", "undefined") == 'ca') {
		Titanium.include('/controller/translations.js');
	} else if (Titanium.App.Properties.getString("language", "undefined") == 'es') {
		Titanium.include('/controller/translations_es.js');
	} else if (Titanium.App.Properties.getString("language", "undefined") == 'en') {
		Titanium.include('/controller/translations_en.js');
	} else if (Titanium.App.Properties.getString("language", "undefined") == 'pt') {
		Titanium.include('/controller/translations_pt.js');
	} else if (Titanium.App.Properties.getString("language", "undefined") == 'pt-PT') {
		Titanium.include('/controller/translations_pt.js');
	}
	else if (Titanium.App.Properties.getString("language", "undefined") == 'eus') {
		Titanium.include('/controller/translations_eus.js');
	} else {
		Titanium.include('/controller/translations_en.js');
	}

	if (Ti.Platform.name === 'iPhone OS') {
		var version = Ti.Platform.version;
		var result = compare("6.0", version);

	}
	if(!isAndroid){
		var module = require('de.marcelpociot.social');

	}
	
	function compare(a, b) {
		var a_elements = a.split(".");
		var b_elements = b.split(".");

		if (parseInt(a_elements[0]) == parseInt(b_elements[0])) {
			return 0;
		} else if (parseInt(a_elements[0]) > parseInt(b_elements[0])) {
			return -1;
		} else {
			return 1;
		}
	}


		var facebook = Ti.UI.createButton({
			width : isAndroid ? 40* formulaHeight : 40,
			height : isAndroid ? 40 * formulaHeight : 40,
			backgroundImage : '/images/facebook.png',
			zIndex : 10,
			color : '#3e3d40',
			top : 2 * formulaHeight,
			right : 70 * formulaWidth
		});
	

	facebook.addEventListener('click', function(e) {
		
			if (result == 0 || result == 1) {
				module.showSheet({
					service : 'facebook',
					message : win.titol,
					urls : ['http://www.eduloc.net/es/escenari/' + win.id + '/preview-iframe'],
					images : [],
					success : function(e) {
						// alert('El post s\'ha publicat correctament');
					},
					cancel : function(e) {
						// alert("User canceled tweet");
					},
					error : function(e) {
							var alertDialog = Titanium.UI.createAlertDialog({
					title : post_no_publicat_facebook,
					// message : xhr_error_message,
					buttonNames : ['ok']
				});
				alertDialog.show();
						
					}
				});
			} else {
				var shareUrl = "http://www.facebook.com/sharer.php?p[title]=" + win.titol + "&u=http://www.eduloc.net/es/escenari/" + win.id + "/preview-iframe";
				//var shareUrl='http://www.google.es';
				var browser = require('/tabInfo/MiniBrowser').MiniBrowser;
				var browserWindow = new browser({
					url : shareUrl,
					//	modal:true,
					barColor : "#000",
					modal : true
				});
				browserWindow.open();
			}
		

	});
	return facebook;
}

module.exports = botoDescarregarEscenari;
