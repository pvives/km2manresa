function botoObrirEscenari(escenariDescarregat, win, url, detallView, downloaderWindowInstance, posicioEscenariDescarregat, arrayObjectesEscenarisOffline) {

	if (Titanium.Platform.osname === 'android') {
		var isAndroid = true;
	}

	var widthpantalla = Ti.Platform.displayCaps.platformWidth;
	var heightpantalla = Ti.Platform.displayCaps.platformHeight;
	var formulaWidth = widthpantalla / 320;
	var formulaHeight = heightpantalla / 480;
	var statusBarHeight = Ti.Platform.displayCaps.logicalDensityFactor * 25;

	if (Titanium.App.Properties.getString("language", "undefined") == 'ca') {
		Titanium.include('/controller/translations.js');
	} else if (Titanium.App.Properties.getString("language", "undefined") == 'es') {
		Titanium.include('/controller/translations_es.js');
	} else if (Titanium.App.Properties.getString("language", "undefined") == 'en') {
		Titanium.include('/controller/translations_en.js');
	} else if (Titanium.App.Properties.getString("language", "undefined") == 'pt') {
		Titanium.include('/controller/translations_pt.js');
	} else if (Titanium.App.Properties.getString("language", "undefined") == 'pt-PT') {
		Titanium.include('/controller/translations_pt.js');
	} else if (Titanium.App.Properties.getString("language", "undefined") == 'eus') {
		Titanium.include('/controller/translations_eus.js');
	} else {
		Titanium.include('/controller/translations_en.js');
	}

	var infoView = Ti.UI.createView({
		width : 320 * formulaWidth,
		height : Ti.UI.SIZE,
		top : 0 * formulaHeight,
		backgroundColor : 'transparent',
		layout : 'vertical',
		left : 0 * formulaWidth
	});

	var labelTitolEscenari = Ti.UI.createLabel({
		text : win.escenariJSON.titulo,
		left : 20 * formulaWidth,
		height : Ti.UI.SIZE,
		width : 300 * formulaWidth,
		color : '#f3f2f2',
		font : {
			fontWeight : 'bold',
			fontSize : 18 + Ti.App.paddingLabels + 'sp'  ,
		},
		top : 15
	});
	infoView.add(labelTitolEscenari);

	var labelNomUsuari = Ti.UI.createLabel({
		text : win.escenariJSON.creador,
		left : 20 * formulaWidth,
		height : Ti.UI.SIZE,
		width : 300 * formulaWidth,
		color : Ti.App.colorPrincipal,
		//backgroundColor : 'green',
		font : {
			fontSize : 15 + Ti.App.paddingLabels + 'sp'  ,
		},
		top : 0
	});
	infoView.add(labelNomUsuari);

	var viewVotacioObrir = Ti.UI.createView({
		width : 320 * formulaWidth,
		height : isAndroid ? 25 * formulaHeight : 25,
		top : 0 * formulaHeight,
		backgroundColor : 'transparent',
		backgroundImage : 'none',
		left : 0 * formulaWidth
	});
	infoView.add(viewVotacioObrir);

// var formula=win.escenariJSON.valoracions.formula;
// 
	// if (formula != 0 && formula != null && formula != '') {
		// formula = formula / 2;
// 
	// }

	var votacio = Ti.UI.createView({
		width : 90 * formulaWidth,
		height : isAndroid ? 25 * formulaHeight : 25,
		top : 0 * formulaHeight,

		backgroundImage : '/images/pvotacion' + win.escenariJSON.valoracio + '.png',
		left : 15 * formulaWidth
	});
	viewVotacioObrir.add(votacio);

// var labelFormula = Ti.UI.createLabel({
					// text :formula.toFixed(1)+' ('+ win.escenariJSON.valoracions.nombrevots+' vots'+') ',
					// left : 110 * formulaWidth,
					// height : Ti.UI.SIZE,
					// width :Ti.UI.SIZE,
					// color : 'white',
// 					
					// bubbleParent : true,
// 				
					// font : {
						// // fontWeight : 'bold',
						// fontSize : 12 + Ti.App.paddingLabels + 'sp',
					// },
					// top : 5 * formulaHeight
				// });
				// viewVotacioObrir.add(labelFormula);
// 				
				// var labelValoracions = Ti.UI.createLabel({
					// text :formula.toFixed(1)+' ('+ win.escenariJSON.valoracions.nombrevots+' vots'+') ',
					// left : 110 * formulaWidth,
					// height : Ti.UI.SIZE,
					// width :Ti.UI.SIZE,
					// color : 'white',
// 					
					// bubbleParent : true,
// 				
					// font : {
						// // fontWeight : 'bold',
						// fontSize : 12 + Ti.App.paddingLabels + 'sp',
					// },
					// top : 5 * formulaHeight
				// });
				// viewVotacioObrir.add(labelValoracions);
	var botoObrirEscenari = Ti.UI.createButton({
		width : 80 * formulaWidth,
		height : isAndroid ? 25 * formulaHeight : 25,
		top : 0 * formulaHeight,
		backgroundColor : 'transparent',
		backgroundImage : 'none',
		borderRadius : 10,
		borderColor : Ti.App.colorPrincipal,
		borderWidth : isAndroid ? 4 : 2,
		title : obrir,
		color : Ti.App.colorPrincipal,
		font : {
			fontSize : '15sp',
			fontWeight : 'bold',
		},
		borderColor :Ti.App.colorPrincipal,
		right : 20 * formulaWidth
	});
	viewVotacioObrir.add(botoObrirEscenari);

	botoObrirEscenari.addEventListener('click', function(e) {

		if (Ti.App.vincDescarregats) {

			Ti.App.escenariDescarregat = true;
			obreEscenari(win.objectesEscenariOffline);
		} else {
			if (!escenariDescarregat) {
				downloaderWindowInstance.open();

				Ti.App.escenariDescarregat = false;

				var data = require('/controller/creacioEscenari/Mod_networkController');
				var dataInstance = new data(url, obreEscenari, downloaderWindowInstance);
			} else {

				Ti.App.escenariDescarregat = true;

				var escenari = arrayObjectesEscenarisOffline[posicioEscenariDescarregat];
				obreEscenari(JSON.parse(escenari), downloaderWindowInstance);
			}

		}

		function obreEscenari(data) {
			//creo el escenari

			if (!Ti.App.vincDescarregats) {
				if (downloaderWindowInstance != 'undefined') {
					downloaderWindowInstance.close();

				}
			}

			var escenariController = require('/controller/creacioEscenari/escenariController');
			new escenariController(win, data);

			detallView.animate({
				left : -320 * formulaWidth,
				duration : 500
			});

		}

	});

	return infoView;
}

module.exports = botoObrirEscenari;
