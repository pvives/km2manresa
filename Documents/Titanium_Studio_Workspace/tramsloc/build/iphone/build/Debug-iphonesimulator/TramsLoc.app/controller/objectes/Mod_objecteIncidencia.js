if (Titanium.App.Properties.getString("language", "undefined") == 'ca') {
	Titanium.include('/controller/translations.js');
} else if (Titanium.App.Properties.getString("language", "undefined") == 'es') {
	Titanium.include('/controller/translations_es.js');
} else if (Titanium.App.Properties.getString("language", "undefined") == 'en') {
	Titanium.include('/controller/translations_en.js');
} else if (Titanium.App.Properties.getString("language", "undefined") == 'pt') {
	Titanium.include('/controller/translations_pt.js');
} else if (Titanium.App.Properties.getString("language", "undefined") == 'pt-PT') {
	Titanium.include('/controller/translations_pt.js');
} 
else if (Titanium.App.Properties.getString("language", "undefined") == 'eus') {
		Titanium.include('/controller/translations_eus.js');
	} else {
	Titanium.include('/controller/translations_en.js');
}

var widthpantalla = Ti.Platform.displayCaps.platformWidth;
var heightpantalla = Ti.Platform.displayCaps.platformHeight;
var formulaWidth = widthpantalla / 320;
var formulaHeight = heightpantalla / 480;
var isAndroid = false;

if (Titanium.Platform.osname === 'android') {
	isAndroid = true;
}

var capesUrl = "http://dev-creuroja.eduloc.net/api/punt/capa";
var postUrl = "http://dev-creuroja.eduloc.net/api/punt/create";



function createFitxaIncidencia(viewEscenari, win){

	var fitxaIncidencia = Ti.UI.createView({
		width : 270 * formulaWidth,
		visible : false,
		top : 0,
		height : isAndroid ? 480 * formulaHeight - statusBarHeight : 480 * formulaHeight,
		backgroundColor : '#4d4d4d'
	});

	var fitxaIncidenciaView = null;

	var photoImage;

	/* INICI DE SECCIÓ DE BARRA SUPERIOR
	*/
	
	var barraSuperior = Ti.UI.createView({
		width : 270 * formulaWidth,
		height : isAndroid ? 44 * formulaHeight : 44,
		top : isAndroid ? 0 * formulaHeight : 0,
		backgroundColor : '#F8B225',
		right : 0 * formulaWidth
	});
	fitxaIncidencia.add(barraSuperior);

	var labelTitolPunt = Ti.UI.createLabel({
		text : crea_nou_punt,
		top : isAndroid ? 0 * formulaHeight : 0,
		left : 0 * formulaWidth,
		color : 'black',
		height : isAndroid ? 44 * formulaHeight : 44,
		textAlign : 'center',
		width : 270 * formulaWidth,
		font : {
			fontSize :  17+Ti.App.paddingLabels+'sp' 
		}
	});
	barraSuperior.add(labelTitolPunt);

	function createIncidencia(viewEscenari, win){

		var photoModule = require('/controller/objectes/Mod_seleccioFoto');
		var selectAPic = new photoModule();

		function photoCallback(image){
			addPhotoButton.visible = false;
			addPhotoView.backgroundImage = image;
			photoImage = image;
		}


		//esborro la fitxa del punt anterior i afegeixo contingut a la nova fitxa cada vegada que clico una anotacio
		if (fitxaIncidenciaView != null) {
			fitxaIncidencia.remove(fitxaIncidenciaView);
		}

		fitxaIncidenciaView = null;

		fitxaIncidenciaView = Ti.UI.createView({
			width : 270 * formulaWidth,
			top : isAndroid ? 0 * formulaHeight : 0,
			left : 0 * formulaWidth,
			height : isAndroid ? 480 * formulaHeight - statusBarHeight - 44 * formulaHeight : 480 * formulaHeight
		});

		var incidenciaScrollView = Ti.UI.createScrollView({
			height: 440,
			width: 270,
			layout: 'vertical',
			backgroundColor: '#4d4d4d',
			scrollingEnable: false,
			left: 0,
			top: 44
		});
		fitxaIncidenciaView.add(incidenciaScrollView);

		var addPhotoView = Ti.UI.createView({
			height: 102,
			width: 270,
			backgroundColor:'black'
		});
		var addPhotoButton = Ti.UI.createButton({
			height: 68,
			width: 115,
			backgroundImage:"/images/botonanadirfoto.png"
		});
		var addPhotoButtonLabel = Ti.UI.createLabel({
			height: 20,
			width:'auto',
			font:{
				fontSize:  16+Ti.App.paddingLabels+'sp' ,
				fontWeight: "bold"
			},
			top:40,
			color:'black',
			text: afegir_foto
		});
		addPhotoButton.addEventListener("click", function(){
			selectAPic.showPhotoDialog(false, photoCallback);		
		});
		addPhotoView.add(addPhotoButton);
		addPhotoButton.add(addPhotoButtonLabel);

		incidenciaScrollView.add(addPhotoView);
	/*FINAL SECCIÓ DE BARRA SUPERIOR
	*/

	/* INICI SECCIÓ DE CAIXES DE TEXT
	*/

	var titleLabel = Ti.UI.createLabel({
		height: 14,
		width:'auto',
		font:{
			fontSize:  12+Ti.App.paddingLabels+'sp' 
		},
		top:6,
		left: 15,
		color:'white',
		text: titol
	});
	incidenciaScrollView.add(titleLabel);

	var titleTextbox = Ti.UI.createTextField({
		borderStyle: Ti.UI.INPUT_BORDERSTYLE_ROUNDED,
		top: 0,
		font:{
			fontSize: '12sp'
		},
		width: 240, 
		height: 30
	});
	incidenciaScrollView.add(titleTextbox);

	var addTextLabel = Ti.UI.createLabel({
		height: 14,
		width:'auto',
		font:{
			fontSize: 12+Ti.App.paddingLabels+'sp' 
		},
		top:6,
		left: 15,
		color:'white',
		text: afegir_text
	});
	incidenciaScrollView.add(addTextLabel);

	if (Ti.Platform.name === 'iPhone OS'){
		var send = Titanium.UI.createButton({
			title : done,
			style : Titanium.UI.iPhone.SystemButtonStyle.DONE,
		});

		var cancel = Titanium.UI.createButton({
			systemButton : Titanium.UI.iPhone.SystemButton.CANCEL
		});

		var flexSpace = Titanium.UI.createButton({
			systemButton : Titanium.UI.iPhone.SystemButton.FLEXIBLE_SPACE
		});
		var lastText;
		var addTextbox = Ti.UI.createTextArea({
			borderRadius: 10,
			suppressReturn: false,
			keyboardToolbar : [cancel, flexSpace, flexSpace, flexSpace, send],
			top: 0,
			width: 240, 
			height: 80
		});
		incidenciaScrollView.add(addTextbox);
		addTextbox.addEventListener('focus', function(e){
			lastText = e.value;
		});
		addTextbox.addEventListener('blur', function(e){
			//tabfotos.scrollTo(0,0);
			//tabfotos.scrollingEnable = false;
		});
		send.addEventListener('click', function(e){
			addTextbox.blur();
		});
		cancel.addEventListener('click', function(e){
			addTextbox.value = lastText;
			addTextbox.blur();
		});
	}else{
		var addTextbox = Ti.UI.createTextArea({
			borderRadius: 10,
			suppressReturn: false,
			top: 0,
			width: 240, 
			height: 90
		});
		incidenciaScrollView.add(addTextbox);
		addTextbox.addEventListener('blur', function(e){
			//tabfotos.setScrollingEnable = false;
		});
	}

	/* FINAL DE SECCIÓ DE CAIXES DE TEXT
	*/

	/* INICI DE SECCIÓ DE PICKERS
	*/

	var capaLabel = Ti.UI.createLabel({
		height: 14,
		width:'auto',
		font:{
			fontSize:  12+Ti.App.paddingLabels+'sp' 
		},
		top:6,
		left: 15,
		color:'white',
		text: capa
	});
	incidenciaScrollView.add(capaLabel);
	var capaPicker = Ti.UI.createButton({
		width:240,  
		height:30
	});
	capaPicker.addEventListener("click", function(e){
		if(isAndroid)
		{

		}
		else
		{
			capaPickerPicker.animate({bottom: 0});
			capaPickerView.animate({bottom: 208});
		}
	});
	incidenciaScrollView.add(capaPicker);
	var capaButtonLabel = Ti.UI.createLabel({
		height: 30,
		left: 15,
		width:'auto',
		font:{
			fontSize:  12+Ti.App.paddingLabels+'sp' ,
			fontWeight: 'bold'
		},
		color:'black',
		text: escull_capa
	});
	capaPicker.add(capaButtonLabel);

	var recursLabel = Ti.UI.createLabel({
		height: 14,
		width:'auto',
		font:{
			fontSize: 12+Ti.App.paddingLabels+'sp' 
		},
		top:6,
		left: 15,
		color:'white',
		text: recurs_servei
	});
	incidenciaScrollView.add(recursLabel);

	var recursPicker = Ti.UI.createButton({
		width:240,  
		height:30
	});
	recursPicker.addEventListener("click", function(e){
		categoriaPicker.animate({bottom: 0});
		categoriaPickerView.animate({bottom: 208});
	});
	var recursButtonLabel = Ti.UI.createLabel({
		height: 30,
		left: 15,
		width:'auto',
		font:{
			fontSize: 12+Ti.App.paddingLabels+'sp' ,
			fontWeight: 'bold'
		},
		color:'black',
		text: escull_recurs_servei
	});
	recursPicker.add(recursButtonLabel);
	incidenciaScrollView.add(recursPicker);

	var publishButton = Ti.UI.createButton({
		height: 42,
		width: 240,
		top:15,
		backgroundImage: "/images/botonpublicar.png"
	});
	publishButton.addEventListener("click", function(e){
		getPosition(publishPointToServer);
	});
	var publishButtonLabel = Ti.UI.createLabel({
		height: 42,
		width:240,
		font:{
			fontSize:  12+Ti.App.paddingLabels+'sp' 
		},
		textAlign: 'center',
		color:'white',
		text: publicar
	});
	publishButton.add(publishButtonLabel);

	incidenciaScrollView.add(publishButton);

	fitxaIncidencia.add(fitxaIncidenciaView);

	var capaArray = [];
	var categoriaArray = [];
	var capaRowArray = [];
	var categoriaRowArray = [];

	var capaTitle, capaId, categoriaTitle, categoriaId, capaPos;
	

	if(isAndroid)
	{
		var capaPickerPicker = Ti.UI.createPicker({
			top: 50
		});
	}
	else
	{

		var column1 = Ti.UI.createPickerColumn({
		});

		var capaPickerPicker = Ti.UI.createPicker({
			bottom: -224,
            selectionIndicator:true,
            height:'auto',
            columns: [column1]
		});
		var capaPickerView = Ti.UI.createView({
			height: 50,
			bottom: -50,
			width: 320,
			left: 0
		});
		var capaPickerViewButton = Ti.UI.createButton({
			height: 44,
			width: 100,
			top: 3,
			right: 15
		});
		capaPickerViewButton.addEventListener("click", function(e){
			// alert("hola");
		});
		capaPickerView.add(capaPickerViewButton);

		var done2 =  Titanium.UI.createButton({
                title:'Done',
                style:Titanium.UI.iPhone.SystemButtonStyle.DONE
        });
        done2.addEventListener("click", function(e){
        	capaPickerView.animate({bottom: -50});
        	capaPickerPicker.animate({bottom: -224});
        	capaId = capaPickerPicker.getSelectedRow(0).id;
        	capaTitle = capaPickerPicker.getSelectedRow(0).title;
        	capaPos = capaPickerPicker.getSelectedRow(0).pos;
        	capaButtonLabel.setText(capaTitle);
        	populateCategoriesPicker(capaPos);
        });

        var spacer2 =  Titanium.UI.createButton({
                systemButton:Titanium.UI.iPhone.SystemButton.FLEXIBLE_SPACE
        });

        var toolbar2 =  Titanium.UI.iOS.createToolbar({
                top:0,
                items:[spacer2,done2]
        });
        capaPickerView.add(toolbar2);
        win.add(capaPickerView);

        var categoriaPickerColumn = Ti.UI.createPickerColumn({});

        var categoriaPicker = Ti.UI.createPicker({
        	bottom: -224,
            selectionIndicator:true,
            height:'auto',
            columns: [categoriaPickerColumn]
        });

        var done3 =  Titanium.UI.createButton({
                title:'Done',
                style:Titanium.UI.iPhone.SystemButtonStyle.DONE
        });
        done3.addEventListener("click", function(e){
        	categoriaTitle = categoriaPicker.getSelectedRow(0).title;
        	categoriaId = categoriaPicker.getSelectedRow(0).id;
        	categoriaPickerView.animate({bottom: -50});
        	categoriaPicker.animate({bottom: -224});
        	//recursLabel.setText(categoriaTitle);
        	recursButtonLabel.setText(categoriaTitle);

        });

        var spacer3 =  Titanium.UI.createButton({
                systemButton:Titanium.UI.iPhone.SystemButton.FLEXIBLE_SPACE
        });

        var toolbar3 =  Titanium.UI.iOS.createToolbar({
                top:0,
                items:[spacer3,done3]
        });

        var categoriaPickerView = Ti.UI.createView({
			height: 50,
			bottom: -50,
			width: 320,
			left: 0
		});
        categoriaPickerView.add(toolbar3);
        win.add(categoriaPickerView);
	}
	win.add(capaPickerPicker);
	win.add(categoriaPicker);

	var data = require('/controller/creacioEscenari/Mod_networkController');
	var dataInstance = new data(capesUrl, populatePickers);

	

	function populatePickers(data)
	{
		// DEMANEM LES DADES AL SERVIDOR I RECUPEREM CAPES I CATEGORIES
		for(var i=0; i<data.length; i++)
		{
			var objectCapaToPush = 
			{
				id: data[i]["id"],
				name: data[i]["nom"],
				pos: i
			};
			capaArray.push(objectCapaToPush);

			categoriaArray.push(data[i]["categories"]);

			capaRowArray[i] = Ti.UI.createPickerRow({title: data[i]["nom"], id:data[i]["id"], pos: i});

			column1.addRow(capaRowArray[i]);
		}

		capaPickerPicker.setColumns([column1]);
		capaPickerPicker.reloadColumn(column1);
	}

	function populateCategoriesPicker(index)
	{
		for(var i=0; i<categoriaRowArray.length; i++){
			categoriaPickerColumn.removeRow(categoriaRowArray[i]);
		}
		categoriaRowArray = [];

		for(var i=0; i<categoriaArray[index].length; i++){
			var categoryRow = Ti.UI.createPickerRow({
				title: categoriaArray[index][i]["nom"],
				id: categoriaArray[index][i]["id"]
			});
			categoriaRowArray.push(categoryRow);
			categoriaPickerColumn.addRow(categoryRow);
		}
		categoriaPicker.reloadColumn(categoriaPickerColumn);
	}
	
	function getPosition(callback){
		Titanium.Geolocation.getCurrentPosition(function(e){
			if(!e.error)
			{
				var lat = e.coords.latitude;
				var lon = e.coords.longitude;
				callback(lat, lon);
			}
			else
			{
				alert(alerta_current_location_message);
			}
			
		});
	}

	function publishPointToServer(lat, lon){
		var dataPost = {
			idescenari: win.id,
			title: titleTextbox.text,
			text: addTextbox.text,
			capa: capaId,
			categoria: categoriaId,
			latitude: lat,
			longitude: lon,
			image: photoImage
		};
		var postNetworkController = require('/controller/creacioEscenari/Mod_PostNetworkController');
		var postNetworkInstance = new postNetworkController(postUrl, dataPost, function(e){
			win.remove(viewEscenari);
			var escenariController = require('/controller/creacioEscenari/escenariController');
			new escenariController(win);
		});
	}

	/* FINAL DE SECCIÓ DE PICKERS
	*/

}

fitxaIncidencia.createIncidencia = createIncidencia;


return fitxaIncidencia;

}


module.exports = createFitxaIncidencia;