function createFitxaVideo(dataPunt, urlBase) {

	//Crea les fitxes de tots els objectes
	var widthpantalla = Ti.Platform.displayCaps.platformWidth;
	var heightpantalla = Ti.Platform.displayCaps.platformHeight;
	var formulaWidth = widthpantalla / 320;
	var formulaHeight = heightpantalla / 480;

	if (Titanium.Platform.osname === 'android') {
		var isAndroid = true;
	}
	var statusBarHeight = Ti.Platform.displayCaps.logicalDensityFactor * 25;

	var fitxaVideo = Ti.UI.createView({
		width : 270 * formulaWidth,
		height : isAndroid ? 480 * formulaHeight - statusBarHeight - 44 * formulaHeight - 44 * formulaHeight : 480 * formulaHeight - 44-44,
		left : 0 * formulaWidth,
		top : 44 ,
		visible:false,
		backgroundColor : 'transparent'

	});

	var viewVideo = Ti.UI.createWebView({
		enableZoomControls : true,
		visible : true,
		left : 0 * formulaWidth,
		height : isAndroid ? 480 * formulaHeight - statusBarHeight - 44 * formulaHeight - 44 * formulaHeight : 480 * formulaHeight - 44- 44,
		top : 0,
		url : dataPunt.galeria.pathvideo,
		width : 270 * formulaWidth,
		showScrollbars : false
	});
	fitxaVideo.add(viewVideo);

	isAndroid ? viewVideo.reload() : viewVideo.repaint();

	return fitxaVideo;

}

module.exports = createFitxaVideo;
