

function createAudioController (audioUrl, buttonInstance){
if(!Ti.App.escenariDescarregat){
	
        var audioController = {};

        var audio =  Ti.Media.createAudioPlayer({ 
                url: audioUrl
        });
        audio.addEventListener("change", function(e){        
        	if(!e.source.playing){
        		        		 buttonInstance.backgroundImage = "/images/playaudio.png";

        	}
        	else{
        		        		        		 buttonInstance.backgroundImage = "/images/playaudioon.png";

        	}
                       
        });
        audioController.audio = audio;

        function click (){
                if(audio.playing){
                        audio.pause();
                       
                }
                else{
                        audio.play();
                }
        }
        audioController.click = click;

        function stop(){
                audio.stop();
        }
        audioController.stop = stop;

}
else{
	  var audioController = {};
	  var audio = Ti.Media.createSound({
	  	                 url: audioUrl

         });

       audio.addEventListener("complete", function(e){           

                       buttonInstance.backgroundImage = "/images/playaudio.png";

         });

         audioController.audio = audio;

 

         function click (){

                 if(audio.playing){

                         audio.pause();

                       buttonInstance.backgroundImage = "/images/playaudio.png";

                 }

                 else{

                         audio.play();

                     buttonInstance.backgroundImage = "/images/playaudioon.png";

                 }

         }

         audioController.click = click;


}
        return audioController;
}

module.exports = createAudioController;