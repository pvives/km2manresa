function bruixola(dataPista, urlBase, passoPistaAResposta) {
	var dist = 1;
	var updatedHeading = 0;
	var heading = 0;
	var ok = false;
	var currBearing = 0;
	var horizAngle = 0;
	var angle = 0;
	var currentHeading = 0;
	var distanceToShow;
	var pistaActual = 0;
	var locations;
	var pistaResolta = false;
	if (!isAndroid) {
		Ti.Geolocation.preferredProvider = Ti.Geolocation.PROVIDER_GPS;
		Ti.Geolocation.accuracy = Ti.Geolocation.ACCURACY_NEAREST_TEN_METERS;
		Ti.Geolocation.distanceFilter = 4;
	} else {
		Ti.Geolocation.preferredProvider = Ti.Geolocation.PROVIDER_GPS;
		Ti.Geolocation.accuracy = Ti.Geolocation.ACCURACY_BEST;
		Ti.Geolocation.distanceFilter = 3;
		Ti.Geolocation.frequency = 1;
	}
var widthpantalla = Ti.Platform.displayCaps.platformWidth;
var heightpantalla = Ti.Platform.displayCaps.platformHeight;
var formulaWidth = widthpantalla / 320;
var formulaHeight = heightpantalla / 480;
var statusBarHeight = isAndroid ? Ti.Platform.displayCaps.logicalDensityFactor * 25 : 20;

	var currLocation = {
		lat : 0,
		lng : 0,
		alt : 0
	};

	var viewBruixola = Ti.UI.createView({
		backgroundColor : '#000',
		width : 270 * formulaWidth,
		height : isAndroid ? 220 * formulaHeight - 44 * formulaHeight : 220 - 44,
		top : isAndroid ? 44 * formulaHeight : 44
	});

	var bruixola = Ti.UI.createView({
		width : 125 * formulaWidth,
		backgroundImage : '/images/brujula.png',
		height : isAndroid ? 125 * formulaWidth : 125,
		right : 72 * formulaWidth,
		top : isAndroid ? 0 * formulaHeight : 0

	});
	viewBruixola.add(bruixola);

	var agulla = Ti.UI.createView({
		width : 125 * formulaWidth,
		//center : centre,
		//backgroundColor:'red',
		backgroundImage : '/images/agujaroja.png',
		height : isAndroid ? 125 * formulaWidth : 125,
		left : 0 * formulaWidth,
		bottom : isAndroid ? 0 * formulaHeight : 0,

	});
	bruixola.add(agulla);

	var labeldist = Ti.UI.createLabel({
		text : calculant_distancia,
		top : isAndroid ? 126 * formulaHeight : 126,
		zIndex : 2,
		left : 0 * formulaWidth,
		height : 'auto',
		color : 'white',
		visible : true,
		//backgroundColor:'red',
		textAlign : 'center',
		width : 270 * formulaWidth,
		font : {
			fontSize : 20+Ti.App.paddingLabels+'sp' 
		}
	});
	viewBruixola.add(labeldist);

	if (Titanium.Geolocation.hasCompass == true) {
		var timer = setInterval(function() {

			updateCompass();

		}, 80);
	}

	//event listeners de location i heading

	// Titanium.Geolocation.getCurrentPosition(function(e) {
	// if (e.error) {
	// Titanium.UI.createAlertDialog({
	// title : alerta_current_location_title,
	// message : alerta_current_location_message
	// }).show();
	// } else {
	//
	// locations = {
	// lat : dataPista.lat,
	// lng : dataPista.lon,
	// alt : 0
	// };
	//
	// currLocation = {
	// lat : e.coords.latitude,
	// lng : e.coords.longitude,
	// alt : e.coords.altitude
	// };
	// dist = Distance(currLocation, locations);
	// dist = parseFloat(dist);
	// if (dist <= 5) {
	// distanceToShow = dist.toFixed(3);
	// distanceToShow = distanceToShow * 1000;
	// distanceToShow = distanceToShow.toFixed(1);
	// distanceToShow = parseInt(distanceToShow);
	// distanceToShow += 'm';
	// } else {
	// distanceToShow = dist.toFixed(2);
	// distanceToShow = parseInt(distanceToShow);
	// distanceToShow += ' Km';
	// }
	// labeldist.text = distanceToShow;
	//
	// horizAngle = Bearing(currLocation, locations);
	//
	// // if (dist <= 0.01) {
	// if (dist <= 2000) {
	// triggerHotspot();
	//
	// }
	//
	// }
	// });

	Ti.Geolocation.getCurrentHeading(function(e) {
		if (e.error) {
			//	currentHeading.text = 'error: ' + e.error;
			alert("Este dispositivo no dispone de brújula.");
			return;
		}

		var x = e.heading.x;
		var y = e.heading.y;
		var z = e.heading.z;
		var magneticHeading = e.heading.magneticHeading;
		var accuracy = e.heading.accuracy;
		var trueHeading = e.heading.trueHeading;
		var timestamp = e.heading.timestamp;
		heading = e.heading.magneticHeading;
		//FilterK(e.heading.magneticHeading, heading, K);
		currBearing = toRad(heading);
		updatedHeading = Math.round(heading);
		Titanium.API.info('geo - current heading: ' + trueHeading);

	});


	Titanium.Geolocation.addEventListener('location', function(e) {
		if (e.error) {
			// Titanium.UI.createAlertDialog({
			// title : alerta_current_location_title,
			// message : alerta_current_location_message
			// }).show();
		} else {

			if (dataPista.lat) {
				locations = {
					lat : dataPista.lat,
					lng : dataPista.lon,
					alt : 0
				};
			}

			currLocation = {
				lat : e.coords.latitude,
				lng : e.coords.longitude,
				alt : e.coords.altitude
			};
			dist = Distance(currLocation, locations);
			horizAngle = Bearing(currLocation, locations);

			if (dist <= 5) {
				distanceToShow = dist.toFixed(3);
				distanceToShow = distanceToShow * 1000;
				distanceToShow = distanceToShow.toFixed(1);
				distanceToShow = parseInt(distanceToShow);
				distanceToShow += ' m';
			} else {
				distanceToShow = dist.toFixed(2);
				distanceToShow = parseInt(distanceToShow);
				distanceToShow += ' Km';
			}

			labeldist.text = distanceToShow;

			//COMPROVEM LA POSICIÓ ACTUAL I  MIREM SI ESTEM DINS EL RADI D'ACCIÓ
			if (dist <= 0.01) {

				triggerHotspot();

			}

		}
	});

	Titanium.Geolocation.addEventListener('heading', function(e) {
		if (e.error) {
			updatedHeading.text = 'error: ' + e.error;
			return;
		}
		heading = e.heading.magneticHeading;
		currBearing = toRad(heading);
		updatedHeading = Math.round(heading);
		ok = true;

	});

	//funcions
	function updateCompass() {

		if (toDeg(-currBearing) <= 180) {
			angle = (toDeg(-currBearing) + toDeg(horizAngle)) % 360;
		} else {
			angle = (toDeg(horizAngle) - (360 - toDeg(-currBearing))) % 360;
		}

		var t = Ti.UI.create2DMatrix();
		t = t.rotate(angle);
		agulla.transform = t;

	}

	function triggerHotspot() {

		if (pistaResolta == false) {
			pistaResolta = true;
			if (!isAndroid) {
				Ti.Media.vibrate();
			} else {
				Ti.Media.vibrate([0, 500, 100, 500, 100, 500]);
			}
			clearInterval(timer);
			passoPistaAResposta();
			labeldist.visible=false;
			agulla.visible=false;

		}
	}

	function toRad(x) {
		return x * Math.PI / 180;
	}

	function toDeg(x) {
		return ((x * 180 / Math.PI) + 360) % 360;
	}

	function Distance(point1, point2) {
		var R = 6371;
		// km
		var dLat = toRad(point2.lat - point1.lat);
		var dLon = toRad(point2.lng - point1.lng);
		var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.cos(toRad(point1.lat)) * Math.cos(toRad(point2.lat)) * Math.sin(dLon / 2) * Math.sin(dLon / 2);
		var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
		return R * c;
	}

	function Bearing(point1, point2) {
		Titanium.API.debug('  Bearing Fnct');
		var lat1 = point1.lat * Math.PI / 180;
		var lat2 = point2.lat * Math.PI / 180;
		var dlng = (point2.lng - point1.lng) * Math.PI / 180;
		var y = Math.sin(dlng) * Math.cos(lat2);
		var x = Math.cos(lat1) * Math.sin(lat2) - Math.sin(lat1) * Math.cos(lat2) * Math.cos(dlng);
		var brng = Math.atan2(y, x);
		Titanium.API.debug('  brng: ' + brng);
		return brng;
	}

	return viewBruixola;
}

module.exports = bruixola;
