function createAnnotations(data, callback,arrayRows) {
	//creo anotacions
	//cal definir els camps de la anotacio per cada projecte

	var annotations = [];
	if (Titanium.Platform.osname === 'android') {
		var isAndroid = true;
	}
var MapModule=require('ti.map');
	if (data != null) {


		for (var i = 0; i < data.length; i++) {

			//// Codi propi del projecte eduloc o geoemotion ////
			if (data[i].tipus == 'punt') {
				var anotacio = MapModule.createAnnotation({
					latitude : data[i].lat,
					longitude : data[i].lon,
					tipus : data[i].tipus,
					descripcio : data[i].descripcio,
					  rightButton : isAndroid ? 'flechaburbuja.png' : 'flechaburbuja.png',
										image : '/images/iconesMapa/'+data[i].capaprincipal.color+data[i].categoriaprincipal.icona,
					title : data[i].nom,
					myid : data[i].id,
					annotationData : data[i]
				});
				for (var j = 0; j < arrayRows.length; j++) {
				
					if(arrayRows[j].titol==data[i].capaprincipal.nom&&arrayRows[j].activat){
							
					
						for (var z = 0; z < arrayRows[j].rowsCategories.length; z++) {
							
						if(arrayRows[j].rowsCategories[z].titol==data[i].categoriaprincipal.nom &&arrayRows[j].rowsCategories[z].categoriaActivada){
							
						annotations.push(anotacio);
						}
						}
						
					}
					
					
				}
				
				
				if (isAndroid) {
					if ((Titanium.Platform.displayCaps.density == "low") || (Titanium.Platform.displayCaps.density == "medium")) {
						anotacio.image = '/iconesMapa/mdpi/punto.png';
						anotacio.rightButton = '/iconesMapa/mdpi/flechaburbuja.png';
					} else if (Titanium.Platform.displayCaps.density == "high") {
						anotacio.image =  '/iconesMapa/hdpi/punto.png';
						anotacio.rightButton = '/iconesMapa/hdpi/flechaburbuja.png';
					} else if (Titanium.Platform.displayCaps.density == "xhigh") {
						anotacio.image =  '/iconesMapa/xhdpi/punto.png';
						
						anotacio.rightButton = '/iconesMapa/xhdpi/flechaburbuja.png';
					}
				}

			} else if (data[i].tipus == 'jocpista') {
				//es mostra nomes la primera pista del joc

				var anotacio = MapModule.createAnnotation({
					latitude : data[i].lat,
					longitude : data[i].lon,
					rightButton : isAndroid ? '/images/flechaburbuja.png' : '/images/flechaburbuja.png',
					tipus : data[i].tipus,
					image : '/images/iconesMapa/pista.png',
					title : data[i].nom,
					myid : data[i].id,
					annotationData : data[i]
				});
				annotations.push(anotacio);

				// if (isAndroid) {
					// if ((Titanium.Platform.displayCaps.density == "low") || (Titanium.Platform.displayCaps.density == "medium")) {
						// anotacio.image = '/iconesMapa/mdpi/pista.png';
												// anotacio.rightButton = '/iconesMapa/mdpi/flechaburbuja.png';
// 
					// } else if (Titanium.Platform.displayCaps.density == "high") {
						// anotacio.image = '/iconesMapa/hdpi/pista.png';
												// anotacio.rightButton = '/iconesMapa/hdpi/flechaburbuja.png';
// 
					// } else if (Titanium.Platform.displayCaps.density == "xhigh") {
						// anotacio.image = '/iconesMapa/xhdpi/pista.png';
												// anotacio.rightButton = '/iconesMapa/xhdpi/flechaburbuja.png';
// 
					// }
				// }

			} else if (data[i].tipus == 'maquina') {

				var anotacio = MapModule.createAnnotation({
					latitude : data[i].lat,
					longitude : data[i].lon,
					tipus : data[i].tipus,
					rightButton : isAndroid ? '/images/flechaburbuja.png' : '/images/flechaburbuja.png',
					image : '/images/iconesMapa/tiempo.png',
					title : data[i].nom,
					myid : data[i].id,
					annotationData : data[i]
				});
				annotations.push(anotacio);
				// if (isAndroid) {
					// if ((Titanium.Platform.displayCaps.density == "low") || (Titanium.Platform.displayCaps.density == "medium")) {
						// anotacio.image = '/iconesMapa/mdpi/tiempo.png';
												// anotacio.rightButton = '/iconesMapa/mdpi/flechaburbuja.png';
// 
					// } else if (Titanium.Platform.displayCaps.density == "high") {
						// anotacio.image =  '/iconesMapa/hdpi/tiempo.png';
												// anotacio.rightButton = '/iconesMapa/hdpi/flechaburbuja.png';
// 
					// } else if (Titanium.Platform.displayCaps.density == "xhigh") {
						// anotacio.image =  '/iconesMapa/xhdpi/tiempo.png';
												// anotacio.rightButton = '/iconesMapa/xhdpi/flechaburbuja.png';
// 
					// }
				// }
			} else if (data[i].tipus == 'trivial') {

				var anotacio = MapModule.createAnnotation({
					latitude : data[i].latitud,
					longitude : data[i].longitud,
					tipus : data[i].tipus,
					rightButton : isAndroid ? '/images/flechaburbuja.png' : '/images/flechaburbuja.png',
					image : '/images/iconesMapa/pregunta.png',
					title : data[i].nom,
					myid : data[i].id,
					annotationData : data[i]
				});
				annotations.push(anotacio);
				// if (isAndroid) {
					// if ((Titanium.Platform.displayCaps.density == "low") || (Titanium.Platform.displayCaps.density == "medium")) {
						// anotacio.image =  '/iconesMapa/mdpi/pregunta.png';
												// anotacio.rightButton = '/iconesMapa/mdpi/flechaburbuja.png';
// 
					// } else if (Titanium.Platform.displayCaps.density == "high") {
						// anotacio.image =  '/iconesMapa/hdpi/pregunta.png';
												// anotacio.rightButton = '/iconesMapa/hdpi/flechaburbuja.png';
// 
					// } else if (Titanium.Platform.displayCaps.density == "xhigh") {
						// anotacio.image =  '/iconesMapa/xhdpi/pregunta.png';
												// anotacio.rightButton = '/iconesMapa/xhdpi/flechaburbuja.png';
// 
					// }
				// }
			}

		}

		//// Fi codi del projecte eduloc o geoemotion ////

	}
	
	//crida a centerMap
	callback(annotations);

	return annotations;

}

module.exports = createAnnotations;

