function llistat(urlBase, dataEscenaris) {

	if (Titanium.App.Properties.getString("language", "undefined") == 'ca') {
		Titanium.include('/controller/translations.js');
	} else if (Titanium.App.Properties.getString("language", "undefined") == 'es') {
		Titanium.include('/controller/translations_es.js');
	} else if (Titanium.App.Properties.getString("language", "undefined") == 'en') {
		Titanium.include('/controller/translations_en.js');
	} else if (Titanium.App.Properties.getString("language", "undefined") == 'pt') {
		Titanium.include('/controller/translations_pt.js');
	} else if (Titanium.App.Properties.getString("language", "undefined") == 'pt-PT') {
		Titanium.include('/controller/translations_pt.js');
	} else if (Titanium.App.Properties.getString("language", "undefined") == 'eus') {
		Titanium.include('/controller/translations_eus.js');
	} else {
		Titanium.include('/controller/translations_en.js');
	}

var StringBuilder = function() {
 
    
　　　this.shrinkText = function(str, ellipsize) {
　　　　　　var res = str;
　　　　　　var work = new String(res);
　　　　　　if (work.length > ellipsize) {
　　　　　　　　　var res = work.substring(0, ellipsize);
　　　　　　　　　res = res + "...";
　　　　　　}
　　　return res;
　　　}
}
var stringBuilder = new StringBuilder();



	var widthpantalla = Ti.Platform.displayCaps.platformWidth;
	var heightpantalla = Ti.Platform.displayCaps.platformHeight;
	var formulaWidth = widthpantalla / 320;
	var formulaHeight = heightpantalla / 480;

	var primeraVegada;
	var xhr = Titanium.Network.createHTTPClient();
	var statusBarHeight = isAndroid ? Ti.Platform.displayCaps.logicalDensityFactor * 25 : 20;

	if (Titanium.Platform.osname === 'android') {
		var isAndroid = true;
	}
	Ti.App.clicat = false;

	var viewOrdenacioInstance = null;
	var viewOrdenacio = require('/controller/llistesEscenaris/ordenacio');
	var refreshLlista = require('/controller/llistesEscenaris/refreshLlistat');
	var table = null;
	var viewLlistat = Ti.UI.createView({
		width : 320 * formulaWidth,
		backgroundColor : '#353535',

		height : isAndroid ? 480 * formulaHeight - 44 * formulaHeight - statusBarHeight : 480 * formulaHeight - 44 - statusBarHeight,
		top : isAndroid ? 44 * formulaHeight : 44,
		left : 0 * formulaWidth

	});
	if (iOS7) {
		viewLlistat.top = 44 + 20;
	}
	creoLlista(dataEscenaris);

	function creoLlista(dataEscenaris) {

		var arrayRows = [];
		var conexio = false;
		var firsttime = true;
		var firstFocus = true;
		var clicat = false;
		var primerFocus = true;
	
		var paperera;
		var arrayObjectesEscenarisOffline = Titanium.App.Properties.getList("arrayObjectesEscenarisOffline", []);
		var idestacats = 0;
		var imatgeEscenari;
		var firstTime = true;
if(dataEscenaris.length!=0){
		for (var i = 0; i < dataEscenaris.length; i++) {
 if(i<100){
			if (Ti.App.vincDescarregats) {

				dataEscenaris[i] = JSON.parse(dataEscenaris[i]);

			}

			//Afegeixo escenaris. Si es llista destacats i lescenari es destacat el poso a la llista
			if (Ti.App.vincDestacats == false || Ti.App.vincDestacats == true && dataEscenaris[i].destacat == true) {

				var row = Ti.UI.createTableViewRow({
					height : isAndroid ? 90 * formulaWidth : 90,
					width : 320 * formulaWidth,
					creador : dataEscenaris[i].creador,
					ambit : dataEscenaris[i].ambit,
					idioma : dataEscenaris[i].idioma,
					borderRadius : 0,
					selectedBackgroundColor:Ti.App.colorPrincipal,
			backgroundSelectedColor:Ti.App.colorPrincipal,
					objectesEscenariOffline : Ti.App.vincDescarregats ? arrayObjectesEscenarisOffline[i] : null,
					escenariJSON : dataEscenaris[i],
					bubbleParent : true,
					borderWidth : 0,
					urlBase : urlBase,
					id : dataEscenaris[i].id,
					name : dataEscenaris[i].titulo,
					descripcio : dataEscenaris[i].descripcio,
					imatge : dataEscenaris[i].thumb560x280!=null?dataEscenaris[i].thumb560x280:'/images/imagendefectoficha.png',
					valoracio : dataEscenaris[i].valoracio,
					backgroundSelectedColor:Ti.App.colorPrincipal,
					// selectedBackgroundColor:Ti.App.colorPrincipal,
					backgroundImage : '/images/lista1.png',
					className : 'default'

				});

				if (Ti.App.vincDescarregats) {

					paperera = Ti.UI.createView({
						height : isAndroid ? 30 * formulaHeight : 30,
						width : 30 * formulaWidth,
						zIndex : 10,
						backgroundImage : '/images/paperera.png',
						layout : 'vertical',
						i : i,
						escenariOffline : arrayObjectesEscenarisOffline[i],
						json : dataEscenaris[i],
						id : dataEscenaris[i].id,
						bubbleParent : false,
						top : 0 * formulaHeight,
						right : 50 * formulaWidth

					});
					row.add(paperera);

					paperera.addEventListener('click', function(e) {
						//esborro els fitxers de memoria
						var deleteController = require('/controller/offline/DownloaderController');

						deleteController.esborrarEscenari(e.source.escenariOffline);

						//elimino de array properties objectes
						var arrayObjectesEscenarisOffline = Titanium.App.Properties.getList("arrayObjectesEscenarisOffline", []);

						arrayObjectesEscenarisOffline.splice(e.source.i, 1);
						Titanium.App.Properties.setList("arrayObjectesEscenarisOffline", arrayObjectesEscenarisOffline);

						//elimino de array properties escenaris
						var arrayEscenarisOffline = Titanium.App.Properties.getList("arrayEscenarisOffline", []);
						arrayEscenarisOffline.splice(e.source.i, 1);
						Titanium.App.Properties.setList("arrayEscenarisOffline", arrayEscenarisOffline);

						//recarrego la llista
						var arrayEscenarisOffline = Titanium.App.Properties.getList("arrayEscenarisOffline", []);

						creoLlista(arrayEscenarisOffline);
					});
				}

				var layout = Ti.UI.createView({
					height : isAndroid ? 90 * formulaWidth : 90,
					width : 320 * formulaWidth,
					zIndex : 3,
selectedBackgroundColor:Ti.App.colorPrincipal,
			backgroundSelectedColor:Ti.App.colorPrincipal,
					backgroundColor : 'transparent',
					layout : 'vertical',
					bubbleParent : true,
					top : 0 * formulaHeight,
					left : 0 * formulaWidth

				});
				row.add(layout);

				if (!Ti.App.vincDestacats) {
					if (i % 2 != 0) {
						row.backgroundImage = '/images/lista2.png';

					}

				} else {
					if (idestacats % 2 != 0) {
						row.backgroundImage = '/images/lista2.png';

					}
					idestacats++;
				}
				// var formula=dataEscenaris[i].valoracions.formula;
				// if(formula!=0&&formula!=null&&formula!=''){
						// formula=formula/2;
// 				
// 				
				// }
			
				
					var viewVotacio = Ti.UI.createView({
						width : Ti.UI.SIZE,
						height : isAndroid ? 25 * formulaWidth : 25,
						top : -2 * formulaHeight,

						left : 90 * formulaWidth
					});
					row.add(viewVotacio);
					
				if (Ti.App.ordenacio == 'valoracio_mitjana') {
					var votacio = Ti.UI.createView({
						width : 90 * formulaWidth,
						height : isAndroid ? 25 * formulaWidth : 25,
						bottom : 0 * formulaHeight,

	 // backgroundImage : '/images/pvotacion' +Math.round(formula) + '.png',
	 						left : 0 * formulaWidth
					});
					viewVotacio.add(votacio);

				} else {
					var dataActualitzacio = dataEscenaris[i].modified.date;
					dataActualitzacio = dataActualitzacio.split(" ");
					dataActualitzacio = dataActualitzacio[0];
					dataActualitzacio = dataActualitzacio.split("-");

					var labelActualitzacio = Ti.UI.createLabel({
						// text : dataActualitzacio[2] + "\/" + dataActualitzacio[1] + "\/" + dataActualitzacio[0],
						left : 6 * formulaWidth,
						height : isAndroid ? 20 * formulaHeight : 20,
						width : 300 * formulaWidth,
						color : '#55c3ec',
						//backgroundColor : 'green',
						font : {
							fontSize : 14 + Ti.App.paddingLabels + 'sp',
						},
						top : 0
					});
					viewVotacio.add(labelActualitzacio);

				}

var labelFormula = Ti.UI.createLabel({
					// text :formula.toFixed(1),
					left : 90 * formulaWidth,
						height : isAndroid ? 25 * formulaWidth : 25,
					width :Ti.UI.SIZE,
					 color : Ti.App.colorPrincipal,
					
					bubbleParent : true,
				 verticalAlign : Ti.UI.TEXT_VERTICAL_ALIGNMENT_BOTTOM,
					font : {
						 fontWeight : 'bold',
						fontSize : 14 + Ti.App.paddingLabels + 'sp'
					},
					bottom : 2 * formulaHeight
				});
				viewVotacio.add(labelFormula);
				
				var labelValoracions = Ti.UI.createLabel({
					// text :'('+dataEscenaris[i].valoracions.nombrevots+' valoracions)',
					left : 115 * formulaWidth,
					height : isAndroid ? 25 * formulaWidth : 25,
					width :Ti.UI.SIZE,
					
					color : '#dadada',
					  verticalAlign : Ti.UI.TEXT_VERTICAL_ALIGNMENT_BOTTOM,
					bubbleParent : true,
				
					font : {
						
						fontSize : 11 + Ti.App.paddingLabels + 'sp'
					},
					bottom : 2 * formulaHeight
				});
				viewVotacio.add(labelValoracions);


				var viewImatge = Ti.UI.createImageView({
					width : 90 * formulaWidth,
					height : isAndroid ? 90 * formulaWidth : 90,
					top : 0 * formulaHeight,
					zIndex : 4,
					//backgroundImage : '/images/imagendefectopequena.png',
					left : 0 * formulaWidth
				});

				if (dataEscenaris[i].thumb90x90 != null) {

					viewImatge.image = urlBase + dataEscenaris[i].thumb90x90;
				} else {
					viewImatge.image = '/images/imagendefectopequena.png';
				}

				row.add(viewImatge);
				var labelTitolEscenari = Ti.UI.createLabel({
					text :stringBuilder.shrinkText(dataEscenaris[i].titulo, 45),
					left : 96 * formulaWidth,
					height : Ti.UI.SIZE,
					width : 175 * formulaWidth,
					color : 'white',
					
					bubbleParent : true,
					layout : layout,
					primera:true,
					descripcio : dataEscenaris[i].descripcio,
					highlightedColor : '#ffe132',
					font : {
						fontWeight : 'bold',
						fontSize : 14 + Ti.App.paddingLabels + 'sp',
					},
					top : 16 * formulaHeight
				});
				layout.add(labelTitolEscenari);

			
					
					
				// labelTitolEscenari.addEventListener('postlayout', function creoLabel(e) {
	
		// if(e.source.primera){
					var labelDescripcioEscenari = Ti.UI.createLabel({		
						left : 96 * formulaWidth,
					text:stringBuilder.shrinkText(dataEscenaris[i].descripcio, 70),
					height:Ti.UI.SIZE,
						// height : 90  * formulaWidth - e.source.rect.height - 16 * formulaWidth,
						bubbleParent : true,
						
					
						 verticalAlign : Ti.UI.TEXT_VERTICAL_ALIGNMENT_TOP,
						 width : 175 * formulaWidth,
						
						color : 'white',
						font : {
							fontSize : 11 + Ti.App.paddingLabels + 'sp' ,
						},
						top : -2 * formulaHeight
					});
					layout.add(labelDescripcioEscenari);
					// labelTitolEscenari.removeEventListener('postlayout', creoLabel);
					// e.source.primera=false;
		// }
				// });
				arrayRows.push(row);

			}
			 }
		}
}
		viewOrdenacioInstance = new viewOrdenacio(viewLlistat, creoLlista, dataEscenaris, table, actInd);

		if (!isAndroid) {
		
			var search_bar = Titanium.UI.createSearchBar({
				showCancel : false,
				top : 0 * formulaHeight,
				zIndex : 3,
				// backgroundImage : '/images/searchbar.png',
				barColor :Ti.App.colorPrincipal,
				backgroundColor : Ti.App.colorPrincipal,
				left : 0 * formulaWidth,
				height : isAndroid ? 44 * formulaHeight : 44,
				width :  100 * formulaWidth,
				hintText : buscar_titulo
			});

			search_bar.addEventListener('focus', function(e) {
				search_bar.animate({
					width : Ti.App.vincDescarregats ? 320 * formulaWidth : 320 * formulaWidth,
					duration : 500
				});
			});

			search_bar.addEventListener('cancel', function(e) {
				search_bar.animate({
					width : Ti.App.vincDescarregats ? 229 * formulaWidth : 229 * formulaWidth,
					duration : 500
				});
			});
		}

		// viewOrdenacioInstance.add(search_bar);

		//TABLE VIEW
		if (table != null) {
			if (!isAndroid) {
				table.data = null;
			}

			viewLlistat.remove(table);

		}

		table = Ti.UI.createTableView({
			separatorColor : 'black',
			data : arrayRows,
			 headerView : viewOrdenacioInstance,
			// hideSearchOnSelection : false,
			selectedBackgroundColor:Ti.App.colorPrincipal,
			backgroundSelectedColor:Ti.App.colorPrincipal,
separatorInsets:{
    		left:0,
    		right:0
			},
			height : isAndroid ? 480 * formulaHeight - 44 * formulaHeight - statusBarHeight : 480 * formulaHeight - 44 - statusBarHeight,

			top : isAndroid ? 0 * formulaHeight : 0,
			backgroundColor : '#353535'
		});
		if (!isAndroid) {
			 table.search = search_bar;
			table.filterAttribute = 'name';
		}

		viewLlistat.add(table);
		table.addEventListener('click', function(e) {
			primeraVegada = true;

			if (Ti.App.clicat == false) {

				Ti.App.clicat = true;
				if (!isAndroid) {
					var animation = Ti.UI.createAnimation({
						left : 0 * formulaWidth,
						duration : 1000
					});
				}

				function isiOS7Plus() {
					// iOS-specific test
					if (Titanium.Platform.name == 'iPhone OS') {
						var version = Titanium.Platform.version.split(".");
						var major = parseInt(version[0], 10);

						// Can only test this support on a 3.2+ device
						if (major >= 7) {
							return true;
						}
					}
					return false;
				}

				var iOS7 = isiOS7Plus();
Ti.App.idEscenari=e.row.id;
				var win1 = Titanium.UI.createWindow({
					left : isAndroid ? 0 : 800,
					id : e.row.id,
					titol : e.row.name,
					creador : e.row.creador,
					ambit : e.row.ambit,
					top : iOS7 ? 20 : 0,
					navBarHidden : true,
					// statusBarStyle: Titanium.UI.iPhone.StatusBar.LIGHT_CONTENT,

					idioma : e.row.idioma,
					escenariJSON : e.row.escenariJSON,
					objectesEscenariOffline : Ti.App.vincDescarregats ? JSON.parse(e.row.objectesEscenariOffline) : null,
					descripcio : e.row.descripcio,
					imatge : e.row.imatge,
					valoracio : e.row.valoracio,
					urlBase : urlBase,
					backgroundColor : 'transparent',
					url : '/controller/creacioEscenari/detallEscenari.js'

				});
				if (isAndroid) {

					win1.open();
				} else {
					win1.open(animation);
				}

			}

		});

		fonsActInd.hide();
		actInd.hide();
		if(!isAndroid){
					var refreshLlistaInstance = new refreshLlista(viewLlistat, table, creoLlista, actInd, viewOrdenacioInstance);

		}


	}

	return viewLlistat;

}

module.exports = llistat;
