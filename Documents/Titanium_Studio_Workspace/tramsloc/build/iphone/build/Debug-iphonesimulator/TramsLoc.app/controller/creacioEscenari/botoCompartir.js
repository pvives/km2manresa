function botoDescarregarEscenari(win) {
	if (Titanium.Platform.osname === 'android') {
		var isAndroid = true;
	}

	var widthpantalla = Ti.Platform.displayCaps.platformWidth;
	var heightpantalla = Ti.Platform.displayCaps.platformHeight;
	var formulaWidth = widthpantalla / 320;
	var formulaHeight = heightpantalla / 480;
	var statusBarHeight = Ti.Platform.displayCaps.logicalDensityFactor * 25;

	if (Titanium.App.Properties.getString("language", "undefined") == 'ca') {
		Titanium.include('/controller/translations.js');
	} else if (Titanium.App.Properties.getString("language", "undefined") == 'es') {
		Titanium.include('/controller/translations_es.js');
	} else if (Titanium.App.Properties.getString("language", "undefined") == 'en') {
		Titanium.include('/controller/translations_en.js');
	} else if (Titanium.App.Properties.getString("language", "undefined") == 'pt') {
		Titanium.include('/controller/translations_pt.js');
	} else if (Titanium.App.Properties.getString("language", "undefined") == 'pt-PT') {
		Titanium.include('/controller/translations_pt.js');
	} 
	else if (Titanium.App.Properties.getString("language", "undefined") == 'eus') {
		Titanium.include('/controller/translations_eus.js');
	} else {
		Titanium.include('/controller/translations_en.js');
	}
	

	
		var compartir = Ti.UI.createButton({
			width : isAndroid ? 40* formulaHeight : 40,
			height : isAndroid ? 40 * formulaHeight : 40,
			backgroundImage : '/images/compartir.png',
			zIndex : 10,
			color : '#3e3d40',
			top : 2 * formulaHeight,
			
			right : 70 * formulaWidth
		});


	compartir.addEventListener('click', function(e) {

			
			var intent = Ti.Android.createIntent({
					action : Ti.Android.ACTION_SEND,
					type : 'text/plain'
				});
				intent.putExtra(Ti.Android.EXTRA_TEXT, win.titol + ': ' + 'http://educacr.itinerarium.es/es/escenari/'+win.id+'/preview-iframe');

				Ti.Android.currentActivity.startActivity(intent);
		

	});
	return compartir;
}

module.exports = botoDescarregarEscenari; 