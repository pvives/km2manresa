if (Titanium.App.Properties.getString("language", "undefined") == 'ca') {
	Titanium.include('/controller/translations.js');
} else if (Titanium.App.Properties.getString("language", "undefined") == 'es') {
	Titanium.include('/controller/translations_es.js');
} else if (Titanium.App.Properties.getString("language", "undefined") == 'en') {
	Titanium.include('/controller/translations_en.js');
} else if (Titanium.App.Properties.getString("language", "undefined") == 'pt') {
	Titanium.include('/controller/translations_pt.js');
} else if (Titanium.App.Properties.getString("language", "undefined") == 'pt-PT') {
	Titanium.include('/controller/translations_pt.js');
} 
else if (Titanium.App.Properties.getString("language", "undefined") == 'eus') {
		Titanium.include('/controller/translations_eus.js');
	} else {
	Titanium.include('/controller/translations_en.js');
}
if (Titanium.Platform.osname === 'android') {
	var isAndroid = true;
}

var widthpantalla = Ti.Platform.displayCaps.platformWidth;
var heightpantalla = Ti.Platform.displayCaps.platformHeight;
var formulaWidth = widthpantalla / 320;
var formulaHeight = heightpantalla / 480;

function createFitxaPregunta(dataTrivial, preguntaActual, arrayPreguntesResoltes, scrollableViewTrivial,sumaLabel) {

	var trivialView = Ti.UI.createView({
		width : 270 * formulaWidth,
		top : isAndroid ? 0 * formulaHeight : 0,
		left : 0 * formulaWidth,
		height : isAndroid ? 480 * formulaHeight - statusBarHeight - 44 * formulaHeight - 44 * formulaHeight : 480 * formulaHeight - 44 - 44
	});

	var blackView = Ti.UI.createView({
		top : isAndroid ? 0 * formulaHeight : 0,
		left : 0 * formulaWidth,
		height : 120 * formulaHeight,
		backgroundColor : 'black',
		width : 270 * formulaWidth
	});

	trivialView.add(blackView);

	var labelPregunta = Ti.UI.createLabel({
		text : dataTrivial.trivialstexte[preguntaActual].pregunta,
		top : isAndroid ? 0 * formulaHeight : 0,
		left : 10 * formulaWidth,
		height : 120 * formulaHeight,
		color : 'white',
		textAlign : 'left',
		width : 250 * formulaWidth,
		font : {
			fontSize : 14+Ti.App.paddingLabels+'sp' 
		}
	});

	blackView.add(labelPregunta);

	var viewResposta1 = Ti.UI.createView({
		width : 270 * formulaWidth,
		height : isAndroid ? 50 * formulaHeight : 50,
		backgroundImage : 'none',
		top : 158 * formulaHeight,
		backgroundColor : '#e4a531',
		left : 0 * formulaWidth
	});
	trivialView.add(viewResposta1);

	var viewResposta1be = Ti.UI.createButton({
		width : 30 * formulaWidth,
		height : isAndroid ? 30 * formulaHeight : 30,
		visible : false,
		backgroundImage : '/images/trivialbe.png',
		top : 10 * formulaHeight,
		right : 5 * formulaWidth
	});
	viewResposta1.add(viewResposta1be);

	var viewResposta1mal = Ti.UI.createButton({
		width : 30 * formulaWidth,
		height : isAndroid ? 30 * formulaHeight : 30,
		visible : false,
		backgroundImage : '/images/trivialmal.png',
		top : 10 * formulaHeight,
		right : 5 * formulaWidth
	});
	viewResposta1.add(viewResposta1mal);

	viewResposta1.addEventListener('click', function(e) {
		if (!arrayPreguntesResoltes[preguntaActual]) {

			if (dataTrivial.trivialstexte[preguntaActual].correcta == 1) {
				viewResposta1be.visible = true;
				arrayPreguntesResoltes[preguntaActual] = true;
				if (scrollableViewTrivial.currentPage < dataTrivial.trivialstexte.length-1) {
					
					scrollableViewTrivial.scrollToView(scrollableViewTrivial.currentPage + 1);
					sumaLabel();
				}

			} else {
				viewResposta1mal.visible = true;
			}
		}
	});
	var labelResposta1 = Ti.UI.createLabel({
		text : dataTrivial.trivialstexte[preguntaActual].respuesta1,
		top : 0 * formulaHeight,
		left : 0 * formulaWidth,
		height : isAndroid ? 50 * formulaHeight : 50,
		color : 'black',
		textAlign : 'center',
		width : 270 * formulaWidth,
		font : {
			fontSize : 14+Ti.App.paddingLabels+'sp' 
		}
	});

	viewResposta1.add(labelResposta1);

	var viewResposta2 = Ti.UI.createView({
		width : 270 * formulaWidth,
		height : isAndroid ? 50 * formulaHeight : 50,
		backgroundImage : 'none',
		top : 223 * formulaHeight,
		backgroundColor :  '#e4a531',
		left : 0 * formulaWidth
	});
	trivialView.add(viewResposta2);

	var viewResposta2be = Ti.UI.createButton({
		width : 30 * formulaWidth,
		height : isAndroid ? 30 * formulaHeight : 30,
		visible : false,
		backgroundImage : '/images/trivialbe.png',
		top : 10 * formulaHeight,
		right : 5 * formulaWidth
	});
	viewResposta2.add(viewResposta2be);

	var viewResposta2mal = Ti.UI.createButton({
		width : 30 * formulaWidth,
		height : isAndroid ? 30 * formulaHeight : 30,
		visible : false,
		backgroundImage : '/images/trivialmal.png',
		top : 10 * formulaHeight,
		right : 5 * formulaWidth
	});
	viewResposta2.add(viewResposta2mal);

	viewResposta2.addEventListener('click', function(e) {
		if (!arrayPreguntesResoltes[preguntaActual]) {
			if (dataTrivial.trivialstexte[preguntaActual].correcta == 2) {
				viewResposta2be.visible = true;
				arrayPreguntesResoltes[preguntaActual] = true;
				if (scrollableViewTrivial.currentPage < dataTrivial.trivialstexte.length-1) {
					scrollableViewTrivial.scrollToView(scrollableViewTrivial.currentPage + 1);
					sumaLabel();
				}
			} else {
				viewResposta2mal.visible = true;
			}
		}
	});

	var labelResposta2 = Ti.UI.createLabel({
		text : dataTrivial.trivialstexte[preguntaActual].respuesta2,
		top : 0 * formulaHeight,
		left : 0 * formulaWidth,
		height : isAndroid ? 50 * formulaHeight : 50,
		color : 'black',

		textAlign : 'center',
		width : 270 * formulaWidth,
		font : {
			fontSize : 14+Ti.App.paddingLabels+'sp' 
		}
	});

	viewResposta2.add(labelResposta2);

	var viewResposta3 = Ti.UI.createView({
		width : 270 * formulaWidth,
		height : isAndroid ? 50 * formulaHeight : 50,
		backgroundImage : 'none',
		top : 288 * formulaHeight,
		backgroundColor :  '#e4a531',
		left : 0 * formulaWidth
	});
	trivialView.add(viewResposta3);

	var viewResposta3be = Ti.UI.createButton({
		width : 30 * formulaWidth,
		height : isAndroid ? 30 * formulaHeight : 30,
		visible : false,
		backgroundImage : '/images/trivialbe.png',
		top : 10 * formulaHeight,
		right : 5 * formulaWidth
	});
	viewResposta3.add(viewResposta3be);

	var viewResposta3mal = Ti.UI.createButton({
		width : 30 * formulaWidth,
		height : isAndroid ? 30 * formulaHeight : 30,
		visible : false,
		backgroundImage : '/images/trivialmal.png',
		top : 10 * formulaHeight,
		right : 5 * formulaWidth
	});
	viewResposta3.add(viewResposta3mal);

	viewResposta3.addEventListener('click', function(e) {
		if (!arrayPreguntesResoltes[preguntaActual]) {
			if (dataTrivial.trivialstexte[preguntaActual].correcta == 3) {

				viewResposta3be.visible = true;
				arrayPreguntesResoltes[preguntaActual] = true;
				if (scrollableViewTrivial.currentPage < dataTrivial.trivialstexte.length-1) {
					scrollableViewTrivial.scrollToView(scrollableViewTrivial.currentPage + 1);
					sumaLabel();
				}
			} else {
				viewResposta3mal.visible = true;
			}
		}
	});

	var labelResposta3 = Ti.UI.createLabel({
		text : dataTrivial.trivialstexte[preguntaActual].respuesta3,
		top : 0 * formulaHeight,
		left : 0 * formulaWidth,
		height : isAndroid ? 50 * formulaHeight : 50,
		color : 'black',

		textAlign : 'center',
		width : 270 * formulaWidth,
		font : {
			fontSize : 14+Ti.App.paddingLabels+'sp' 
		}
	});

	viewResposta3.add(labelResposta3);

	return trivialView;

}

module.exports = createFitxaPregunta; 