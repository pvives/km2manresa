function refreshLlistat(viewLlistat, table, creoLlista, actInd, viewOrdenacioInstance) {
	if (Titanium.Platform.osname === 'android') {
		var isAndroid = true;
	}
	var widthpantalla = Ti.Platform.displayCaps.platformWidth;
	var heightpantalla = Ti.Platform.displayCaps.platformHeight;
	var formulaWidth = widthpantalla / 320;
	var formulaHeight = heightpantalla / 480;

	if (!isAndroid) {

		function formatDate() {
			if (Titanium.App.Properties.getString("language", "undefined") == 'en') {
				var date = new Date();
				var datestr = date.getMonth() + 1 + '/' + date.getDate() + '/' + date.getFullYear();
				if (date.getHours() >= 12) {
					datestr += ' ' + (date.getHours() == 12 ? date.getHours() : date.getHours() - 12) + ':' + date.getMinutes() + ' PM';
				} else {
					datestr += ' ' + date.getHours() + ':' + date.getMinutes() + ' AM';
				}
			} else {
				var date = new Date();
				var datestr = date.getMonth() + 1 + '/' + date.getDate() + '/' + date.getFullYear();
				if (date.getHours() >= 12) {
					datestr += ' ' + (date.getHours() == 12 ? date.getHours() : date.getHours() - 12) + ':' + date.getMinutes() + ' PM';
				} else {
					datestr += ' ' + date.getHours() + ':' + date.getMinutes() + ' AM';
				}
			}

			return datestr;
		}

		var lastRow = 4;

		var border = Ti.UI.createView({
			backgroundColor : "black",
			height : isAndroid ? 2 * formulaHeight : 2,
			bottom : 0 * formulaHeight
		});

		var tableHeader = Ti.UI.createView({
			backgroundColor : '#3d3d3c',
			width : 320 * formulaWidth,
			height : isAndroid ? 60 * formulaHeight : 60
		});

		// fake it til ya make it..  create a 2 pixel
		// bottom border
		tableHeader.add(border);

		var arrow = Ti.UI.createView({
			backgroundImage : "/images/whiteArrow.png",
			width : 23 * formulaWidth,
			height : isAndroid ? 60 * formulaHeight : 60,
			bottom : 10 * formulaHeight,
			left : 20 * formulaWidth
		});

		var statusLabel = Ti.UI.createLabel({
			text : pull_to_reload,
			left : 55 * formulaWidth,
			width : 210 * formulaWidth,
			bottom : 30 * formulaHeight,
			height : "auto",
			color : "white",
			textAlign : "center",
			font : {
				fontSize : 13 + Ti.App.paddingLabels + 'sp',
				fontWeight : "bold"
			},
			shadowColor : "#999",
			shadowOffset : {
				x : 0,
				y : 1
			}
		});

		var lastUpdatedLabel = Ti.UI.createLabel({
			text : ultima_actualitzacio + formatDate(),
			left : 55 * formulaWidth,
			width : 230 * formulaWidth,
			bottom : 15 * formulaHeight,
			height : "auto",
			color : "white",
			textAlign : "center",
			font : {
				fontSize : 11 + Ti.App.paddingLabels + 'sp'
			},
			shadowColor : "#999",
			shadowOffset : {
				x : 0,
				y : 1
			}
		});

		var actInd2 = Titanium.UI.createActivityIndicator({
			left : 20 * formulaWidth,
			bottom : 13 * formulaHeight,
			width : 30 * formulaWidth,
			height : isAndroid ? 30 * formulaHeight : 30
		});

		tableHeader.add(arrow);
		tableHeader.add(statusLabel);
		tableHeader.add(lastUpdatedLabel);
		tableHeader.add(actInd2);

		table.headerPullView = tableHeader;

		var pulling = false;
		var reloading = false;

		function beginReloading() {
			// actInd.show();
			table.data = [];
			// just mock out the reload
			setTimeout(endReloading, 2000);
		}

		function endReloading() {

			if (Ti.App.vincMeus) {

				var dataMisEscenarios = require('controller/llistesEscenaris/meusNetworkController');
				var dataMisEscenariosInstance = new dataMisEscenarios(urlBase + '/api/scenario/my-list', callbackMisEscenarios);
				function callbackMisEscenarios(data) {

					creoLlista(data);
				}

			} else if (Ti.App.vincDescarregats) {
				var arrayEscenarisOffline = Titanium.App.Properties.getList("arrayEscenarisOffline", []);

				creoLlista(arrayEscenarisOffline);

			} else {
				// actInd.show();

				var dataBiblioteca = require('controller/llistesEscenaris/llistats_network_Controller');
				if (Ti.App.entorn == 'eduloc') {
					new dataBiblioteca(urlBase + '/mobile/scenario/library', callbackLlistatBiblioteca, actInd);

				} else {
					new dataBiblioteca(urlBase + '/api/scenario/library/' + Ti.App.entorn + '/es', callbackLlistatBiblioteca, actInd);

				}
				function callbackLlistatBiblioteca(data) {

					creoLlista(data);
				}

			}

			// when you're done, just reset
			table.setContentInsets({
				top : 0
			}, {
				animated : true
			});
			reloading = false;
			lastUpdatedLabel.text = ultima_actualitzacio + formatDate();
			statusLabel.text = pull_down_refresh;
			actInd2.hide();
			arrow.show();
		}


		table.addEventListener('scroll', function(e) {
			var offset = e.contentOffset.y;
			if (offset <= -65.0 && !pulling && !reloading) {
				var t = Ti.UI.create2DMatrix();
				t = t.rotate(-180);
				pulling = true;
				arrow.animate({
					transform : t,
					duration : 180
				});
				statusLabel.text = release_to_refresh;
			} else if (pulling && (offset > -65.0 && offset < 0) && !reloading) {
				pulling = false;
				var t = Ti.UI.create2DMatrix();
				arrow.animate({
					transform : t,
					duration : 180
				});
				statusLabel.text = pull_down_refresh;
			}
		});

		var event1 = 'dragEnd';
		if (Ti.version >= '3.0.0') {
			event1 = 'dragend';
		}

		table.addEventListener(event1, function(e) {
			if (pulling && !reloading) {
				reloading = true;
				pulling = false;
				arrow.hide();
				actInd2.show();
				statusLabel.text = carregant;
				table.setContentInsets({
					top : 60
				}, {
					animated : true
				});
				arrow.transform = Ti.UI.create2DMatrix();
				beginReloading();
			}
		});
	}
}

module.exports = refreshLlistat;
