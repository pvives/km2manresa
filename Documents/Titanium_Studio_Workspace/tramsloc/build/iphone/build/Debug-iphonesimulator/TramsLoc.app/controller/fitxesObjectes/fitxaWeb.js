function createFitxaWeb(dataPunt, urlBase) {

	//Crea les fitxes de tots els objectes
	var widthpantalla = Ti.Platform.displayCaps.platformWidth;
	var heightpantalla = Ti.Platform.displayCaps.platformHeight;
	var formulaWidth = widthpantalla / 320;
	var formulaHeight = heightpantalla / 480;

	if (Titanium.Platform.osname === 'android') {
		var isAndroid = true;
	}
	var statusBarHeight = Ti.Platform.displayCaps.logicalDensityFactor * 25;

	var fitxaWeb = Ti.UI.createView({
		width : 270 * formulaWidth,
		height : isAndroid ? 480 * formulaHeight - statusBarHeight - 44 * formulaHeight - 44 * formulaHeight : 480 * formulaHeight - 44-44,
		left : 0 * formulaWidth,
		top : 44*formulaHeight ,
		visible:false,
		backgroundColor : 'transparent'

	});

	var barraWebView = Ti.UI.createView({
		width : 270 * formulaWidth,
		height : isAndroid ? 44 * formulaHeight : 44,
		left : 0 * formulaWidth,
		backgroundColor : 'black',
		top : isAndroid ? 0 * formulaHeight : 0

	});
	fitxaWeb.add(barraWebView);

	var botoBack = Ti.UI.createView({
		width : 44 * formulaWidth,
		height : isAndroid ? 44 * formulaHeight : 44,
		left : 20 * formulaWidth,
		top : isAndroid ? 0 * formulaHeight : 0,
		backgroundImage : '/images/controlsWeb/anteriorweb.png'

	});
	botoBack.addEventListener('click', function(e) {
		viewWeb.goBack();
	});
	barraWebView.add(botoBack);

	var botoForward = Ti.UI.createView({
		width : 44 * formulaWidth,
		height : isAndroid ? 44 * formulaHeight : 44,
		left : 83 * formulaWidth,
		top : isAndroid ? 0 * formulaHeight : 0,
		backgroundImage : '/images/controlsWeb/siguienteweb.png'

	});
	botoForward.addEventListener('click', function(e) {
		viewWeb.goForward();
	});
	barraWebView.add(botoForward);

	var botoRefresh = Ti.UI.createView({
		width : 44 * formulaWidth,
		height : isAndroid ? 44 * formulaHeight : 44,
		left : 146 * formulaWidth,
		top : isAndroid ? 0 * formulaHeight : 0,
		backgroundImage : '/images/controlsWeb/actualizar.png'

	});
	botoRefresh.addEventListener('click', function(e) {
		viewWeb.reload();
	});
	barraWebView.add(botoRefresh);

	var botoShare = Ti.UI.createView({
		width : 44 * formulaWidth,
		height : isAndroid ? 44 * formulaHeight : 44,
		left : 209 * formulaWidth,
		bottom : isAndroid ? 0 * formulaHeight : 0,
		backgroundImage : '/images/controlsWeb/compartir.png'

	});

	botoShare.addEventListener('click', function(e) {

	});
	//barraWebView.add(botoShare);

	var viewWeb = Ti.UI.createWebView({
		enableZoomControls : true,
		visible : true,
		left : 0 * formulaWidth,
		height : isAndroid ? 480 * formulaHeight - statusBarHeight - 44 * formulaHeight - 44 * formulaHeight - 44 * formulaHeight : 480 * formulaHeight - 44 - 44 - 44,
		top : 44*formulaHeight,
		url : dataPunt.galeria.urlGenerica,
		width : 270 * formulaWidth,
		showScrollbars : false
	});
	fitxaWeb.add(viewWeb);

	isAndroid ? viewWeb.reload() : viewWeb.repaint();

	return fitxaWeb;

}

module.exports = createFitxaWeb;
