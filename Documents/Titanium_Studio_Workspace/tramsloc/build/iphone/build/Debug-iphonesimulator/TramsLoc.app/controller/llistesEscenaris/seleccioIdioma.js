function idioma(loginViewFonsGris,loginView) {
	var idioma;
	var idiomaCurt;
	if (Titanium.App.Properties.getString("language", "undefined") == 'ca') {
		Titanium.include('/controller/translations.js');
		idiomaCurt='ca';
		idioma = 'Català';
	} else if (Titanium.App.Properties.getString("language", "undefined") == 'es') {
		Titanium.include('/controller/translations_es.js');
		idioma = 'Español';
		idiomaCurt='es';
	} else if (Titanium.App.Properties.getString("language", "undefined") == 'en') {
		Titanium.include('/controller/translations_en.js');
		idioma = 'English';
		idiomaCurt='en';
	}  else if (Titanium.App.Properties.getString("language", "undefined") == 'eus') {
		Titanium.include('/controller/translations_eus.js');
		idioma = 'Euskara';
	} else {
		Titanium.include('/controller/translations_en.js');
		idioma = 'English';
		idiomaCurt='eu';
	}
	var idiomes = ['Català', 'Español', 'English'];

	var arrayIdiomes = [];
	
		var viewIdioma320 = Ti.UI.createView({
		width : 320 * formulaWidth,
		height : Ti.UI.FILL,
		top : 0 * formulaHeight,
		bubbleParent : false,
		zIndex:15,
		backgroundColor :  '#ebebeb',
		left : 320 * formulaWidth
	});
	loginViewFonsGris.add(viewIdioma320);
	
	for (var i = 0; i < idiomes.length; i++) {

		var row = Ti.UI.createTableViewRow({
			height : isAndroid ? 40 * formulaWidth : 40,
			width : 320 * formulaWidth,
rightImage:idiomes[i]==idioma?'/images/tik.png':'none',
			font : {
				fontSize : 16 + Ti.App.paddingLabels + 'sp' ,
			},
			idioma:idiomes[i],
					selectedBackgroundColor:'#dbdbdb',
		backgroundSelectedColor:'#dbdbdb',
			borderRadius : 0,
			color : 'black',
			borderWidth : 0,
			title : idiomes[i]
		});
		arrayIdiomes.push(row);
	}

	var botoIdioma = Ti.UI.createView({
		width : 240 * formulaWidth,
		height : isAndroid ? 35 * formulaHeight : 35,
		bottom : 40 * formulaHeight,
		bubbleParent : false,
		borderRadius : 10,
		
		backgroundColor : '#747474',
		left : 40 * formulaWidth
	});
	loginViewFonsGris.add(botoIdioma);
	



		var labelIdiomaReinici1 = Ti.UI.createLabel({
		text : canvi_idioma,
		left : 48 * formulaWidth,
		height : isAndroid ? 29 * formulaHeight : 29,
		width : 215 * formulaWidth,
		visible:false,
		font : {
			fontSize : 10 + Ti.App.paddingLabels + 'sp'
		},
		color : '#afafaf',
		bottom :14 * formulaHeight
	});
	loginViewFonsGris.add(labelIdiomaReinici1);
	
var iconaIdioma = Ti.UI.createView({
		width : 35 * formulaWidth,
		height : isAndroid ? 35 * formulaHeight : 35,
		bottom : 0 * formulaHeight,
		bubbleParent : false,
		backgroundImage :  '/images/idioma.png',
		left : 0 * formulaWidth
	});
	botoIdioma.add(iconaIdioma);
	
	var labelInformatiuIdioma = Ti.UI.createLabel({
		text : labelIdiomaText,
		left : 40 * formulaWidth,
		textAlign : 'left',
		height : isAndroid ? 35 * formulaHeight : 35,
		width : Ti.UI.SIZE,
		color : '#d3d3d2',
		top : 0 * formulaHeight,
		font : {
			fontSize : 14 + Ti.App.paddingLabels + 'sp'
		}
	});
	 botoIdioma.add(labelInformatiuIdioma);

	var labelIdioma = Ti.UI.createLabel({
		text : idioma,
		right : 10 * formulaWidth,
		height : isAndroid ? 35 * formulaHeight : 35,
		width : Ti.UI.SIZE,
		font : {
			fontSize : 14 + Ti.App.paddingLabels + 'sp'
		},
		color : '#8e8e93',
		top : 0 * formulaHeight
	});
	botoIdioma.add(labelIdioma);
	
	var botoBack = Ti.UI.createButton({
	width : 44 * formulaWidth,
	height : isAndroid ? 44 * formulaHeight : 44,
visible:false,
	left : 0 * formulaWidth,
	top : 0 * formulaHeight,
	backgroundImage : '/images/back.png'

});
barraSuperior.add(botoBack);

	botoBack.addEventListener('click', function(e) {
			labelBarraSuperior.text=configuracio;
		menuButton.show();
		botoBack.hide();
		viewIdioma320.animate({
				left : isAndroid ? 320*formulaWidth : 320,
				duration : 500
			});	
	});
		var labelIdiomaReinici = Ti.UI.createLabel({
		text : canvi_idioma,
		left : 15 * formulaWidth,
		height : isAndroid ? 29 * formulaHeight : 29,
		width : 290* formulaWidth,
		font : {
			fontSize : 10 + Ti.App.paddingLabels + 'sp'
		},
		color : '#afafaf',
		top : 28 * formulaHeight
	});
	viewIdioma320.add(labelIdiomaReinici);
	
	var tableIdiomes = Ti.UI.createTableView({
		separatorColor : '#c8c7cc',
		color : 'black',
		font : {
			fontSize : 16 + Ti.App.paddingLabels + 'sp'
		},
		data : arrayIdiomes,
		hideSearchOnSelection : false,
		backgroundColor : 'white',
		left : 0 * formulaWidth,
		selectedBackgroundColor:'#f6f6f6',
		backgroundSelectedColor:'#f6f6f6',
		width : 320 * formulaWidth,
		height : Ti.UI.SIZE,
		zIndex : 15,
		top : isAndroid ? 64 * formulaHeight : 64
	});
	viewIdioma320.add(tableIdiomes);

	tableIdiomes.addEventListener('click', function(e) {
		if (e.index == 0) {
			Titanium.App.Properties.setString("languageGuardada", 'ca');
		} else if (e.index == 1) {
			Titanium.App.Properties.setString("languageGuardada", 'es');
		} else if (e.index == 2) {
			Titanium.App.Properties.setString("languageGuardada", 'en');
		} else {

			Titanium.App.Properties.setString("language", llengua);

		}
		labelIdioma.text=e.source.idioma;
		labelIdiomaReinici1.visible=true;
		labelBarraSuperior.text=configuracio;
		menuButton.show();
		botoBack.hide();
		viewIdioma320.animate({
				left : isAndroid ? 320*formulaWidth : 320,
				duration : 500
			});

	});

	botoIdioma.addEventListener('click', function(e) {
		labelBarraSuperior.text=labelIdiomaTextMinuscula;
		menuButton.hide();
		botoBack.show();
viewIdioma320.animate({
				left : isAndroid ? 0 : 0,
				duration : 500
			});


	});
	loginView.add(viewIdioma320);
	
return botoIdioma;
}

module.exports = idioma; 