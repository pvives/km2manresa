function fitxaLogin(urlBase) {
	var idioma;
	if (Titanium.App.Properties.getString("language", "undefined") == 'ca') {
		Titanium.include('/controller/translations.js');
		idioma = 'Català';
	} else if (Titanium.App.Properties.getString("language", "undefined") == 'es') {
		Titanium.include('/controller/translations_es.js');
		idioma = 'Español';
	} else if (Titanium.App.Properties.getString("language", "undefined") == 'en') {
		Titanium.include('/controller/translations_en.js');
		idioma = 'English';
	} else if (Titanium.App.Properties.getString("language", "undefined") == 'pt') {
		Titanium.include('/controller/translations_pt.js');
		idioma = 'Português';
	} else if (Titanium.App.Properties.getString("language", "undefined") == 'pt-PT') {
		Titanium.include('/controller/translations_pt.js');
		idioma = 'Português';
	} else if (Titanium.App.Properties.getString("language", "undefined") == 'eus') {
		Titanium.include('/controller/translations_eus.js');
		idioma = 'Euskara';
	} else {
		Titanium.include('/controller/translations_en.js');
		idioma = 'English';
	}
	if (Ti.App.entorn == 'eduloc') {
		var customFont = 'Anna Beta';
	} else {
		var customFont = 'Helvetica';
	}
	if (Titanium.Platform.osname === 'android') {
		var isAndroid = true;
	}
	var widthpantalla = Ti.Platform.displayCaps.platformWidth;
	var heightpantalla = Ti.Platform.displayCaps.platformHeight;
	var formulaWidth = widthpantalla / 320;
	var formulaHeight = heightpantalla / 480;

	var statusBarHeight = Ti.Platform.displayCaps.logicalDensityFactor * 25;

	var loginView = Ti.UI.createView({
		width : 640 * formulaWidth,
		visible : false,

		height : isAndroid ? 480 * formulaHeight - 44 * formulaHeight - statusBarHeight : 480 * formulaHeight - 44 - statusBarHeight,
		top : isAndroid ? 44 * formulaHeight : 44,
		left : 0
	});

	var loginViewFonsGris = Ti.UI.createView({
		width : 320 * formulaWidth,
		height : isAndroid ? 480 * formulaHeight - 44 * formulaHeight - statusBarHeight : 480 * formulaHeight - 44 - statusBarHeight,
		top : 0,
		backgroundColor : '#ebebeb',
		left : 0
	});
	loginView.add(loginViewFonsGris);

	var pantallaIdioma = require('/controller/llistesEscenaris/seleccioIdioma');
	var botoIdiomaInstance = new pantallaIdioma(loginViewFonsGris,loginView);
	

	var imatgeLogoFons = Ti.UI.createImageView({
		width : 240 * formulaWidth,
		height : isAndroid ? 110 * formulaWidth : 110,
		top : 20,
		backgroundImage : '/images/logo.png',
		left : 40
	});
	loginViewFonsGris.add(imatgeLogoFons);

	var camps = Ti.UI.createView({
		width : 320 * formulaWidth,
		height : isAndroid ? 80 * formulaHeight : 80,
		top : isAndroid ? 140 * formulaHeight : 140,
		bubbleParent : false,

		left : 0 * formulaWidth
	});
	loginViewFonsGris.add(camps);

	var taLoginView = Ti.UI.createView({
		width : 240 * formulaWidth,
		height : isAndroid ? 35 * formulaHeight : 35,
		top : isAndroid ? 0 * formulaHeight : 0,
		bubbleParent : false,
		borderRadius : 10,
		borderWidth : 1,
		borderColor : '#666666',
		backgroundColor : 'white',
		left : 40 * formulaWidth
	});
	camps.add(taLoginView);

	var iconaLogin = Ti.UI.createView({
		width : 35 * formulaWidth,
		height : isAndroid ? 35 * formulaHeight : 35,
		top : isAndroid ? 0 * formulaHeight : 0,
		bubbleParent : false,

		backgroundImage : '/images/user.png',
		left : 5 * formulaWidth
	});
	taLoginView.add(iconaLogin);

	var taLogin = Titanium.UI.createTextArea({
		editable : true,
		left : 40 * formulaWidth,
		value : nomUsuari,

		autocapitalization : false,
		height : isAndroid ? 35 * formulaHeight : 35,
		width : 200 * formulaWidth,
		top : 0 * formulaHeight,
		//font:{fontSize:20,fontFamily:'Marker Felt', fontWeight:'bold'},
		font : {
			fontSize : '14sp'
		},
		clearOnEdit : true,
		enableReturnKey : false,
		verticalAlign : Ti.UI.TEXT_VERTICAL_ALIGNMENT_CENTER,
		color : '#989898',
		textAlign : 'left',
		suppressReturn : true
	});
	taLoginView.add(taLogin);

	loginView.addEventListener('click', function(e) {
		if (taLogin.value == '') {
			taLogin.value = nomUsuari;
		}
		if (taPassword.value == '') {
			taPassword.value = password;
		}
		taLogin.blur();
		taPassword.blur();
	});

	taLogin.addEventListener('return', function(e) {

		taLogin.value = taLogin.value;
		taLogin.blur();
	});

	taLogin.addEventListener('focus', function(e) {

		taLogin.value = '';

	});

	var passwordView = Ti.UI.createView({
		width : 240 * formulaWidth,
		height : isAndroid ? 35 * formulaHeight : 35,
		bottom : isAndroid ? 0 * formulaHeight : 0,
		bubbleParent : false,
		borderRadius : 10,
		borderWidth : 1,
		borderColor : '#666666',
		backgroundColor : 'white',
		left : 40 * formulaWidth
	});
	camps.add(passwordView);

	var iconaLogin = Ti.UI.createView({
		width : 35 * formulaWidth,
		height : isAndroid ? 35 * formulaHeight : 35,
		top : isAndroid ? 0 * formulaHeight : 0,
		bubbleParent : false,

		backgroundImage : '/images/password.png',
		left : 5 * formulaWidth
	});
	passwordView.add(iconaLogin);
	var taPassword = Titanium.UI.createTextField({
		editable : true,
		left : 40 * formulaWidth,
		value : password,
		passwordMask : true,
		//autocapitalization : false,
		height : isAndroid ? 35 * formulaHeight : 35,
		width : 200 * formulaWidth,
		bottom : 0 * formulaHeight,
		//font:{fontSize:20,fontFamily:'Marker Felt', fontWeight:'bold'},
		font : {
			fontSize : '14sp'
		},
		clearOnEdit : true,
		enableReturnKey : false,
		verticalAlign : Ti.UI.TEXT_VERTICAL_ALIGNMENT_CENTER,
		color : '#989898',
		textAlign : 'left',

		suppressReturn : true
	});
	passwordView.add(taPassword);

	taPassword.addEventListener('focus', function(e) {

		taPassword.value = '';

	});
if(isAndroid){
	var viewEntrar = Ti.UI.createView({
		width : 240 * formulaWidth,
		height : isAndroid ? 40 * formulaHeight : 40,
		top : isAndroid ? 230 * formulaHeight : 230,
		borderRadius : 10,
		backgroundColor : Ti.App.colorPrincipal,
		left : 40 * formulaWidth
	});
	loginViewFonsGris.add(viewEntrar);	
}
else{
	var viewEntrar = Ti.UI.createButton({
		width : 240 * formulaWidth,
		height : isAndroid ? 40 * formulaHeight : 40,
		top : isAndroid ? 230 * formulaHeight : 230,
		borderRadius : 10,
		backgroundImage:'none',
		font : {
			fontSize : 16 + Ti.App.paddingLabels + 'sp'
		},
		title:entrar,
		color:'white',
		backgroundColor : Ti.App.colorPrincipal,
		left : 40 * formulaWidth
	});
	loginViewFonsGris.add(viewEntrar);	
}

	var labelEntrar = Ti.UI.createLabel({
		text : entrar,
		left : 0 * widthpantalla,

		textAlign : 'center',
		height : isAndroid ? 40 * formulaHeight : 40,
		width : 240 * formulaWidth,
		color : 'white',
		top : 0 * formulaHeight,
		font : {
			fontSize : 16 + Ti.App.paddingLabels + 'sp'
		}
	});
	if(isAndroid){
		viewEntrar.add(labelEntrar);
	}
	 

	var nomUsuariLabel = Ti.UI.createLabel({
		text : '',
		left : 5 * formulaWidth,
		textAlign : 'center',
		visible : false,
		height : 40 * formulaHeight,
		width : 310 * formulaWidth,
		color : '#8e8e93',
		zIndex:15,
		top : 163 * formulaHeight,
		font : {
			fontWeight:'bold',
			fontSize : 13 + Ti.App.paddingLabels + 'sp'
		}
	});
	loginViewFonsGris.add(nomUsuariLabel);

	var labelLoggedAs = Ti.UI.createLabel({
		text : logged_in_as,
		left : 5  * formulaWidth,
		textAlign : 'center',
		visible : false,
		zIndex:15,
		height : 40 * formulaHeight,
		width : 310 * formulaWidth,
		color : '#8e8e93',
		top : 150 * formulaHeight,
		font : {
			fontSize : 13 + Ti.App.paddingLabels + 'sp'
		}
	});
	loginViewFonsGris.add(labelLoggedAs);
	viewEntrar.addEventListener('click', function(e) {

		var login = require('controller/llistesEscenaris/login');
		new login(taLogin.value, taPassword.value, callbackLoginOk, urlBase + '/api/getsalt');

		function callbackLoginOk(saltedPassword) {
			Titanium.App.Properties.setString("saltedPassword", saltedPassword);
			Titanium.App.Properties.setString("user", taLogin.value);

			var wsse = require('controller/llistesEscenaris/wsse');
			var wsseHeader = wsse.wsseHeader(taLogin.value, saltedPassword);
			var xhr = Titanium.Network.createHTTPClient();

			xhr.onload = function() {

				taLogin.blur();
				taPassword.blur();
botoIdiomaInstance.bottom=170*formulaHeight;
				Titanium.App.Properties.setString("loggedIn", "true");
				viewSortir.show();
				viewEntrar.hide();
			viewRegistre.hide();
			labelRegistre.hide();
		
				camps.visible=false;
				labelLoggedAs.visible = true;
				
nomUsuariLabel.text = taLogin.value;
				nomUsuariLabel.visible = true;
				

			};
			Ti.API.info(urlBase + '/api/scenario/my-list');
			xhr.open('POST', urlBase + '/api/scenario/my-list');
			xhr.setRequestHeader('X-WSSE', wsseHeader);
			xhr.onerror = function(e) {
				//this fires if Titanium/the native SDK cannot successfully retrieve a resource

				var alertDialog = Titanium.UI.createAlertDialog({
					title : contrasenya_incorrecta,
					buttonNames : ['Ok']
				});
				alertDialog.show();

			};

			xhr.send({

				'filter_order_by' : Ti.App.ordenacio,
			});

		}

	});
if(isAndroid){
		var viewSortir = Ti.UI.createView({
		width : 240 * formulaWidth,
		height : isAndroid ? 40 * formulaHeight : 40,
		top : isAndroid ? 230 * formulaHeight : 230,
		borderRadius : 10,
		visible : false,
		backgroundColor : Ti.App.colorPrincipal,
		left : 40 * formulaWidth
	});
	loginViewFonsGris.add(viewSortir);
}
else{
		var viewSortir = Ti.UI.createButton({
		width : 240 * formulaWidth,
		height : isAndroid ? 40 * formulaHeight : 40,
		top : isAndroid ? 230 * formulaHeight : 230,
		borderRadius : 10,
		visible : false,font : {
			fontSize : 16 + Ti.App.paddingLabels + 'sp'
		},
		title:'Logout',
		color:'white',
		backgroundColor : Ti.App.colorPrincipal,
		left : 40 * formulaWidth
	});
	loginViewFonsGris.add(viewSortir);
}

	var labelSortir = Ti.UI.createLabel({
		text : 'Logout',
		left : 0 * formulaWidth,

		textAlign : 'center',
		height : isAndroid ? 40 * formulaHeight : 40,
		width : 240 * formulaWidth,
		color : 'white',
		top : 0 * formulaHeight,
		font : {
			fontSize : 16 + Ti.App.paddingLabels + 'sp'
		}
	});
	viewSortir.add(labelSortir);
	viewSortir.addEventListener('click', function(e) {
		Titanium.App.Properties.setString("loggedIn", "false");
		viewSortir.hide();
		viewEntrar.show();
		viewRegistre.show();
		botoIdiomaInstance.bottom=40*formulaHeight;
			labelRegistre.show();
		camps.show();
		labelLoggedAs.visible = false;
		nomUsuariLabel.visible = false;
		// loginView.visible=false;

		Titanium.App.Properties.setString("saltedPassword", '');
		Titanium.App.Properties.setString("user", '');
		//Ti.App.finestra.remove(tableViewMeusInstance);
		taPassword.value = '';
		taLogin.value = '';
	});

		var viewRegistre = Ti.UI.createView({
		width : 35 * formulaWidth,
		height : isAndroid ? 35 * formulaHeight : 35,
		top : 235 * formulaHeight,
		bubbleParent : false,
		borderRadius : 10,
		
		backgroundImage : '/images/registre.png',
		left : 40 * formulaWidth
	});
	// loginViewFonsGris.add(viewRegistre);
	
	viewRegistre.addEventListener('click', function(e) {
			
				Ti.Platform.openURL(win.urlBase+'/'+idiomaCurt+'_ES/user/register');
				
			});

var labelRegistre = Ti.UI.createLabel({
		text : registre,
		left : 80 * formulaWidth,
		height : isAndroid ? 35 * formulaHeight : 35,
		width : 185 * formulaWidth,
		visible:true,
		
		font : {
			fontSize : 12 + Ti.App.paddingLabels + 'sp'
		},
		color : '#8e8e93',
		top :235 * formulaHeight
	});
	// loginViewFonsGris.add(labelRegistre);
	
	labelRegistre.addEventListener('click', function(e) {
			
				Ti.Platform.openURL(urlBase+'/'+idiomaCurt+'_ES/user/register');
				
			});
			
	var loggedIn = Titanium.App.Properties.getString("loggedIn");
	var nomLoguejat = Titanium.App.Properties.getString("user");
	if (loggedIn == 'true') {
botoIdiomaInstance.bottom=170*formulaHeight;
		viewSortir.show();
		viewEntrar.hide();
viewRegistre.hide();
			labelRegistre.hide();
		camps.visible = false;
		labelLoggedAs.visible = true;
		nomUsuariLabel.visible = true;
		
		nomUsuariLabel.text = nomLoguejat;
	}
	return loginView;

}

module.exports = fitxaLogin;
