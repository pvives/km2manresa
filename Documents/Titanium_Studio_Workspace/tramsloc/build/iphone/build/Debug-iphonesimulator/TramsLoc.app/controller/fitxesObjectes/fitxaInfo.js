function createFitxaInfo(data, urlBase,tipusObjecte,numTabs) {

	//Crea les fitxes de tots els objectes
	var widthpantalla = Ti.Platform.displayCaps.platformWidth;
	var heightpantalla = Ti.Platform.displayCaps.platformHeight;
	var formulaWidth = widthpantalla / 320;
	var formulaHeight = heightpantalla / 480;

	if (Titanium.Platform.osname === 'android') {
		var isAndroid = true;
	}
	var statusBarHeight = Ti.Platform.displayCaps.logicalDensityFactor * 25;

	var fitxaInfo = Ti.UI.createView({
		width : 270 * formulaWidth,
		height : isAndroid ? 480 * formulaHeight - statusBarHeight - 44 * formulaHeight - 220 * formulaHeight : 480 * formulaHeight - 20 - 220-20,
		top : isAndroid ? 220 * formulaHeight : 220,
		left:0

	});
	if(tipusObjecte=='punt'){

		fitxaInfo.top=isAndroid ? 220 * formulaHeight+44* formulaHeight : 220+44;
	}

	
	//scrollview descripcio punt

	var scrollViewDescripcio = Ti.UI.createScrollView({
		width : 270 * formulaWidth,
		height : isAndroid ? 480 * formulaHeight - statusBarHeight - 44 * formulaHeight - 44 * formulaHeight - 220 * formulaHeight : 480 * formulaHeight - 20 - 44 - 220 -44-20-5-5,
		top : isAndroid ? 0 * formulaHeight : 0,
		left : 0 * formulaWidth,
		
		contentHeight : Ti.UI.SIZE
	});
	var labelDescripcio = Ti.UI.createLabel({
		text : data.descripcio,
		top : isAndroid ? 10 * formulaHeight : 10,
		left : 10 * formulaWidth,
		height : Ti.UI.SIZE,
		color : 'white',
		textAlign : 'left',
		width : 250 * formulaWidth,
		font : {
			fontSize :14+Ti.App.paddingLabels+'sp' 
		}
	});

	scrollViewDescripcio.add(labelDescripcio);
if(numTabs==1){
	
	scrollViewDescripcio.height = isAndroid ? 480 * formulaHeight - statusBarHeight - 44 * formulaHeight - 220 * formulaHeight : 480 * formulaHeight - 20 - 220-20-44;
	
	if(tipusObjecte=='maquina'){
	scrollViewDescripcio.height = isAndroid ? 480 * formulaHeight - statusBarHeight - 44 * formulaHeight - 220 * formulaHeight-20 * formulaHeight : 480 * formulaHeight - 20 - 220-20-44-20;
}
}
else{
	if(tipusObjecte=='maquina'){
	scrollViewDescripcio.height = isAndroid ? 480 * formulaHeight - statusBarHeight - 44 * formulaHeight - 44 * formulaHeight - 220 * formulaHeight-20 * formulaHeight : 480 * formulaHeight - 20 - 44 - 220 -44-20-5-5-20;
labelDescripcio.top=isAndroid ? 30 * formulaHeight : 10;
}
}



fitxaInfo.add(scrollViewDescripcio);

	return fitxaInfo;

}

module.exports = createFitxaInfo;
