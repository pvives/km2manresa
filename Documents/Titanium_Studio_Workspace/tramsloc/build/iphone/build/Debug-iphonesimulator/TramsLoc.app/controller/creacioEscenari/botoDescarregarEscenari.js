function botoDescarregarEscenari(escenariDescarregat,url,arrayObjectesEscenarisOffline,arrayEscenarisOffline,detallView,downloaderWindowInstance,urlBase,win) {
	if (Titanium.Platform.osname === 'android') {
		var isAndroid = true;
	}

	var widthpantalla = Ti.Platform.displayCaps.platformWidth;
	var heightpantalla = Ti.Platform.displayCaps.platformHeight;
	var formulaWidth = widthpantalla / 320;
	var formulaHeight = heightpantalla / 480;
	var statusBarHeight = Ti.Platform.displayCaps.logicalDensityFactor * 25;

	if (Titanium.App.Properties.getString("language", "undefined") == 'ca') {
		Titanium.include('/controller/translations.js');
	} else if (Titanium.App.Properties.getString("language", "undefined") == 'es') {
		Titanium.include('/controller/translations_es.js');
	} else if (Titanium.App.Properties.getString("language", "undefined") == 'en') {
		Titanium.include('/controller/translations_en.js');
	} else if (Titanium.App.Properties.getString("language", "undefined") == 'pt') {
		Titanium.include('/controller/translations_pt.js');
	} else if (Titanium.App.Properties.getString("language", "undefined") == 'pt-PT') {
		Titanium.include('/controller/translations_pt.js');
	} 
	else if (Titanium.App.Properties.getString("language", "undefined") == 'eus') {
		Titanium.include('/controller/translations_eus.js');
	} else {
		Titanium.include('/controller/translations_en.js');
	}

	if (!isAndroid) {
		var descarregaView = Ti.UI.createButton({
			width : isAndroid ? 40* formulaHeight : 40,
			height : isAndroid ? 40 * formulaHeight : 40,
			backgroundImage : '/images/descargar.png',
			zIndex : 10,
			color : '#3e3d40',
			top : 2 * formulaHeight,
			
			right : 120 * formulaWidth
		});
	} else {
		var descarregaView = Ti.UI.createView({
			width : isAndroid ? 40* formulaHeight : 40,
			height : isAndroid ? 40 * formulaHeight : 40,
			backgroundImage : '/images/descargar.png',
			color : '#3e3d40',
			top : 2 * formulaHeight,

			right : isAndroid?20* formulaWidth:130 * formulaWidth
		});
	}
if(escenariDescarregat){
	descarregaView.backgroundImage= '/images/descargadoon.png';
}
	descarregaView.addEventListener('click', function(e) {
		if (!escenariDescarregat) {
			
								var alertDialog = Titanium.UI.createAlertDialog({
				title :vols_descarregar,
				buttonNames : [si,no]
			});
			alertDialog.show();
			
			alertDialog.addEventListener('click', function(e) {
				if(e.index==0){
					
							var data = require('/controller/creacioEscenari/Mod_networkController');
			var dataInstance = new data(url, descarregaContingut,downloaderWindowInstance);

			function descarregaContingut(dadesJSON) {
				
				var downloaderController = require('/controller/offline/DownloaderController');
				downloaderController.parseForMediaContent(dadesJSON, obreEscenari, urlBase);

				function obreEscenari(dadesJSONOffline) {
					//poso objectes escenari offline a properties

					arrayObjectesEscenarisOffline.push(JSON.stringify(dadesJSONOffline));
					Titanium.App.Properties.setList("arrayObjectesEscenarisOffline", arrayObjectesEscenarisOffline);

					//poso detall escenari offline a properties

					arrayEscenarisOffline.push(JSON.stringify(win.escenariJSON));
				
					Titanium.App.Properties.setList("arrayEscenarisOffline", arrayEscenarisOffline);
					Ti.App.escenariDescarregat = true;
					
// win.esborrarLlista()			//creo el escenari

					//creo el escenari
					var escenariController = require('/controller/creacioEscenari/escenariController');
					new escenariController(win, dadesJSONOffline);

if(!isAndroid){
		detallView.animate({
						left : -320 * formulaWidth,
						duration : 500
					});	
}
else{
	detallView.left=-320 * formulaWidth;
}			

					Ti.App.vincDescarregats = true;
				}

			}
				}
				else{
					alertDialog.hide();
				}
			});
	

		}
		else{
					var alertDialog = Titanium.UI.createAlertDialog({
				title : escenari_descarregat,
				buttonNames : ['Ok']
			});
			alertDialog.show();
		}

	});
	return descarregaView;
}

module.exports = botoDescarregarEscenari; 