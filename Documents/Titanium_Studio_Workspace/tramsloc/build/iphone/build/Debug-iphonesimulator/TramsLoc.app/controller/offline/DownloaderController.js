function parseForMediaContent(objectList, _callback_CloseDownloadWindow, urlBase) {
	
	var downloaderWindow = require('/controller/offline/DownloaderWindow');
	var downloaderWindowInstance = new downloaderWindow(descarregant);
	downloaderWindowInstance.open();

	var _callBack_DownloadOneFileFinished = function(download_result) {
		Ti.API.info('one file downloaded');
	};
	var _callBack_DownloadMultipleFileFinished = function() {
		
		 var alertDialog = Titanium.UI.createAlertDialog({
		 title : descarregat_correctament,
		 message : consultar_descarregats,
		 buttonNames : ['OK']
		 });
		 alertDialog.show();
		 
		downloaderWindowInstance.close();


	_callback_CloseDownloadWindow(objectList);
	
	};

	//var baseUrl = Ti.App.Properties.getString("url", null);
	var media = [];

	if (objectList != null) {
		for (var i = 0; i < objectList.length; i++) {
			if (objectList[i].tipus == "jocpista") {
				if (objectList[i].pistes.length != 0) {

					for (var j = 0; j < objectList[i].pistes.length; j++) {
						if (objectList[i].pistes[j].galeria != null) {
							//audio pista
							if (objectList[i].pistes[j].galeria.pathaudio != null) {
								media.push(objectList[i].pistes[j].galeria.pathaudio);
								var urlfilename = objectList[i].pistes[j].galeria.pathaudio.split('/');
								var filename = urlfilename[urlfilename.length - 1];
								filename = filename.split("%20").join("_");
								objectList[i].pistes[j].galeria.pathaudio = Ti.Filesystem.applicationDataDirectory + filename;

							}
							//galeria imatges pista
							 if (objectList[i].pistes[j].galeria.imatges.length != 0) {
							 	
								for (var z = 0; z < objectList[i].pistes[j].galeria.imatges.length; z++) {
									media.push(objectList[i].pistes[j].galeria.imatges[z].pathImatge);
									
									var urlfilename = objectList[i].pistes[j].galeria.imatges[z].pathImatge.split('/');
									var filename = urlfilename[urlfilename.length - 1];
									filename = filename.split("%20").join("_");
									objectList[i].pistes[j].galeria.imatges[z].pathImatge = Ti.Filesystem.applicationDataDirectory + filename;
								}
							}
						}

						//audio meta
						 if (objectList[i].audio_resposta != null) {
							media.push(objectList[i].audio_resposta);
							var urlfilename = objectList[i].audio_resposta.split('/');
							var filename = urlfilename[urlfilename.length - 1];
							filename = filename.split("%20").join("_");
							objectList[i].audio_resposta = Ti.Filesystem.applicationDataDirectory + filename;

						}
						//galeria imatges meta
						 if (objectList[i].foto_resposta != null) {
							media.push(objectList[i].foto_resposta);
							var urlfilename = objectList[i].foto_resposta.split('/');
							var filename = urlfilename[urlfilename.length - 1];
							filename = filename.split("%20").join("_");
							objectList[i].foto_resposta = Ti.Filesystem.applicationDataDirectory + filename;

						}
						//imatge resum joc pistes

					}
					if (objectList[i].pathfoto != null) {
						media.push(objectList[i].pathfoto);
						var urlfilename = objectList[i].pathfoto.split('/');
						var filename = urlfilename[urlfilename.length - 1];
						filename = filename.split("%20").join("_");
						objectList[i].pathfoto = Ti.Filesystem.applicationDataDirectory + filename;

					}
				}
			} else if (objectList[i].tipus == "punt") {
				if (objectList[i].galeria != null &&objectList[i].galeria.imatges != null) {
					if (objectList[i].galeria.pathaudio != null) {
						media.push(objectList[i].galeria.pathaudio);
						var urlfilename = objectList[i].galeria.pathaudio.split('/');
						var filename = urlfilename[urlfilename.length - 1];
						filename = filename.split("%20").join("_");
						objectList[i].galeria.pathaudio = Ti.Filesystem.applicationDataDirectory + filename;
					} if (objectList[i].galeria.imatges.length != 0) {
						for (var z = 0; z < objectList[i].galeria.imatges.length; z++) {
							
							media.push(objectList[i].galeria.imatges[z].pathImatge);
							var urlfilename = objectList[i].galeria.imatges[z].pathImatge.split('/');
							var filename = urlfilename[urlfilename.length - 1];
							filename = filename.split("%20").join("_");
							objectList[i].galeria.imatges[z].pathImatge = Ti.Filesystem.applicationDataDirectory + filename;
						}
					}
				}

			} else if (objectList[i].tipus == "maquina") {
				if(objectList[i].instantanies!=null){
				if (objectList[i].instantanies.length != 0) {
					for (var j = 0; j < objectList[i].instantanies.length; j++) {
						if (objectList[i].instantanies[j].galeria != '') {
							//audio instantania
							if (objectList[i].instantanies[j].galeria.pathaudio != null) {
								media.push(objectList[i].instantanies[j].galeria.pathaudio);
								var urlfilename = objectList[i].instantanies[j].galeria.pathaudio.split('/');
								var filename = urlfilename[urlfilename.length - 1];
								filename = filename.split("%20").join("_");
								objectList[i].instantanies[j].galeria.pathaudio = Ti.Filesystem.applicationDataDirectory + filename;

							}
							//galeria imatges instantania
							// if(objectList[i].instantanies[j].galeria.imatges!=null){
							 if (objectList[i].instantanies[j].galeria.imatges.length != 0) {
																
																	
									media.push(objectList[i].instantanies[j].galeria.imatges[0].pathImatge);
									var urlfilename = objectList[i].instantanies[j].galeria.imatges[0].pathImatge.split('/');
									var filename = urlfilename[urlfilename.length - 1];
									filename = filename.split("%20").join("_");
									objectList[i].instantanies[j].galeria.imatges[0].pathImatge = Ti.Filesystem.applicationDataDirectory + filename;
								}
							}
						// }
						}
					}
				
				}
				//imatge escenari
				if (objectList[i].foto != null) {
					media.push(objectList[i].foto);
					var urlfilename = objectList[i].foto.split('/');
					var filename = urlfilename[urlfilename.length - 1];
					filename = filename.split("%20").join("_");
					objectList[i].foto = Ti.Filesystem.applicationDataDirectory + filename;

				}
			}
		}
	}

	// if(objectList != null){
	// for(key in objectList){
	// if(key == "pregunta1" || key == "pregunta2" || key == "pregunta3" || key == "pregunta4" || key == "pregunta5" || key == "pregunta6" || key == "pregunta7" || key == "pregunta8"){
	// if(objectList[key][0]["tipus"] != "pregunta_model_a" && objectList[key][0]["tipus"] != "pregunta_model_b"){
	// for(var i=0; i<objectList[key][0]["respostes"].length; i++){
	// media.push(objectList[key][0]["respostes"][i]);
	// var urlfilename = objectList[key][0]["respostes"][i].split('/');
	// var filename = urlfilename[urlfilename.length - 1];
	// filename = filename.split("%20").join("_");
	// objectList[key][0]["respostes"][i] = Ti.Filesystem.applicationDataDirectory + filename;
	// }
	// }
	// else if (objectList[key][0]["tipus"] == "pregunta_model_b"){
	// media.push(objectList[key][0]["foto"]);
	// var urlfilename = objectList[key][0]["foto"].split('/');
	// var filename = urlfilename[urlfilename.length - 1];
	// filename = filename.split("%20").join("_");
	// objectList[key][0]["foto"] = Ti.Filesystem.applicationDataDirectory + filename;
	// }
	// }
	// }
	// }

	/*
	 * Creo la cua de descàrrega
	 */

	var downloadQueueArray = [];
	for (var i = 0; i < media.length; i++) {
		var fileName = media[i].split("/");
		var fileNameWithReplacedSpaces = fileName[fileName.length - 1].split("%20").join("_");
		var object = {
			'url' : urlBase + "/" + media[i],
			'filepath' : Ti.Filesystem.applicationDataDirectory + fileNameWithReplacedSpaces
		};

		if (!Ti.Filesystem.getFile(object.filepath).exists()) {
			downloadQueueArray.push(object);
			Ti.API.info('Object for dowload: ' + object.url + " path: " + object.filepath);
		}
	}
	Ti.API.info(downloadQueueArray);

	/*
	 * Crido al codi que gestiona la descàrrega
	 */

	var downloadManager = require('/controller/offline/download_utility').createDownloadManager();
	downloadManager.downloadMultiFile(downloadQueueArray, _callBack_DownloadOneFileFinished, _callBack_DownloadMultipleFileFinished, 1);
}

exports.parseForMediaContent = parseForMediaContent;

function esborrarEscenari(objectList) {

	var media = [];

	var objectList = JSON.parse(objectList);

	if (objectList != null) {
		for (var i = 0; i < objectList.length; i++) {

			if (objectList[i].tipus == "jocpista") {
				if (objectList[i].pistes.length != 0) {

					for (var j = 0; j < objectList[i].pistes.length; j++) {
						if (objectList[i].pistes[j].galeria != null) {
							//audio pista
							if (objectList[i].pistes[j].galeria.pathaudio != null) {
								media.push(objectList[i].pistes[j].galeria.pathaudio);

							}
							//galeria imatges pista
							else if (objectList[i].pistes[j].galeria.imatges != null) {
								for (var z = 0; z < objectList[i].pistes[j].galeria.imatges.length; z++) {
									media.push(objectList[i].pistes[j].galeria.imatges[z].pathImatge);

								}
							}
						}

						//audio meta
						 if (objectList[i].audio_resposta != null) {
							media.push(objectList[i].audio_resposta);

						}
						//galeria imatges meta
						 if (objectList[i].foto_resposta != null) {
							media.push(objectList[i].foto_resposta);

						}
						//imatge resum joc pistes

					}
					if (objectList[i].pathfoto != null) {
						media.push(objectList[i].pathfoto);

					}
				}
			} else if (objectList[i].tipus == "punt") {

				if (objectList[i].galeria != null) {
					if (objectList[i].galeria.pathaudio != null) {
						media.push(objectList[i].galeria.pathaudio);

					}  if (objectList[i].galeria.imatges != null) {
						for (var z = 0; z < objectList[i].galeria.imatges.length; z++) {
							media.push(objectList[i].galeria.imatges[z].pathImatge);

						}
					}
				}

			} else if (objectList[i].tipus == "maquina") {
				if (objectList[i].instantanies.length != 0) {
					for (var j = 0; j < objectList[i].instantanies.length; j++) {
						if (objectList[i].instantanies[j].galeria != null) {
							//audio instantania
							if (objectList[i].instantanies[j].galeria.pathaudio != null) {
								media.push(objectList[i].instantanies[j].galeria.pathaudio);

							}
							//galeria imatges instantania
							 if (objectList[i].instantanies[j].galeria.imatges != null) {
								for (var z = 0; z < objectList[i].instantanies[j].galeria.imatges.length; z++) {
									media.push(objectList[i].instantanies[j].galeria.imatges[z].pathImatge);

								}
							
							}
						}
					}
				}
				
				//imatge escenari
				if (objectList[i].foto != null) {
					media.push(objectList[i].foto);

				}
			}
		}

		for (var i = 0; i < media.length; i++) {

			var contentToDelete = Ti.Filesystem.getFile(media[i]);
			contentToDelete.deleteFile();
		}
	}

			var alertDialog = Titanium.UI.createAlertDialog({
				title : esborrat_correctament,
				buttonNames : ['Ok']
			});
			alertDialog.show();
			
	
}

exports.esborrarEscenari = esborrarEscenari;

function removeAllDownloadedData() {
	var dataDirectory = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory);
	var directoryListing = dataDirectory.getDirectoryListing();
	if (directoryListing != null) {
		for (var i = 0; i < directoryListing.length; i++) {
			var fileToDelete = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory + directoryListing[i]);
			fileToDelete.deleteFile();
		}
		alert("All files have been removed from the device.");
	}
}

exports.removeAllDownloadedData = removeAllDownloadedData;
