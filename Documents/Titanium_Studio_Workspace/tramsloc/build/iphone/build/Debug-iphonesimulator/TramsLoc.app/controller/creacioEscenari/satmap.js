function createSatMap(viewEscenari,mapView) {
	
		//Crea les fitxes de tots els objectes
	var widthpantalla = Ti.Platform.displayCaps.platformWidth;
	var heightpantalla = Ti.Platform.displayCaps.platformHeight;
	var formulaWidth = widthpantalla / 320;
	var formulaHeight = heightpantalla / 480;

	if (Titanium.Platform.osname === 'android') {
		var isAndroid = true;
	}
	 var MapModule=require('ti.map');
	var statusBarHeight = Ti.Platform.displayCaps.logicalDensityFactor * 25;
	
	//BOTO VISTA MAP
	var vistaMap = Titanium.UI.createView({
		backgroundImage : '/images/map.png',
		top : 54 *formulaHeight,
		height : isAndroid?30 *formulaHeight:30,
		width : 50 * widthpantalla / 320,
			zIndex : 1,
		left : 58 * widthpantalla / 320
	});

	vistaMap.addEventListener('click', function(e) {

		 mapView.setMapType(MapModule.NORMAL_TYPE);
		vistaMap.backgroundImage = '/images/mapon.png';
		vistaSat.backgroundImage = '/images/sat.png';

	});

	// //BOTO VISTA SAT
	var vistaSat = Titanium.UI.createView({
		backgroundImage : '/images/saton.png',
		zIndex : 1,
		top : 54 *formulaHeight,
		height : isAndroid?30 *formulaHeight:30,
		width : 50 * widthpantalla / 320,
		left : 10 * widthpantalla / 320
	})

	vistaSat.addEventListener('click', function(e) {

		 mapView.setMapType(MapModule.SATELLITE_TYPE);
		vistaMap.backgroundImage = isAndroid ? '/images/map.png' : '/images/map.png';
		vistaSat.backgroundImage = isAndroid ? '/images/saton.png' : '/images/saton.png';

	});

	viewEscenari.add(vistaMap);
	viewEscenari.add(vistaSat);

}

module.exports = createSatMap;
