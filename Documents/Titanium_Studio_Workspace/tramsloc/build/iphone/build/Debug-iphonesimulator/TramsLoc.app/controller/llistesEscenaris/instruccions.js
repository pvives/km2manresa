function infoMedalles(viewCentral) {
	if (Titanium.App.Properties.getString("language", "undefined") == 'ca') {
		Titanium.include('/controller/translations.js');
	} else if (Titanium.App.Properties.getString("language", "undefined") == 'es') {
		Titanium.include('/controller/translations_es.js');
	} else if (Titanium.App.Properties.getString("language", "undefined") == 'en') {
		Titanium.include('/controller/translations_en.js');
	} else if (Titanium.App.Properties.getString("language", "undefined") == 'pt') {
		Titanium.include('/controller/translations_pt.js');
	} else if (Titanium.App.Properties.getString("language", "undefined") == 'pt-PT') {
		Titanium.include('/controller/translations_pt.js');
	}
	else if (Titanium.App.Properties.getString("language", "undefined") == 'eus') {
		Titanium.include('/controller/translations_eus.js');
	}  else {
		Titanium.include('/controller/translations_en.js');
	}
var widthpantalla = Ti.Platform.displayCaps.platformWidth;
var heightpantalla = Ti.Platform.displayCaps.platformHeight;
var formulaWidth = widthpantalla / 320;
var formulaHeight = heightpantalla / 480;
var paddingLabels = 0;

	
var statusBarHeight=Ti.Platform.displayCaps.logicalDensityFactor*25;

	if (Titanium.Platform.osname === 'android') {
		var isAndroid = true;
	}
	
var viewInfo = Ti.UI.createView({
		width : 320 * formulaWidth,
		height : Ti.UI.SIZE,
		left : 0 * formulaWidth,
	zIndex:7,
		top : 0 * formulaHeight

	});
	
	var viewInfoOpacity = Ti.UI.createView({
		width : 320 * formulaWidth,
		height : isAndroid ?  480 * formulaHeight - statusBarHeight : 480 * formulaHeight - 20,
		left : 0 * formulaWidth,
		top : 0 * formulaHeight,
		opacity:0.5,
		backgroundColor : 'black'

	});
	viewInfo.add(viewInfoOpacity);
	
	var viewText = Ti.UI.createView({
		width : 260 * formulaWidth,
		height : Ti.UI.SIZE,
		left : 30 * formulaWidth,
		zIndex:10,
			borderRadius:10,
		top : 15 * formulaHeight,	
		backgroundColor : 'white'

	});
	
	viewInfo.add(viewText);
	
		
	
		var labelInfo = Ti.UI.createLabel({
		text :info_descarregats,
		left : 15 * formulaWidth,
		height :Ti.UI.SIZE,
		width : 230 * formulaWidth,
		color : 'black',
		zIndex:2,
		top : 10 * formulaHeight,
		font : {
			fontSize : 14+Ti.App.paddingLabels+'sp'
		}
	});
	
	viewText.add(labelInfo);
	

	
	viewInfo.addEventListener('click', function(e) {
		viewCentral.remove(viewInfo);
		
	});

	return viewInfo;
}

module.exports = infoMedalles;