function create4tabs(imatgetab1, viewtab1, imatgetab2, viewtab2, imatgetab3, viewtab3, imatgetab4, viewtab4, imatgetab5, viewtab5, numTabs,vincDe,dataPunt) {

	var widthpantalla = Ti.Platform.displayCaps.platformWidth;
	var heightpantalla = Ti.Platform.displayCaps.platformHeight;
	var formulaWidth = widthpantalla / 320;
	var formulaHeight = heightpantalla / 480;

	if (Titanium.Platform.osname === 'android') {
		var isAndroid = true;
	}

	var viewTabs = Ti.UI.createView({
		width : 270 * formulaWidth,
		height : 44 * formulaHeight,
		right : 0 * formulaWidth,
		layout : 'horizontal',
		zIndex : 10,
		bottom : vincDe=='jocpistes' ? 44* formulaHeight : 0,
		backgroundColor : 'black'

	});

	if (viewtab1 != null) {

		var tab1Marcat = Ti.UI.createView({
			width : 270 * formulaWidth / numTabs,
			height : 44 * formulaHeight,
			left : 0 * formulaWidth,
			visible : true,
			bottom : isAndroid ? 0 * formulaHeight : 0,
			backgroundColor : '#4d4d4d'

		});
		viewTabs.add(tab1Marcat);

		tab1Marcat.addEventListener('click', function(e) {
			if (imatgetab1 == '/images/tabs/videosi.png' && isAndroid) {
				Ti.Platform.openURL(dataPunt.galeria.pathvideo);
			} else {
				tab1Marcat.backgroundColor = '#4d4d4d';
				viewtab1.show();
				if (viewtab2 != null) {
					tab2Marcat.backgroundColor = 'black';
					viewtab2.hide();
				}
				if (viewtab3 != null) {
					tab3Marcat.backgroundColor = 'black';
					viewtab3.hide();
				}
				if (viewtab4 != null) {
					tab4Marcat.backgroundColor = 'black';
					viewtab4.hide();
				}
				if (viewtab5 != null) {
					tab5Marcat.backgroundColor = 'black';
					viewtab5.hide();
				}
			}

		});

		var tab1 = Ti.UI.createView({
			width : 44 * formulaWidth,
			height : isAndroid ? 44 * formulaHeight : 44,
			left : tab1Marcat.width / 2 - 22 * formulaWidth,
			bottom : isAndroid ? 0 * formulaHeight : 0,
			backgroundImage : imatgetab1

		});
		tab1Marcat.add(tab1);
	}

	if (viewtab2 != null) {

		var tab2Marcat = Ti.UI.createView({
			width : 270 * formulaWidth / numTabs,
			height : 44 * formulaHeight,
			left : 0 * formulaWidth,
			visible : true,
			bottom : isAndroid ? 0 * formulaHeight : 0,
			backgroundColor : 'black'

		});
		viewTabs.add(tab2Marcat);

		tab2Marcat.addEventListener('click', function(e) {
			if (imatgetab2 == '/images/tabs/videosi.png' && isAndroid) {
				Ti.Platform.openURL(dataPunt.galeria.pathvideo);
			} else {
				tab1Marcat.backgroundColor = 'black';
				viewtab1.hide();
				if (viewtab2 != null) {

					tab2Marcat.backgroundColor = '#4d4d4d';
					viewtab2.show();
				}
				if (viewtab3 != null) {
					tab3Marcat.backgroundColor = 'black';
					viewtab3.hide();
				}
				if (viewtab4 != null) {
					tab4Marcat.backgroundColor = 'black';
					viewtab4.hide();
				}
				if (viewtab5 != null) {
					tab5Marcat.backgroundColor = 'black';
					viewtab5.hide();
				}
			}
		});

		var tab2 = Ti.UI.createView({
			width : 44 * formulaWidth,
			height : isAndroid ? 44 * formulaHeight : 44,
			left : tab2Marcat.width / 2 - 22 * formulaWidth,
			bottom : isAndroid ? 0 * formulaHeight : 0,
			backgroundImage : imatgetab2

		});
		tab2Marcat.add(tab2);
	}

	if (viewtab3 != null) {

		var tab3Marcat = Ti.UI.createView({
			width : 270 * formulaWidth / numTabs,
			height : 44 * formulaHeight,
			left : 0 * formulaWidth,
			visible : true,

			bottom : isAndroid ? 0 * formulaHeight : 0,
			backgroundColor : 'black'

		});
		viewTabs.add(tab3Marcat);

		tab3Marcat.addEventListener('click', function(e) {
			if (imatgetab3 == '/images/tabs/videosi.png' && isAndroid) {
				Ti.Platform.openURL(dataPunt.galeria.pathvideo);
			} else {
				tab1Marcat.backgroundColor = 'black';
				viewtab1.hide();
				if (viewtab2 != null) {
					tab2Marcat.backgroundColor = 'black';
					viewtab2.hide();
				}
				if (viewtab3 != null) {
					tab3Marcat.backgroundColor = '#4d4d4d';
					viewtab3.show();
				}
				if (viewtab4 != null) {
					tab4Marcat.backgroundColor = 'black';
					viewtab4.hide();
				}
				if (viewtab5 != null) {
					tab5Marcat.backgroundColor = 'black';
					viewtab5.hide();
				}
			}
		});

		var tab3 = Ti.UI.createView({
			width : 44 * formulaWidth,
			height : isAndroid ? 44 * formulaHeight : 44,
			left : tab3Marcat.width / 2 - 22 * formulaWidth,
			bottom : isAndroid ? 0 * formulaHeight : 0,
			backgroundImage : imatgetab3

		});
		tab3Marcat.add(tab3);

	}

	if (viewtab4 != null) {

		var tab4Marcat = Ti.UI.createView({
			width : 270 * formulaWidth / numTabs,
			height : 44 * formulaHeight,
			left : 0 * formulaWidth,
			visible : true,
			bottom : isAndroid ? 0 * formulaHeight : 0,
			backgroundColor : 'black'

		});
		viewTabs.add(tab4Marcat);

		tab4Marcat.addEventListener('click', function(e) {
			if (imatgetab4 == '/images/tabs/videosi.png' && isAndroid) {
				Ti.Platform.openURL(dataPunt.galeria.pathvideo);
			} else {
				tab1Marcat.backgroundColor = 'black';
				viewtab1.hide();
				if (viewtab2 != null) {
					tab2Marcat.backgroundColor = 'black';
					viewtab2.hide();
				}
				if (viewtab3 != null) {
					tab3Marcat.backgroundColor = 'black';
					viewtab3.hide();
				}
				if (viewtab4 != null) {
					tab4Marcat.backgroundColor = '#4d4d4d';
					viewtab4.show();
				}
				if (viewtab5 != null) {
					tab5Marcat.backgroundColor = 'black';
					viewtab5.hide();
				}
			}
		});

		var tab4 = Ti.UI.createView({
			width : 44 * formulaWidth,
			height : isAndroid ? 44 * formulaHeight : 44,
			left : tab4Marcat.width / 2 - 22 * formulaWidth,
			bottom : isAndroid ? 0 * formulaHeight : 0,
			backgroundImage : imatgetab4

		});
		tab4Marcat.add(tab4);
		}
		
		
		
			if (viewtab5 != null) {

		var tab5Marcat = Ti.UI.createView({
			width : 270 * formulaWidth / numTabs,
			height : 44 * formulaHeight,
			left : 0 * formulaWidth,
			visible : true,
			bottom : isAndroid ? 0 * formulaHeight : 0,
			backgroundColor : 'black'

		});
		viewTabs.add(tab5Marcat);

		tab5Marcat.addEventListener('click', function(e) {
			if (imatgetab5 == '/images/tabs/linksi.png' && isAndroid) {
				Ti.Platform.openURL(dataPunt.galeria.pathvideo);
			} else {
				tab1Marcat.backgroundColor = 'black';
				viewtab1.hide();
				if (viewtab2 != null) {
					tab2Marcat.backgroundColor = 'black';
					viewtab2.hide();
				}
				if (viewtab3 != null) {
					tab3Marcat.backgroundColor = 'black';
					viewtab3.hide();
				}
				if (viewtab4 != null) {
					tab4Marcat.backgroundColor = 'black';
					viewtab4.hide();
				}
				if (viewtab5 != null) {
					tab5Marcat.backgroundColor = '#4d4d4d';
					viewtab5.show();
				}
			}
		});

		var tab5 = Ti.UI.createView({
			width : 44 * formulaWidth,
			height : isAndroid ? 44 * formulaHeight : 44,
			left : tab5Marcat.width / 2 - 22 * formulaWidth,
			bottom : isAndroid ? 0 * formulaHeight : 0,
			backgroundImage : imatgetab5

		});
		tab5Marcat.add(tab5);

	}

	return viewTabs;

}

module.exports = create4tabs;
