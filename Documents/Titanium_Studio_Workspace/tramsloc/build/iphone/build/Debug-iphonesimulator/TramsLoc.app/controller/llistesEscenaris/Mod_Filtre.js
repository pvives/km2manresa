function Filtre(urlBase, winCentral, fonsGris) {

	if (Titanium.App.Properties.getString("language", "undefined") == 'ca') {
		Titanium.include('/controller/translations.js');
	} else if (Titanium.App.Properties.getString("language", "undefined") == 'es') {
		Titanium.include('/controller/translations_es.js');
	} else if (Titanium.App.Properties.getString("language", "undefined") == 'en') {
		Titanium.include('/controller/translations_en.js');
	} else if (Titanium.App.Properties.getString("language", "undefined") == 'pt') {
		Titanium.include('/controller/translations_pt.js');
	} else if (Titanium.App.Properties.getString("language", "undefined") == 'pt-PT') {
		Titanium.include('/controller/translations_pt.js');
	} else if (Titanium.App.Properties.getString("language", "undefined") == 'eus') {
		Titanium.include('/controller/translations_eus.js');
	} else {
		Titanium.include('/controller/translations_en.js');
	}

	if (Titanium.Platform.osname === 'android') {
		var isAndroid = true;
	}
	var colorTitols = '#353535';
	var colorTextArea = '#c2c2c2';
	var widthpantalla = Ti.Platform.displayCaps.platformWidth;
	var heightpantalla = Ti.Platform.displayCaps.platformHeight;
	var formulaWidth = widthpantalla / 320;
	var formulaHeight = heightpantalla / 480;
	var statusBarHeight = isAndroid ? Ti.Platform.displayCaps.logicalDensityFactor * 25 : 20;

	var edatAutor = '';
	var publicObjectiu = '';
	var areesConeixement = '';
	var customFont = 'Anna Beta';
	if (Ti.Platform.osname == 'android') {
		// on Android, use the "base name" of the file (name without extension)
		customFont = 'AnnaBeta-Regular';
	}

	var fitxaFiltreAvancat = Ti.UI.createView({
		width : 640 * formulaWidth,

		height : isAndroid ? 480 * formulaHeight - statusBarHeight - 44 * formulaHeight : 480 * formulaHeight - statusBarHeight - 44,
		top : isAndroid ? -480 * formulaHeight - 44 : -480 * formulaHeight - 44,
		zIndex : 3,
		backgroundColor : '#ebebeb',
		left : 0 * formulaWidth
	});
		var botoBack = Ti.UI.createButton({
	width : 44 * formulaWidth,
	height : isAndroid ? 44 * formulaHeight : 44,
visible:false,
	left : 0 * formulaWidth,
	top : 0 * formulaHeight,
	backgroundImage : '/images/back.png'

});
barraSuperior.add(botoBack);

	botoBack.addEventListener('click', function(e) {
			labelBarraSuperior.text=biblioteca;
		menuButton.show();
		botoBack.hide();
		fitxaFiltreAvancat.animate({
				left : isAndroid ? 0*formulaWidth : 0,
				duration : 500
			});	
	});

	var labelEscenarisFiltre = Ti.UI.createLabel({

		left : 10 * formulaWidth,
		textAlign : 'center',
		height : Ti.UI.SIZE,
		width : 300 * formulaWidth,
		color : '#b7b7b7',
		bottom : 90 * formulaHeight,
		font : {
			fontWeight : 'bold',
			fontSize : 13 + Ti.App.paddingLabels + 'sp',
			fontFamily : Ti.App.entorn == 'eduloc' ? customFont : 'Helvetica'
		},
	});
	fitxaFiltreAvancat.add(labelEscenarisFiltre);

	var scrollView = Ti.UI.createScrollView({
		width : 320 * formulaWidth,
		height : isAndroid ? 480 * formulaHeight - 44 * formulaHeight - statusBarHeight - 100 * formulaHeight : 480 * formulaHeight - 44 - statusBarHeight - 100 * formulaHeight,
		top : 0 * formulaHeight,
		zIndex : 4,
		left : 0 * formulaWidth,
		backgroundColor : 'transparent',
		contentHeight : 'auto'
	});
	fitxaFiltreAvancat.add(scrollView);

	var botoOk = Ti.UI.createButton({
		width : 140 * formulaWidth,
		height : isAndroid ? 30 * formulaHeight : 30,
		bottom : 60 * formulaHeight,
		color : Ti.App.colorPrincipal,
		borderRadius : 10,
		title : 'Ok',
		font : {
			fontFamily : customFont,
			fontWeight : 'bold',
			fontSize : 16 + Ti.App.paddingLabels + 'sp'
		},

		backgroundColor : '#353535',
		left : 90 * formulaWidth
	});

	fitxaFiltreAvancat.add(botoOk);

	botoOk.addEventListener('click', function(e) {
		filtreInstance.animate({
			top : isAndroid ? -480 * formulaHeight - 44 * formulaHeight : -480 * formulaHeight - 44,
			duration : 500
		});

		filtreObert = false;
	});
	//FILTRES
	var labelParaulaClau = Ti.UI.createLabel({
		text : paraula_clau,
		left : 25 * formulaWidth,
		height : 'auto',
		width : Ti.UI.SIZE,
		color : colorTitols,
		font : {
			fontWeight : 'bold',
			fontSize : 14 + Ti.App.paddingLabels + 'sp'
		},
		top : 20 * formulaHeight
	});
	scrollView.add(labelParaulaClau);

	taParaulaClau = Titanium.UI.createTextArea({
		editable : true,
		left : 25 * formulaWidth,
		value : '',

		returnKeyType : Titanium.UI.RETURNKEY_DONE,
		verticalAlign : Ti.UI.TEXT_VERTICAL_ALIGNMENT_CENTER,
		height : isAndroid ? 32 * formulaHeight : 32,
		width : 270 * formulaWidth,
		top : 35 * formulaHeight,
		font : {
			fontSize : '13sp'
		},

		color : colorTextArea,
		textAlign : 'left',
		suppressReturn : true
	});

	taParaulaClau.addEventListener('return', function(e) {
		taParaulaClau.value = taParaulaClau.value;
		
		do_search(taParaulaClau.value, taParaulaClau.value, taPoblacio.value, taIdioma.value, taNomEscola.value, taAutor.value);

		taParaulaClau.blur();
	});

	scrollView.add(taParaulaClau);

	var labelAutor = Ti.UI.createLabel({
		text : autor,
		left : 25 * formulaWidth,
		height : 'auto',
		width : Ti.UI.SIZE,
		color : colorTitols,
		font : {
			fontWeight : 'bold',
			fontSize : 14 + Ti.App.paddingLabels + 'sp'
		},
		top : 72 * formulaHeight
	});
	scrollView.add(labelAutor);

	taAutor = Titanium.UI.createTextArea({
		editable : true,
		left : 25 * formulaWidth,
		value : '',
		verticalAlign : Ti.UI.TEXT_VERTICAL_ALIGNMENT_CENTER,
		returnKeyType : Titanium.UI.RETURNKEY_DONE,

		height : isAndroid ? 32 * formulaHeight : 32,
		width : 130 * formulaWidth,
		top : 88 * formulaHeight,
		font : {
			fontSize : '13sp'
		},

		color : colorTextArea,
		textAlign : 'left',
		suppressReturn : true
	});

	taAutor.addEventListener('return', function(e) {
		taAutor.value = taAutor.value;
		do_search(taParaulaClau.value, taParaulaClau.value, taPoblacio.value, taIdioma.value,  taNomEscola.value, taAutor.value);

		taAutor.blur();

	});

	scrollView.add(taAutor);

	var labelPoblacio = Ti.UI.createLabel({
		text : ambit_geografic,
		right : 25 * formulaWidth,
		height : 'auto',
		width : 130 * formulaWidth,
		color : colorTitols,
		font : {
			fontWeight : 'bold',
			fontSize : 14 + Ti.App.paddingLabels + 'sp'
		},
		top : 125 * formulaHeight
	});
	scrollView.add(labelPoblacio);

	taPoblacio = Titanium.UI.createTextArea({
		editable : true,
		right : 25 * formulaWidth,
		value : '',
		verticalAlign : Ti.UI.TEXT_VERTICAL_ALIGNMENT_CENTER,
		returnKeyType : Titanium.UI.RETURNKEY_DONE,
		height : isAndroid ? 32 * formulaHeight : 32,
		width : 130 * formulaWidth,
		top : 141 * formulaHeight,
		font : {
			fontSize : '13sp'
		},

		color : colorTextArea,
		textAlign : 'left',
		suppressReturn : true
	});

	taPoblacio.addEventListener('return', function(e) {
		taPoblacio.value = taPoblacio.value;
		do_search(taParaulaClau.value, taParaulaClau.value, taPoblacio.value, taIdioma.value,  taNomEscola.value, taAutor.value);

		taPoblacio.blur();
	});

	scrollView.add(taPoblacio);

	var labelNomEscola = Ti.UI.createLabel({
		text : centre_educatiu,
		right : 25 * formulaWidth,
		height : 'auto',

		width : 130 * formulaWidth,
		color : colorTitols,
		font : {
			fontWeight : 'bold',
			fontSize : 14 + Ti.App.paddingLabels + 'sp'
		},
		top : 72 * formulaHeight
	});
	scrollView.add(labelNomEscola);

	taNomEscola = Titanium.UI.createTextArea({
		editable : true,
		right : 25 * formulaWidth,
		value : '',

		returnKeyType : Titanium.UI.RETURNKEY_DONE,
		verticalAlign : Ti.UI.TEXT_VERTICAL_ALIGNMENT_CENTER,
		height : isAndroid ? 32 * formulaHeight : 32,
		width : 130 * formulaWidth,
		top : 88 * formulaHeight,
		font : {
			fontSize : '13sp'
		},
		color : 'black',
		textAlign : 'left'
	});
	taNomEscola.addEventListener('return', function(e) {
		taNomEscola.value = taNomEscola.value;
		do_search(taParaulaClau.value, taParaulaClau.value, taPoblacio.value, taIdioma.value, taNomEscola.value, taAutor.value);

		taNomEscola.blur();
	});

	scrollView.add(taNomEscola);



	taIdioma = Titanium.UI.createTextArea({
		editable : true,
		left : 25 * formulaWidth,
		value : '',
		verticalAlign : Ti.UI.TEXT_VERTICAL_ALIGNMENT_CENTER,
		
		height : isAndroid ? 32 * formulaHeight : 32,
		width : 130 * formulaWidth,
		returnKeyType : Titanium.UI.RETURNKEY_DONE,
		//autocapitalization : false,
		top : 141 * formulaHeight,
		font : {
			fontSize : '13sp'
		},
		color : 'black',
		textAlign : 'left',
		suppressReturn : true
	});
		var labelIdioma = Ti.UI.createLabel({
		text : labelIdiomaTextMinuscula,
		left : 25 * formulaWidth,
		height : 'auto',
		width : 130 * formulaWidth,
		color : colorTitols,
		zIndex:1,
		font : {
			fontWeight : 'bold',
			fontSize : 14 + Ti.App.paddingLabels + 'sp'
		},
		top : 125 * formulaHeight
	});
	scrollView.add(labelIdioma);
	
	taIdioma.addEventListener('return', function(e) {
		taIdioma.value = taIdioma.value;
		do_search(taParaulaClau.value, taParaulaClau.value, taPoblacio.value, taIdioma.value,  taNomEscola.value, taAutor.value);

		taIdioma.blur();
	});

	scrollView.add(taIdioma);

	var labelAreesConeixement = Ti.UI.createLabel({
		text : area_coneixement,
		left : 35 * formulaWidth,
	height : isAndroid ? 35 * formulaHeight : 35,
	zIndex:5,
		width : Ti.UI.SIZE,
		color : colorTitols,
		font : {
			fontWeight : 'bold',
			fontSize : 14 + Ti.App.paddingLabels + 'sp'
		},
		top : 228 * formulaHeight
	});
	scrollView.add(labelAreesConeixement);
	
		var labelAreesConeixementSeleccio = Ti.UI.createLabel({
		text : totes,
		right : 55 * formulaWidth,
	height : isAndroid ? 35 * formulaHeight : 35,
	zIndex:5,
		width : Ti.UI.SIZE,
		color : colorTextArea,
		font : {
			fontWeight : 'bold',
			fontSize : 13 + Ti.App.paddingLabels + 'sp'
		},
		top : 228 * formulaHeight
	});
	scrollView.add(labelAreesConeixementSeleccio);
	// if (!isAndroid) {
	var taAreesConeixement = Titanium.UI.createButton({
		left : 25 * formulaWidth,
		value : '',
		textAlign : 'left',
		height : isAndroid ? 35 * formulaHeight : 35,
		width : 270 * formulaWidth,

		zIndex : 4,
		bubbleParent : false,
		top : 228 * formulaHeight,
		backgroundColor : 'white',
		backgroundImage : 'none',
		font : {
			fontSize : '12sp'
		},
		color : 'black',
		textAlign : 'left'

	});

	scrollView.add(taAreesConeixement);

	var desplegarAreesConeixement = Titanium.UI.createButton({
		right : 0 * formulaWidth,
		height : isAndroid ? 35 * formulaHeight : 35,
		width : 30 * formulaWidth,
		zIndex : 4,
		bottom : 0 * formulaHeight,
		backgroundImage : '/images/flecha.png'

	});

	taAreesConeixement.add(desplegarAreesConeixement);

	var dataAreesConeixement = [];
	dataAreesConeixement[0] = Ti.UI.createTableViewRow({
		title : totes,
		id : 0
	});
	dataAreesConeixement[1] = Ti.UI.createTableViewRow({
		title : ciencies_socials,
		id : 'socials'
	});
	dataAreesConeixement[2] = Ti.UI.createTableViewRow({
		title : llengua_literatura,
		id : 'lengua-literatura'
	});
	dataAreesConeixement[3] = Ti.UI.createTableViewRow({
		title : llengues_estrangeres,
		id : 'lengua-extranjera'
	});
	dataAreesConeixement[4] = Ti.UI.createTableViewRow({
		title : matematiques,
		id : 'matematicas'
	});
	dataAreesConeixement[5] = Ti.UI.createTableViewRow({
		title : musica,
		id : 'musica'
	});
	dataAreesConeixement[6] = Ti.UI.createTableViewRow({
		title : ciencies_naturals,
		id : 'naturales'
	});
	dataAreesConeixement[7] = Ti.UI.createTableViewRow({
		title : educacio_fisica,
		id : 'educacion-fisica'
	});
	dataAreesConeixement[8] = Ti.UI.createTableViewRow({
		title : educacio_artistica,
		id : 'educacion-artistica'
	});
	dataAreesConeixement[9] = Ti.UI.createTableViewRow({
		title : oci_temps_lliure,
		id : 'ocio'
	});
	dataAreesConeixement[10] = Ti.UI.createTableViewRow({
		title : altres,
		id : 'otros'
	});

	taAreesConeixement.addEventListener('click', function(e) {
		var detallEdatAutor = require('controller/llistesEscenaris/detallFiltrePicker');
		new detallEdatAutor(fitxaFiltreAvancat, dataAreesConeixement, taEdatAutor, do_search, taParaulaClau.value, taPoblacio.value, taIdioma.value, taNomEscola.value, taAutor.value,labelAreesConeixementSeleccio,botoBack,'areaConeixement');

		fitxaFiltreAvancat.animate({
			left : -320 * formulaWidth,
			duration : 500
		});
labelBarraSuperior.text= area_coneixement;
		menuButton.hide();
		botoBack.show();
	});
	// }
	var labelPublicObjectiu = Ti.UI.createLabel({
		text : public_objectiu,
		left : 35 * formulaWidth,
		zIndex:5,
			height : isAndroid ? 35 * formulaWidth : 35,
		width :Ti.UI.SIZE,
		color : colorTitols,
		font : {
			fontWeight : 'bold',
			fontSize : 14 + Ti.App.paddingLabels + 'sp'
		},
		top : 183 * formulaHeight
	});
	scrollView.add(labelPublicObjectiu);
	
		var labelPublicObjectiuSeleccio = Ti.UI.createLabel({
		text : tots,
		right : 55 * formulaWidth,
		zIndex:5,
			height : isAndroid ? 35 * formulaWidth : 35,
		width : Ti.UI.SIZE,
		color : colorTextArea,
		font : {
			fontWeight : 'bold',
			fontSize : 13 + Ti.App.paddingLabels + 'sp'
		},
		top : 183 * formulaHeight
	});
	scrollView.add(labelPublicObjectiuSeleccio);

	var taPublicObjectiu = Titanium.UI.createButton({
		left : 25 * formulaWidth,
		value : '',
		zIndex : 4,
		bubbleParent : false,

		backgroundImage : 'none',
		height : isAndroid ? 35 * formulaWidth : 35,
		width : 270 * formulaWidth,
		top : 183 * formulaHeight,
		textAlign : 'left',
		backgroundColor : 'white'

	});

	var desplegarPublicObjectiu = Titanium.UI.createButton({
		zIndex : 4,
		right : 0 * formulaWidth,
		height : isAndroid ? 35 * formulaHeight : 35,
		width : 35 * formulaWidth,
		bottom : 0 * formulaHeight,
		backgroundImage : '/images/flecha.png'

	});

	taPublicObjectiu.add(desplegarPublicObjectiu);

	var dataPublicObjectiu = [];
	dataPublicObjectiu[0] = Ti.UI.createTableViewRow({
		title : tots,
		id : 0
	});
	dataPublicObjectiu[1] = Ti.UI.createTableViewRow({
		title : _8_10_anys,
		id : 1
	});
	dataPublicObjectiu[2] = Ti.UI.createTableViewRow({
		title : _10_12_anys,
		id : 2
	});

	dataPublicObjectiu[3] = Ti.UI.createTableViewRow({
		title : _12_14_anys,
		id : 3
	});
	dataPublicObjectiu[4] = Ti.UI.createTableViewRow({
		title : _14_17_anys,
		id : 4
	});
	dataPublicObjectiu[5] = Ti.UI.createTableViewRow({
		title : _17_25_anys,
		id : 5
	});
	dataPublicObjectiu[6] = Ti.UI.createTableViewRow({
		title : _25_60_anys,
		id : 6
	});

	dataPublicObjectiu[7] = Ti.UI.createTableViewRow({
		title : majors_60,
		id : 7
	});

	taPublicObjectiu.addEventListener('click', function(e) {
		var detallEdatAutor = require('controller/llistesEscenaris/detallFiltrePicker');
		new detallEdatAutor(fitxaFiltreAvancat, dataPublicObjectiu, taEdatAutor, do_search, taParaulaClau.value, taPoblacio.value, taIdioma.value,  taNomEscola.value, taAutor.value,labelPublicObjectiuSeleccio,botoBack,'public');

		fitxaFiltreAvancat.animate({
			left : -320 * formulaWidth,
			duration : 500
		});
labelBarraSuperior.text= public_objectiu;
		menuButton.hide();
		botoBack.show();
	});
	scrollView.add(taPublicObjectiu);

	var labelEdatAutor = Ti.UI.createLabel({
		text : edat_autor,
		left : 35 * formulaWidth,
		height : isAndroid ? 35 * formulaHeight : 35,
		width : Ti.UI.SIZE,
		color : colorTitols,
		zIndex:5,
		font : {
			fontWeight : 'bold',
			fontSize : 14 + Ti.App.paddingLabels + 'sp'
		},
		top : 273 * formulaHeight
	});
	scrollView.add(labelEdatAutor);

	var labelEdatAutorSeleccio = Ti.UI.createLabel({
		text :totes,
		right : 55 * formulaWidth,
		height : isAndroid ? 35 * formulaHeight : 35,
		width : Ti.UI.SIZE,
		color : colorTextArea,
		zIndex:5,
		font : {
			fontWeight : 'bold',
			fontSize : 13 + Ti.App.paddingLabels + 'sp'
		},
		top : 273 * formulaHeight
	});
	scrollView.add(labelEdatAutorSeleccio);
	
	var taEdatAutor = Titanium.UI.createButton({
		bubbleParent : false,
		left : 25 * formulaWidth,
		// value : 'Indeferent',
		textAlign:'right',
		
		zIndex : 4,
		height : isAndroid ? 35 * formulaHeight : 35,

		width : 270 * formulaWidth,
		top : 273 * formulaHeight,
		backgroundImage : 'none',
		backgroundColor : 'white'
	});

	var desplegarEdatAutor = Titanium.UI.createButton({
		zIndex : 4,
		right : 0 * formulaWidth,
		height : isAndroid ? 35 * formulaHeight : 35,
		width : 35 * formulaWidth,
		bottom : 0 * formulaHeight,
		backgroundImage : '/images/flecha.png'

	});

	taEdatAutor.add(desplegarEdatAutor);

	var dataEdatAutor = [];
	dataEdatAutor[0] = Ti.UI.createTableViewRow({
		title : tots,
		id : 0
	});
	dataEdatAutor[1] = Ti.UI.createTableViewRow({
		title : _8_10_anys,
		id : 1
	});
	dataEdatAutor[2] = Ti.UI.createTableViewRow({
		title : _10_12_anys,
		id : 2
	});
	dataEdatAutor[3] = Ti.UI.createTableViewRow({
		title : _12_14_anys,
		id : 3
	});
	dataEdatAutor[4] = Ti.UI.createTableViewRow({
		title : _14_17_anys,
		id : 4
	});
	dataEdatAutor[5] = Ti.UI.createTableViewRow({
		title : _17_25_anys,
		id : 5
	});
	dataEdatAutor[6] = Ti.UI.createTableViewRow({
		title : _25_60_anys,
		color : 'black',
		id : 6
	});
	dataEdatAutor[7] = Ti.UI.createTableViewRow({
		title : majors_60,
		id : 7
	});

	taEdatAutor.addEventListener('click', function(e) {

		var detallEdatAutor = require('controller/llistesEscenaris/detallFiltrePicker');
		new detallEdatAutor(fitxaFiltreAvancat, dataEdatAutor, taEdatAutor, do_search, taParaulaClau.value, taPoblacio.value, taIdioma.value, taNomEscola.value, taAutor.value,labelEdatAutorSeleccio,botoBack,'edatAutor');

		fitxaFiltreAvancat.animate({
			left : -320 * formulaWidth,
			duration : 500
		});
labelBarraSuperior.text= edat_autor;
		menuButton.hide();
		botoBack.show();
	});
	scrollView.add(taEdatAutor);

	function do_search(paraula_clau2, nom_escenari, poblacio, idioma, nom_centre, autor) {

		fonsActInd.show();
		actInd.show();
		if (!isAndroid) {
			var downloaderWindow = require('/controller/offline/DownloaderWindow');
			var downloaderWindowInstance = new downloaderWindow(filtrar);
			downloaderWindowInstance.open();

		}

		Ti.App.paraula_clau = paraula_clau2;
		Ti.App.nom_escenari = nom_escenari;
		Ti.App.poblacio = poblacio;
		Ti.App.idioma = idioma;
		Ti.App.nom_centre = nom_centre;
		Ti.App.autor = autor;

		var dataFiltre = require('controller/llistesEscenaris/llistats_network_Controller');

		if (Ti.App.entorn == 'eduloc') {
			new dataFiltre(urlBase + '/mobile/scenario/library', callbackLlistatFiltrat, actInd);

		} else {
			new dataFiltre(urlBase + '/api/scenario/library/' + Ti.App.entorn + '/es', callbackLlistatFiltrat, actInd);

		}

		function callbackLlistatFiltrat(data) {


			if ((Ti.App.paraula_clau == ''||Ti.App.paraula_clau ==null)&&(Ti.App.nom_escenari == ''||Ti.App.nom_escenari == null)&&(Ti.App.poblacio == ''||Ti.App.poblacio == null)&&(Ti.App.idioma == ''||Ti.App.idioma == null)&&Ti.App.area_coneixement == 0 && Ti.App.public_objectiu == 0 && Ti.App.edat_autor == 0 &&(Ti.App.nom_centre == ''||Ti.App.nom_centre == null)&&(Ti.App.autor == ''||Ti.App.autor == null)){
				labelON.visible=false;
				filterButton.backgroundImage = '/images/filter.png';
				labelEscenarisFiltre.text = data.length + escenaris2;

			} else {
				filterButton.backgroundImage = '/images/filteron.png';
				labelEscenarisFiltre.text = data.length + escenaris_filtre;
labelON.visible=true;
			}

			if (tableViewBibliotecaInstance != null) {

				winCentral.remove(tableViewBibliotecaInstance);
				tableViewBibliotecaInstance = null;

			}
			if (viewFiltreInstance != null) {

				winCentral.remove(viewFiltreInstance);
				viewFiltreInstance = null;
			}

			labelEscenaris.text = data.length + escenaris2;

			var tableViewFiltre = require('controller/llistesEscenaris/Mod_llistats');
			viewFiltreInstance = new tableViewFiltre(urlBase, data);
			fonsActInd.hide();
			actInd.hide();
			winCentral.add(viewFiltreInstance);
		}

		if (downloaderWindowInstance != null) {
			downloaderWindowInstance.close();

		}

	}

	return fitxaFiltreAvancat;

}

module.exports = Filtre;
