function login (nomusuari,contrasenya,callback,url){

var login = require('/controller/llistesEscenaris/wsse');
//var sha512utils = require('sha512').sha512utils;

var primeraVegada=true;
var saltHttpPetition = Ti.Network.createHTTPClient({
        onload : function(e) {

                var responseSalt = JSON.parse(this.responseText);
                if(responseSalt.ERROR== "No existeix aquest usuari!"){
                	var alertDialog = Titanium.UI.createAlertDialog({
					title : usuari_no_existeix,
					buttonNames : ['Ok']
				});
				alertDialog.show();
                }
                else{
                	 generateSaltedPassword(responseSalt.salt);
                }
              
            
            
              
        },
        onerror : function(e) {
        		//this fires if Titanium/the native SDK cannot successfully retrieve a resource
		if (primeraVegada == true) {
			actInd.hide();
			fonsActInd.hide();
			var alertDialog = Titanium.UI.createAlertDialog({
				title : xhr_error_title,
				message : xhr_error_message,
				buttonNames : ['Ok']
			});
			alertDialog.show();
			primeraVegada = false;
		}
      
        }
});
saltHttpPetition.open('GET', url);
saltHttpPetition.send({
        '_username' : nomusuari
});

function generateSaltedPassword(salt){
        var loginHash = require('cat.itinerarium.loginhash');
        var hash = loginHash.processSHA512(contrasenya, salt, 5000);

       callback(hash);
       
     
}





}

module.exports = login;