if (Titanium.App.Properties.getString("language", "undefined") == 'ca') {
	Titanium.include('/controller/translations.js');
} else if (Titanium.App.Properties.getString("language", "undefined") == 'es') {
	Titanium.include('/controller/translations_es.js');
} else if (Titanium.App.Properties.getString("language", "undefined") == 'en') {
	Titanium.include('/controller/translations_en.js');
} else if (Titanium.App.Properties.getString("language", "undefined") == 'pt') {
	Titanium.include('/controller/translations_pt.js');
} else if (Titanium.App.Properties.getString("language", "undefined") == 'pt-PT') {
	Titanium.include('/controller/translations_pt.js');
} 
else if (Titanium.App.Properties.getString("language", "undefined") == 'eus') {
		Titanium.include('/controller/translations_eus.js');
	} else {
	Titanium.include('/controller/translations_en.js');
}

var widthpantalla = Ti.Platform.displayCaps.platformWidth;
var heightpantalla = Ti.Platform.displayCaps.platformHeight;
var formulaWidth = widthpantalla / 320;
var formulaHeight = heightpantalla / 480;

if (Titanium.Platform.osname === 'android') {
	var isAndroid = true;
}

function createfitxaMeta(dataPista, urlBase) {


	

	var numTabsMeta = 0;

	var fitxaMetaView = Ti.UI.createView({
		width : 270 * formulaWidth,
		top : isAndroid ? 0 * formulaHeight : 0,
		backgroundColor:'black',
		left : 0 * formulaWidth,
		height : isAndroid ? 480 * formulaHeight - statusBarHeight - 44 * formulaHeight : 480 * formulaHeight-20-20
	});
if(dataPista!=null){
	
	var barraSuperior = Ti.UI.createView({
		width : 270 * formulaWidth,
		height : isAndroid ? 44 * formulaHeight : 44,
		top : isAndroid ? 0 * formulaHeight : 0,
		backgroundColor : '#55c3ec',
		right : 0 * formulaWidth
	});
	fitxaMetaView.add(barraSuperior);

if (dataPista.nom != null &&dataPista.nom != 'undefined') {
	var labelTitolPista = Ti.UI.createLabel({
		text : dataPista.nom,
		top : isAndroid ? 0 * formulaHeight : 0,
		left : 44 * formulaWidth,
		color : 'black',
		height : isAndroid ? 44 * formulaHeight : 44,
		textAlign : 'center',
		width : 270 * formulaWidth-44 * formulaHeight-44 * formulaHeight,
		font : {
			fontSize : 17+Ti.App.paddingLabels+'sp' 
		}
	});
	barraSuperior.add(labelTitolPista);
}
		var imatgeMeta = Ti.UI.createView({
			backgroundColor : '#000',
			width : 125 * formulaWidth,
			height : isAndroid ? 125 * formulaHeight : 125,
		right : 72 * formulaWidth,
		top : isAndroid ? 44 * formulaHeight : 44,
			backgroundImage : '/images/hasllegado.png'
		});
		fitxaMetaView.add(imatgeMeta);
	

	//creo fitxa info
	var scrollViewDescripcio = Ti.UI.createScrollView({
		width : 270 * formulaWidth,
		//backgroundColor : 'red',
		 backgroundColor : '#4d4d4d',
		height : isAndroid ? 480 * formulaHeight - statusBarHeight - 44 * formulaHeight- 44 * formulaHeight - 220 * formulaHeight - 44 * formulaHeight: 480 * formulaHeight - 220-44-44,
		bottom : isAndroid ? 44 * formulaHeight+ 44 * formulaHeight : 44+44,
		left : 0 * formulaWidth,
		contentHeight : Ti.UI.SIZE
	});


if (dataPista.descripcio != null&&dataPista.descripcio != '') {
	var labelDescripcio = Ti.UI.createLabel({
		text : dataPista.resposta,
		top : isAndroid ? 10 * formulaHeight : 10,
		left : 10 * formulaWidth,
		height : Ti.UI.SIZE,
		color : 'white',
		textAlign : 'left',
		width : 250 * formulaWidth,
		font : {
			fontSize :  14+Ti.App.paddingLabels+'sp' 
		}
	});

	scrollViewDescripcio.add(labelDescripcio);
}
	numTabsMeta++;

	fitxaMetaView.add(scrollViewDescripcio);

	if (dataPista.audio_resposta != null&&dataPista.audio_resposta != '') {
		//Creo fitxa audio
		var viewAudio = Ti.UI.createView({
			width : 270 * formulaWidth,
			height : isAndroid ? 480 * formulaHeight - statusBarHeight - 44 * formulaHeight - 44 * formulaHeight - 220 * formulaHeight : 480 * formulaHeight - 44 - 220 - 44,
			bottom : isAndroid ? 0 * formulaHeight : 0,
			visible : false,
			backgroundColor : '#4d4d4d',
			left : 0 * formulaWidth

		});
		fitxaMetaView.add(viewAudio);

		var botoAudio = Ti.UI.createView({
			width : 80 * formulaWidth,
			height : isAndroid ? 44 * formulaHeight : 44,
			left : 95 * formulaWidth,
			bottom : isAndroid ? 44 * formulaHeight : 44,
			backgroundImage : '/images/playaudio.png'

		});
		viewAudio.add(botoAudio);

		var urlAudio = urlBase + '/' + dataPista.audio_resposta;

		var audioController = require('/controller/fitxesObjectes/audioController');
		audioControllerInstance = new audioController(urlAudio, botoAudio);

		botoAudio.addEventListener('click', function(e) {
			audioControllerInstance.click();
		});
		numTabsMeta++;

	}

	if (dataPista.video_resposta != '' && dataPista.video_resposta != null) {

		//Creo fitxa video
		var viewVideo = Ti.UI.createWebView({
			enableZoomControls : true,
			backgroundColor : '#4d4d4d',
			visible : false,
			left : 0 * formulaWidth,
			height : isAndroid ? 480 * formulaHeight - statusBarHeight - 44 * formulaHeight - 44 * formulaHeight : 480 * formulaHeight - 44 - 44,
			top : isAndroid ? 44 * formulaHeight : 44 * formulaHeight,
			url : dataPista.video_resposta,
			width : 270 * formulaWidth,
			showScrollbars : false
		});
		fitxaMetaView.add(viewVideo);

		isAndroid ? viewVideo.reload() : viewVideo.repaint();

		numTabsMeta++;
	}

	if (dataPista.url_resposta != null&&dataPista.url_resposta != '') {
		//Creo fitxa video
		var viewWeb = Ti.UI.createView({

			visible : false,
			left : 0 * formulaWidth,
			height : isAndroid ? 480 * formulaHeight - statusBarHeight - 44 * formulaHeight - 44 * formulaHeight : 480 * formulaHeight - 44 - 44,
			top : 44,

			width : 270 * formulaWidth
		});
		fitxaMetaView.add(viewWeb);
		//Creo fitxa web
		var barraWebView = Ti.UI.createView({
			width : 270 * formulaWidth,
			height : isAndroid ? 44 * formulaHeight : 44,
			left : 0 * formulaWidth,
			backgroundColor : 'black',
			top : isAndroid ? 0 * formulaHeight : 0

		});
		viewWeb.add(barraWebView);

		var botoBack = Ti.UI.createView({
			width : 44 * formulaWidth,
			height : isAndroid ? 44 * formulaHeight : 44,
			left : 20 * formulaWidth,
			top : isAndroid ? 0 * formulaHeight : 0,
			backgroundImage : '/images/controlsWeb/anteriorweb.png'

		});
		botoBack.addEventListener('click', function(e) {
			viewWeb.goBack();
		});
		barraWebView.add(botoBack);

		var botoForward = Ti.UI.createView({
			width : 44 * formulaWidth,
			height : isAndroid ? 44 * formulaHeight : 44,
			left : 83 * formulaWidth,
			top : isAndroid ? 0 * formulaHeight : 0,
			backgroundImage : '/images/controlsWeb/siguienteweb.png'

		});
		botoForward.addEventListener('click', function(e) {
			viewWeb.goForward();
		});
		barraWebView.add(botoForward);

		var botoRefresh = Ti.UI.createView({
			width : 44 * formulaWidth,
			height : isAndroid ? 44 * formulaHeight : 44,
			left : 146 * formulaWidth,
			top : isAndroid ? 0 * formulaHeight : 0,
			backgroundImage : '/images/controlsWeb/actualizar.png'

		});
		botoRefresh.addEventListener('click', function(e) {
			viewWeb.reload();
		});
		barraWebView.add(botoRefresh);

		var botoShare = Ti.UI.createView({
			width : 44 * formulaWidth,
			height : isAndroid ? 44 * formulaHeight : 44,
			left : 209 * formulaWidth,
			bottom : isAndroid ? 0 * formulaHeight : 0,
			backgroundImage : '/images/controlsWeb/compartir.png'

		});

		botoShare.addEventListener('click', function(e) {

		});
		//barraWebView.add(botoShare);

		var webView = Ti.UI.createWebView({
			enableZoomControls : true,
			visible : true,
			
			left : 0 * formulaWidth,
			height : isAndroid ? 480 * formulaHeight - statusBarHeight - 44 * formulaHeight - 44 * formulaHeight - 44 * formulaHeight : 480 * formulaHeight - 44 - 44 - 44,
			top : isAndroid ? 44 * formulaHeight * formulaHeight : 44,
			url : dataPista.url_resposta,
			width : 270 * formulaWidth,
			showScrollbars : false
		});
		viewWeb.add(webView);

		isAndroid ? webView.reload() : webView.repaint();
		numTabsMeta++;
	}

if (numTabsMeta == 1) {
	
		scrollViewDescripcio.height = isAndroid ? 480 * formulaHeight - statusBarHeight - 44 * formulaHeight - 220 * formulaHeight : 480 * formulaHeight - 220-44;
		scrollViewDescripcio.bottom= isAndroid ? 0 * formulaHeight : 44;
	}
	
	if (numTabsMeta != 1) {
		var viewtabs = require('/controller/objectes/mod_tabs');
		var viewTabsInstance = new viewtabs('/images/tabs/infosi.png', scrollViewDescripcio,null,null, '/images/tabs/audiosi.png', viewAudio, '/images/tabs/videosi.png', viewVideo, '/images/tabs/linksi.png', viewWeb,numTabsMeta,'jocpistes');

		fitxaMetaView.add(viewTabsInstance);
	}
}
	return fitxaMetaView;

}

module.exports = createfitxaMeta;
