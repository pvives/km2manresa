// var xhr = Titanium.Network.createHTTPClient();
//
// xhr.onload = function() {
//
// if (this.responseText > Ti.App.version) {
// var alertDialog = Titanium.UI.createAlertDialog({
// title : actualitzar_app,
// buttonNames : ['Ok']
// });
// alertDialog.show();
//
// alertDialog.addEventListener("click", function(e) {
// if (e.index == 0) {
// Titanium.Platform.openURL('https://itunes.apple.com/es/app/eduloc/id473930810');
//
// }
// });
// }
// };
//
// xhr.open('GET', 'http://eduloc.net/api/version');
//
// xhr.onerror = function(e) {
//
// };
// xhr.send();

// function inici() {



function isiOS7Plus() {
	// iOS-specific test
	if (Titanium.Platform.name == 'iPhone OS') {
		var version = Titanium.Platform.version.split(".");
		var major = parseInt(version[0], 10);

		// Can only test this support on a 3.2+ device
		if (major >= 7) {
			return true;
		}
	}
	return false;
}

var iOS7 = isiOS7Plus();

var llenguaGuardada = Titanium.App.Properties.getString("languageGuardada", "undefined");
if (llenguaGuardada == 'ca') {
	Titanium.App.Properties.setString("language", 'ca');
} else if (llenguaGuardada == 'es') {
	Titanium.App.Properties.setString("language", 'es');
} else if (llenguaGuardada == 'en') {
	Titanium.App.Properties.setString("language", 'en');
} else if (llenguaGuardada == 'pt') {
	Titanium.App.Properties.setString("language", 'pt');
} else if (llenguaGuardada == 'eus') {
	Titanium.App.Properties.setString("language", 'eus');
} else {
	var llengua = Titanium.Locale.getCurrentLanguage();
	Titanium.App.Properties.setString("language", llengua);

}

// var urlBase = 'http://educacr.itinerarium.es';

 var urlBase = 'http://tramsloc.itinerarium.cat';
 Ti.App.urlBase='http://tramsloc.itinerarium.cat';
// Ti.App.entorn='trans';
// var urlBase = 'http://www.eduloc.net';
Ti.App.entorn='trans';

if (Titanium.Platform.osname === 'android') {
	var isAndroid = true;
}
if(isAndroid){
	Ti.App.maxAnotacions=60;
	Ti.App.numAnotacions=60;
}
else{
	Ti.App.maxAnotacions=150;
	Ti.App.numAnotacions=150;	
}

var widthpantalla = Ti.Platform.displayCaps.platformWidth;
var heightpantalla = Ti.Platform.displayCaps.platformHeight;
var formulaWidth = widthpantalla / 320;
var formulaHeight = heightpantalla / 480;
var statusBarHeight = Ti.Platform.displayCaps.logicalDensityFactor * 25;
var loggedIn = Titanium.App.Properties.getString("loggedIn", "false");

Ti.App.colorPrincipal = '#2eb7bc';
if(Ti.App.entorn=='eduloc'){
if (Ti.Platform.osname == 'android') {
	// on Android, use the "base name" of the file (name without extension)
	customFont = 'AnnaBeta-Regular';
}
var customFont = 'Anna Beta';	
}

Ti.App.paddingLabels = 0;

if(isAndroid){
	

if (heightpantalla < 470 * Ti.Platform.displayCaps.logicalDensityFactor) {
	Ti.App.paddingLabels = 0;
	
	
} else if (heightpantalla >= 470 * Ti.Platform.displayCaps.logicalDensityFactor && heightpantalla < 640 * Ti.Platform.displayCaps.logicalDensityFactor) {
	Ti.App.paddingLabels = 0;
	
} else if (heightpantalla >= 640 * Ti.Platform.displayCaps.logicalDensityFactor && heightpantalla < 960 * Ti.Platform.displayCaps.logicalDensityFactor) {
	
	Ti.App.paddingLabels = 3;
} else if (heightpantalla >= 960 * Ti.Platform.displayCaps.logicalDensityFactor) {
	
	Ti.App.paddingLabels = 8;
}
}
if (Titanium.App.Properties.getString("language", "undefined") == 'ca') {
	Titanium.include('/controller/translations.js');
} else if (Titanium.App.Properties.getString("language", "undefined") == 'es') {
	Titanium.include('/controller/translations_es.js');
} else if (Titanium.App.Properties.getString("language", "undefined") == 'en') {
	Titanium.include('/controller/translations_en.js');
} else if (Titanium.App.Properties.getString("language", "undefined") == 'pt') {
	Titanium.include('/controller/translations_pt.js');
} else if (Titanium.App.Properties.getString("language", "undefined") == 'pt-PT') {
	Titanium.include('/controller/translations_pt.js');
} else if (Titanium.App.Properties.getString("language", "undefined") == 'eus') {
	Titanium.include('/controller/translations_eus.js');
} else {
	Titanium.include('/controller/translations_en.js');
}
var firstTime = true;
Ti.App.ordenacio = 'valoracio_mitjana';
Ti.App.paraula_clau = '';
Ti.App.nom_escenari = '';
Ti.App.poblacio = '';
Ti.App.idioma = '';
Ti.App.area_coneixement = 0;
Ti.App.public_objectiu = 0;
Ti.App.edat_autor = 0;
Ti.App.nom_centre = '';
Ti.App.autor = '';
Ti.App.nomescenari = '';
Ti.App.vincDestacats = true;
Ti.App.vincMeus = false;
Ti.App.vincDescarregats = false;
var filtreObert = false;
var viewOkCancel;
var taParaulaClau;
var taAutor;
var taPoblacio;
var taIdioma;
var taNomEscola;
var tableViewBibliotecaInstance = null;
var tableViewMeusInstance = null;
var tableViewDestacatsInstance = null;
var tableViewOfflineInstance = null;
var filtreInstance = null;
var viewFiltreInstance = null;
var fitxaLoginInstance = null;
var NappDrawerModule = require('dk.napp.drawer');

if (!isAndroid) {
	var winCentral = Ti.UI.createWindow({
		backgroundColor :'#353535',
		statusBarStyle : Titanium.UI.iPhone.StatusBar.LIGHT_CONTENT,
		top : 0
	});
} else {
	var winCentral = Ti.UI.createView({
		backgroundColor :'#353535',
		fullscreen : false,
		navBarHidden : true
	});
}

if (!isAndroid) {
	var win = Ti.UI.createWindow({
		backgroundColor : '#353535',
		statusBarStyle : Titanium.UI.iPhone.StatusBar.LIGHT_CONTENT,
		top : 0
	});
} else {
	var win = Ti.UI.createView({
		backgroundColor : Ti.App.colorPrincipal,
		fullscreen : false,
		navBarHidden : true

	});
}

var leftMenuView = Ti.UI.createView({
	backgroundColor : 'white',
	width : Ti.UI.FILL,
	height : Ti.UI.FILL
});

var centerView = Ti.UI.createView({
	backgroundColor : 'white',
	width : Ti.UI.FILL,
	height : Ti.UI.FILL
});

var fonsActInd = Titanium.UI.createView({
	backgroundColor : 'black',
	top : isAndroid ? 44 * formulaHeight : 44,
	zIndex : 2,
	visible : false,
	opacity : 0.7,
	height : isAndroid ? 480 * formulaHeight - 44 * formulaHeight : 480 * formulaHeight - 44,
	width : 320 * formulaWidth,
	left : 0 * formulaWidth
});
winCentral.add(fonsActInd);

var actInd = Ti.UI.createActivityIndicator({
	color : 'white',
	font : {
		fontFamily : 'Helvetica Neue',
		fontSize : isAndroid ? '20sp' : '24sp',
		fontWeight : 'bold'
	},
	visible : false,
	message : descarregant,
	zIndex : 10,
	style : isAndroid?Ti.UI.iPhone.ActivityIndicatorStyle.BIG:'none',
	top : 150 * formulaHeight,
	textAlign : 'center',
	left : isAndroid ? 0 * formulaWidth : 0,
	// height:isAndroid ? 480 * formulaHeight - 44 * formulaHeight : 480 * formulaHeight - 44,
	width : 320 * formulaWidth
});

fonsActInd.add(actInd);


var barraSuperior = Titanium.UI.createView({
	// backgroundImage : '/images/topbar.png',
	backgroundColor : '#353535',
	top : 0 * formulaHeight,
	zIndex : 4,
	height : isAndroid ? 44 * formulaHeight : 44,
	width : 320 * formulaWidth,
	left : 0 * formulaWidth
});
if (iOS7) {
	barraSuperior.top = 20;

	var fonsStatusBar = Titanium.UI.createView({
		// backgroundImage : '/images/topbar.png',
		backgroundColor : '#353535',
		top : 0 * formulaHeight,
		zIndex : 4,
		height : isAndroid ? 20 * formulaHeight : 20,
		width : 320 * formulaWidth,
		left : 0 * formulaWidth
	});
	winCentral.add(fonsStatusBar);
}
var labelBarraSuperior = Ti.UI.createLabel({
	text : biblioteca,
	left : 0 * formulaWidth,

	textAlign : 'center',
	height : isAndroid ? 44 * formulaHeight : 44,
	width : 320 * formulaWidth,
	color :Ti.App.colorPrincipal,
	top : 0 * formulaHeight,
	font : {
		fontSize : 20 + Ti.App.paddingLabels + 'sp',
		 fontFamily : Ti.App.entorn=='eduloc'?customFont:'Helvetica'
	}
});
barraSuperior.add(labelBarraSuperior);

winCentral.add(barraSuperior);

var labelEscenaris = Ti.UI.createLabel({

	right : 0 * formulaWidth,
	textAlign : 'center',
	height : Ti.UI.SIZE,
	width : 320 * formulaWidth,
	color : '#cbcbca',
	bottom : 1 * formulaHeight,
	font : {
		fontWeight : 'bold',
		fontSize : 12 + Ti.App.paddingLabels + 'sp'
	},
});

if(isAndroid){
var filterButton = Ti.UI.createView({
	width : 44 * formulaWidth,
	height : isAndroid ? 44 * formulaHeight : 44,
	right : 0 * formulaWidth,
	zIndex : 5,
	visible : true,
	top : 0 * formulaHeight,
	backgroundImage : '/images/filter.png'

});	
}
else{
	var filterButton = Ti.UI.createButton({
	width : 44 * formulaWidth,
	height : isAndroid ? 44 * formulaHeight : 44,
	right : 0 * formulaWidth,
	zIndex : 5,
	visible : true,
	top : 0 * formulaHeight,
	backgroundImage : '/images/filter.png'

});	
}
 barraSuperior.add(filterButton);

var labelON = Ti.UI.createLabel({
text:'ON',
visible:false,
	right : 0 * formulaWidth,
	textAlign : 'center',
	verticalAlign : Ti.UI.TEXT_VERTICAL_ALIGNMENT_BOTTOM,
	height : 16* formulaHeight,
	width : 44 * formulaWidth,
	color : Ti.App.colorPrincipal,
	bottom : 0 * formulaHeight,
	font : {
		fontWeight : 'bold',
		fontSize : 9 + Ti.App.paddingLabels + 'sp'
	},
});
 filterButton.add(labelON);

if (!isAndroid) {
	var filtre = require('controller/llistesEscenaris/Mod_Filtre');
	filtreInstance = new filtre(urlBase, winCentral);
	winCentral.add(filtreInstance);
}

filterButton.addEventListener("click", function(e) {
	if (filtreObert) {
		if (!isAndroid) {
			filtreInstance.animate({
				top : isAndroid ? -480 * formulaHeight - 44 * formulaHeight : -480 * formulaHeight - 44,
				duration : 500
			});
		} else {
			filtreInstance.top = isAndroid ? -480 * formulaHeight - 44 * formulaHeight : -480 * formulaHeight - 44;
		}

		filtreObert = false;
		
	} else {
		if (firstTime) {
			if (isAndroid) {
				var filtre = require('controller/llistesEscenaris/Mod_Filtre');
				filtreInstance = new filtre(urlBase, winCentral);
				winCentral.add(filtreInstance);
			}

			firstTime = false;
		}

		if (!isAndroid) {
			filtreInstance.animate({
				top : isAndroid ? 44 * formulaHeight+staturBarHeight : 44 + 20,
				duration : 500
			});
		} else {
			filtreInstance.top = 30 * formulaHeight;
		}

		
		filtreObert = true;
		
	}

});


var menuButton = Ti.UI.createButton({
	width : 44 * formulaWidth,
	height : isAndroid ? 44 * formulaHeight : 44,

	left : 0 * formulaWidth,
	top : 0 * formulaHeight,
	backgroundImage : '/images/listas.png'

});
barraSuperior.add(menuButton);

menuButton.addEventListener("click", function(e) {
	drawer.toggleLeftWindow();

});

var botoInfoDescarregats = Ti.UI.createButton({
	width : 44 * formulaWidth,
	height : isAndroid ? 44 * formulaHeight : 44,
	right : 0 * formulaWidth,
	zIndex : 5,
	visible : false,
	top : 0 * formulaHeight,
	backgroundImage : '/images/help.png'

});
barraSuperior.add(botoInfoDescarregats);

botoInfoDescarregats.addEventListener("click", function(e) {
	var infoDescarregats = require('controller/llistesEscenaris/instruccions');
	var infoDescarregatsInstance = new infoDescarregats(winCentral);
	winCentral.add(infoDescarregatsInstance);

});

//canvi titol barra superior
function canviaTitol(titol) {

	labelBarraSuperior.text = titol;
};

//carrego primera biblioteca
createLlistatBiblioteca(winCentral);

function createLlistatBiblioteca(winCentral) {
	fonsActInd.show();
	actInd.show();

	var data = require('controller/llistesEscenaris/llistats_network_Controller');
if (Ti.App.entorn == 'eduloc') {
				new data(urlBase + '/mobile/scenario/library', callbackLlistat, actInd);

			} else {
				
				 new data(urlBase + '/api/scenario/library/'+Ti.App.entorn+'/es', callbackLlistat, actInd);

			}

	function callbackLlistat(data) {
		Ti.App.vincDestacats = false;
		Ti.App.vincMeus = false;
		Ti.App.vincDescarregats = false;

		var tableViewBiblioteca = require('controller/llistesEscenaris/Mod_llistats');
		tableViewBibliotecaInstance = new tableViewBiblioteca(urlBase, data);

		fonsActInd.visible = false;
		actInd.hide();

		winCentral.add(tableViewBibliotecaInstance);

	}

}

var llistatDrawer = require('controller/llistesEscenaris/llistatDrawer');
var llistatDrawerInstance = new llistatDrawer(urlBase, win);

if (!isAndroid) {
	var drawer = NappDrawerModule.createDrawer({
		leftWindow : llistatDrawerInstance,
		centerWindow : winCentral,
		showShadow : true,
		backgroundColor :'#353535',
		closeDrawerGestureMode : isAndroid ? NappDrawerModule.MODE_MARGIN : NappDrawerModule.CLOSE_MODE_ALL,
		openDrawerGestureMode : NappDrawerModule.OPEN_MODE_ALL,
		leftDrawerWidth : 276 * formulaWidth

	});
} else {
	var drawer = NappDrawerModule.createDrawer({
backgroundColor :'#353535',
		navBarHidden : true,
		leftWindow : llistatDrawerInstance,
		centerWindow : winCentral,
		// rightWindow: rightMenuView,
		fading : 0.2, // 0-1
		parallaxAmount : 0.2, //0-1
		// shadowWidth : "40dp",
		showShadow : false,
		leftDrawerWidth : "200dp",
		rightDrawerWidth : 276 * formulaWidth,
		animationMode : NappDrawerModule.ANIMATION_NONE,
		closeDrawerGestureMode : NappDrawerModule.MODE_MARGIN,
		openDrawerGestureMode : NappDrawerModule.MODE_ALL
	});
}

if (!isAndroid) {
	drawer.setAnimationMode(NappDrawerModule.ANIMATION_SLIDE_SCALE);
}

drawer.open();

if (!isAndroid) {
	var tmpWin = Ti.UI.createWindow({
		backgroundColor : 'transparent',
		zIndex : 1000,
		touchEnabled : false,

		statusBarStyle : Titanium.UI.iPhone.StatusBar.LIGHT_CONTENT
	});

	tmpWin.open();
}

if (!isAndroid) {
	win.open();
}
// };
