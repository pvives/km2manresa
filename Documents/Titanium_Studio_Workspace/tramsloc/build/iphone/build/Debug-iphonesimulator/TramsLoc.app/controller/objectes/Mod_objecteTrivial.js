if (Titanium.App.Properties.getString("language", "undefined") == 'ca') {
	Titanium.include('/controller/translations.js');
} else if (Titanium.App.Properties.getString("language", "undefined") == 'es') {
	Titanium.include('/controller/translations_es.js');
} else if (Titanium.App.Properties.getString("language", "undefined") == 'en') {
	Titanium.include('/controller/translations_en.js');
} else if (Titanium.App.Properties.getString("language", "undefined") == 'pt') {
	Titanium.include('/controller/translations_pt.js');
} else if (Titanium.App.Properties.getString("language", "undefined") == 'pt-PT') {
	Titanium.include('/controller/translations_pt.js');
} 
else if (Titanium.App.Properties.getString("language", "undefined") == 'eus') {
		Titanium.include('/controller/translations_eus.js');
	} else {
	Titanium.include('/controller/translations_en.js');
}
if (Titanium.Platform.osname === 'android') {
	var isAndroid = true;
}

var widthpantalla = Ti.Platform.displayCaps.platformWidth;
var heightpantalla = Ti.Platform.displayCaps.platformHeight;
var formulaWidth = widthpantalla / 320;
var formulaHeight = heightpantalla / 480;
var arrayInstantanies = [];
var statusBarHeight = isAndroid ? Ti.Platform.displayCaps.logicalDensityFactor * 25 : 20;

function createFitxaTrivial() {

	var scrollableViewTrivial;

	var fitxaTrivial = Ti.UI.createView({
		width : 270 * formulaWidth,
		visible : false,
		top : 0,
		borderRadius:10,
		height : isAndroid ? 480 * formulaHeight - statusBarHeight - 20 * formulaHeight: 480 * formulaHeight - statusBarHeight -20,
		backgroundColor : '#4d4d4d'
	});

	//creo view Maquina
	var barraSuperior = Ti.UI.createView({
		width : 270 * formulaWidth,
		height : isAndroid ? 44 * formulaHeight : 44,
		top : isAndroid ? 0 * formulaHeight : 0,
		backgroundColor : '#55c3ec',
		right : 0 * formulaWidth
	});
	fitxaTrivial.add(barraSuperior);

	var labelTitolTrivial = Ti.UI.createLabel({
		//text : dataPunt.nom,
		top : isAndroid ? 0 * formulaHeight : 0,
		left : 44 * formulaWidth,
		color : 'black',
		height : isAndroid ? 44 * formulaHeight : 44,
		textAlign : 'center',
		width : 270 * formulaWidth-44 * formulaHeight-44 * formulaHeight,
		font : {
			fontSize :  17+Ti.App.paddingLabels+'sp' 
		}
	});
	barraSuperior.add(labelTitolTrivial);

	function refreshTrivial(dataTrivial, urlBase) {

		var arrayPreguntes = [];
		var arrayPreguntesResoltes = [];

		if (scrollableViewTrivial != null) {
			fitxaTrivial.remove(scrollableViewTrivial);
		}

		labelTitolTrivial.text = dataTrivial.nom;

		var scrollableViewTrivial = Ti.UI.createScrollableView({
			top : isAndroid ? 44 * formulaHeight : 44,
			height : isAndroid ? 480 * formulaHeight - statusBarHeight - 20 * formulaHeight - 44 * formulaHeight : 480 * formulaHeight - statusBarHeight - 20 - 44,
			left : 0 * formulaWidth,
			width : 270 * formulaWidth,
			views : [],
			scrollingEnabled:false,
			showPagingControl:false,
			visible : true,
			
			maxZoomScale : 2.0
		});

		fitxaTrivial.add(scrollableViewTrivial);

		var numPreguntaView = Ti.UI.createView({
			width : 270 * formulaWidth,
			height : isAndroid ? 44 * formulaHeight : 44,
			bottom : isAndroid ? 0 * formulaHeight : 0,
			backgroundColor : 'black',
			left : 0 * formulaWidth
		});
		fitxaTrivial.add(numPreguntaView);

		var labelNumPregunta = Ti.UI.createLabel({
			text : scrollableViewTrivial.currentPage + 1 + '/' + dataTrivial.trivialstexte.length,
			top : isAndroid ? 0 * formulaHeight : 0,
			left : 0 * formulaWidth,
			color : 'white',
			height : isAndroid ? 44 * formulaHeight : 44,
			textAlign : 'center',
			width : 270 * formulaWidth,
			font : {
				fontSize :  17+Ti.App.paddingLabels+'sp' 
			}
		});
		numPreguntaView.add(labelNumPregunta);

		var fletxaEsquerraPregunta = Ti.UI.createButton({
			width : 44 * formulaWidth,
			height : isAndroid ? 44 * formulaHeight : 44,
			left : 0 * formulaWidth,
			top : isAndroid ? 0 * formulaHeight : 0,
			backgroundImage : '/images/controlsWeb/anteriorweb.png'

		});
		numPreguntaView.add(fletxaEsquerraPregunta);

		scrollableViewTrivial.addEventListener('scrollEnd', function(e) {
			labelNumPregunta.text = scrollableViewTrivial.currentPage + 1 + '/' + dataTrivial.trivialstexte.length;
		});

		fletxaEsquerraPregunta.addEventListener('click', function(e) {
			if (scrollableViewTrivial.currentPage > 0) {
				scrollableViewTrivial.scrollToView(scrollableViewTrivial.currentPage - 1);
				labelNumPregunta.text = scrollableViewTrivial.currentPage + 1 + '/' + dataTrivial.trivialstexte.length;
			}
		});

		var fletxaDretaPregunta = Ti.UI.createButton({
			width : 44 * formulaWidth,
			height : isAndroid ? 44 * formulaHeight : 44,
			right : 0 * formulaWidth,
			top : isAndroid ? 0 * formulaHeight : 0,
			backgroundImage : '/images/controlsWeb/siguienteweb.png'

		});
		numPreguntaView.add(fletxaDretaPregunta);

		fletxaDretaPregunta.addEventListener('click', function(e) {
			if (scrollableViewTrivial.currentPage < dataTrivial.trivialstexte.length - 1) {

				scrollableViewTrivial.scrollToView(scrollableViewTrivial.currentPage + 1);
				sumaLabel();
			}

		});

		function sumaLabel() {

			labelNumPregunta.text = scrollableViewTrivial.currentPage + 1 + '/' + dataTrivial.trivialstexte.length;
		}

		for (var i = 0; i < dataTrivial.trivialstexte.length; i++) {
			var fitxaPregunta = require('/controller/fitxesObjectes/fitxaPregunta');
			var fitxaPreguntaInstance = new fitxaPregunta(dataTrivial, i, arrayPreguntesResoltes, scrollableViewTrivial, sumaLabel);

			arrayPreguntes[i] = fitxaPreguntaInstance;
		}
		scrollableViewTrivial.views = arrayPreguntes;

	}


	fitxaTrivial.refreshTrivial = refreshTrivial;
	return fitxaTrivial;

}

module.exports = createFitxaTrivial;
