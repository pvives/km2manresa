if (Titanium.App.Properties.getString("language", "undefined") == 'ca') {
	Titanium.include('/controller/translations.js');
} else if (Titanium.App.Properties.getString("language", "undefined") == 'es') {
	Titanium.include('/controller/translations_es.js');
} else if (Titanium.App.Properties.getString("language", "undefined") == 'en') {
	Titanium.include('/controller/translations_en.js');
} else if (Titanium.App.Properties.getString("language", "undefined") == 'pt') {
	Titanium.include('/controller/translations_pt.js');
} else if (Titanium.App.Properties.getString("language", "undefined") == 'pt-PT') {
	Titanium.include('/controller/translations_pt.js');
} else if (Titanium.App.Properties.getString("language", "undefined") == 'eus') {
	Titanium.include('/controller/translations_eus.js');
} else {
	Titanium.include('/controller/translations_en.js');
}
if (Titanium.Platform.osname === 'android') {
	var isAndroid = true;
}

var widthpantalla = Ti.Platform.displayCaps.platformWidth;
var heightpantalla = Ti.Platform.displayCaps.platformHeight;
var formulaWidth = widthpantalla / 320;
var formulaHeight = heightpantalla / 480;
var statusBarHeight = isAndroid ? Ti.Platform.displayCaps.logicalDensityFactor * 25 : 20;

function createFitxaMaquina() {

	var scrollableViewMaquina = null;


	var fitxaMaquina = Ti.UI.createView({
		width : 270 * formulaWidth,
		visible : false,
		top : 0,
		borderRadius : 10,
		height : isAndroid ? 480 * formulaHeight - statusBarHeight - 20 * formulaHeight : 480 * formulaHeight - statusBarHeight - 20,
		backgroundColor : '#4d4d4d'
	});

	//creo view Maquina
	var barraSuperior = Ti.UI.createView({
		width : 270 * formulaWidth,
		height : isAndroid ? 44 * formulaHeight : 44,
		top : isAndroid ? 0 * formulaHeight : 0,
		backgroundColor : '#55c3ec',
		right : 0 * formulaWidth
	});
	fitxaMaquina.add(barraSuperior);

	var labelTitolMaquina = Ti.UI.createLabel({
		//text : dataPunt.nom,
		top : isAndroid ? 0 * formulaHeight : 0,
		left : 44 * formulaWidth,
		color : 'black',
		height : isAndroid ? 44 * formulaHeight : 44,
		textAlign : 'center',
		width : 270 * formulaWidth - 44 * formulaHeight - 44 * formulaHeight,
		font : {
			fontSize : 17 + Ti.App.paddingLabels + 'sp'
		}
	});
	barraSuperior.add(labelTitolMaquina);

	function refreshMaquina(dataMaquina, urlBase) {
		var numTabs = 0;
		var fitxaInfoInstance;
		var fitxaAudioInstance;
		var fitxaVideoInstance;
		var fitxaWebInstance;
		var arrayInstantanies = [];
		if (scrollableViewMaquina != null) {
			fitxaMaquina.remove(scrollableViewMaquina);
		}

		labelTitolMaquina.text = dataMaquina.nom;

		scrollableViewMaquina = Ti.UI.createScrollableView({
			top : isAndroid ? 44 * formulaHeight : 44,
			height : isAndroid ? 480 * formulaHeight - statusBarHeight - 44 : 480 * formulaHeight - 44 - 20 - 20,
			left : 0 * formulaWidth,
			width : 270 * formulaWidth,
			views : [],
			visible : true,
			showPagingControl : isAndroid ? false : true,
			maxZoomScale : 2.0
		});

		fitxaMaquina.add(scrollableViewMaquina);

		for (var i = 0; i < dataMaquina.instantanies.length; i++) {
			var numTabs = 0;
			var instantaniaView = Ti.UI.createView({
				width : 270 * formulaWidth,
				top : isAndroid ? 0 * formulaHeight : 0,
				left : 0 * formulaWidth,
				height : isAndroid ? 480 * formulaHeight - statusBarHeight - 44 * formulaHeight : 480 * formulaHeight - 44 - 20 - 20 - 20
			});

			var dataInstantaniaView = Ti.UI.createView({
				width : 270 * formulaWidth,
				height : isAndroid ? 44 * formulaHeight : 44,
				top : isAndroid ? 0 * formulaHeight : 0,
				backgroundColor : '#353535',
				left : 0 * formulaWidth
			});
			instantaniaView.add(dataInstantaniaView);

			var fletxaEsquerraMaquina = Ti.UI.createButton({
				width : 44 * formulaWidth,
				height : isAndroid ? 44 * formulaHeight : 44,
				left : 0 * formulaWidth,
				zIndex : 10,
				top : isAndroid ? 0 : 0,
				backgroundImage : '/images/controlsWeb/anteriorweb.png'

			});
			dataInstantaniaView.add(fletxaEsquerraMaquina);

			fletxaEsquerraMaquina.addEventListener('click', function(e) {
				if (scrollableViewMaquina.currentPage > 0) {
					scrollableViewMaquina.scrollToView(scrollableViewMaquina.currentPage - 1);
				}
			});

			var fletxaDretaMaquina = Ti.UI.createButton({
				width : 44 * formulaWidth,
				height : isAndroid ? 44 * formulaHeight : 44,
				right : 0 * formulaWidth,
				zIndex : 10,
				top : isAndroid ? 0 : 0,
				backgroundImage : '/images/controlsWeb/siguienteweb.png'

			});
			dataInstantaniaView.add(fletxaDretaMaquina);

			fletxaDretaMaquina.addEventListener('click', function(e) {
				if (scrollableViewMaquina.currentPage < dataMaquina.instantanies.length - 1) {
					scrollableViewMaquina.scrollToView(scrollableViewMaquina.currentPage + 1);
				}

			});

			var labelData = Ti.UI.createLabel({
				text : dataMaquina.instantanies[i].any,
				top : isAndroid ? 0 * formulaHeight : 0,
				left : 0 * formulaWidth,
				color : 'white',
				height : isAndroid ? 44 * formulaHeight : 44,
				textAlign : 'center',
				width : 270 * formulaWidth,
				font : {
					fontSize : 17 + Ti.App.paddingLabels + 'sp'
				}
			});
			dataInstantaniaView.add(labelData);

			if (dataMaquina.instantanies[i].galeria.imatges != '' && dataMaquina.instantanies[i].galeria.imatges != null) {

				var fotoInstantania = Ti.UI.createImageView({
					backgroundColor : 'black',
					width : 270 * formulaWidth,
					height : isAndroid ? 220 * formulaHeight - 44 : 220 - 44,
					top : isAndroid ? 44 * formulaHeight : 44,

					image : Ti.App.escenariDescarregat ? dataMaquina.instantanies[i].galeria.imatges[0].pathImatge : urlBase + dataMaquina.instantanies[i].galeria.imatges[0].thumb176x270
				});
				instantaniaView.add(fotoInstantania);

			} else {

				var fotoInstantania = Ti.UI.createImageView({
					width : 270 * formulaWidth,
					height : isAndroid ? 220 * formulaHeight - 44 * formulaHeight : 220 - 44,
					// top : isAndroid ? 44 * formulaHeight : 44,
					image : '/images/defectotiempo.png'
				});
				instantaniaView.add(fotoInstantania);
			}

			if (dataMaquina.instantanies[i].galeria.pathaudio != null) {

				//Creo fitxa audio
				var fitxaAudio = require('/controller/fitxesObjectes/fitxaAudio');
				fitxaAudioInstance = new fitxaAudio(dataMaquina.instantanies[i], urlBase, 'maquina');
				instantaniaView.add(fitxaAudioInstance);
				numTabs++;
			}

			if (dataMaquina.instantanies[i].galeria.pathvideo != '' && dataMaquina.instantanies[i].galeria.pathvideo != null) {
				//Creo fitxa video
				var fitxaVideo = require('/controller/fitxesObjectes/fitxaVideo');
				fitxaVideoInstance = new fitxaVideo(dataMaquina.instantanies[i], urlBase);
				instantaniaView.add(fitxaVideoInstance);
				numTabs++;
			}

			if (dataMaquina.instantanies[i].galeria.urlGenerica != null) {
				//Creo fitxa web
				var fitxaWeb = require('/controller/fitxesObjectes/fitxaWeb');
				fitxaWebInstance = new fitxaWeb(dataMaquina.instantanies[i], urlBase);
				instantaniaView.add(fitxaWebInstance);
				numTabs++;
			}
			//creo fitxa info
			numTabs++;
			var fitxaInfo = require('/controller/fitxesObjectes/fitxaInfoMaquina');
			fitxaInfoInstance = new fitxaInfo(dataMaquina.instantanies[i], urlBase, 'maquina', numTabs);
			instantaniaView.add(fitxaInfoInstance);

			if (numTabs != 1) {
				var viewtabs = require('/controller/objectes/mod_tabs');
				var viewTabsInstance = new viewtabs('/images/tabs/infosi.png', fitxaInfoInstance, '/images/tabs/audiosi.png', fitxaAudioInstance, '/images/tabs/videosi.png', fitxaVideoInstance, '/images/tabs/linksi.png', fitxaWebInstance, null, null, numTabs, 'maquina');

				instantaniaView.add(viewTabsInstance);
			}

			arrayInstantanies[i] = instantaniaView;
		}

		scrollableViewMaquina.views = arrayInstantanies;
	}


	fitxaMaquina.refreshMaquina = refreshMaquina;

	return fitxaMaquina;

}

module.exports = createFitxaMaquina;
