function createFitxaAudio(dataPunt, urlBase, tipusObjecte) {

	//Crea les fitxes de tots els objectes
	var widthpantalla = Ti.Platform.displayCaps.platformWidth;
	var heightpantalla = Ti.Platform.displayCaps.platformHeight;
	var formulaWidth = widthpantalla / 320;
	var formulaHeight = heightpantalla / 480;

	if (Titanium.Platform.osname === 'android') {
		var isAndroid = true;
	}
	var statusBarHeight = Ti.Platform.displayCaps.logicalDensityFactor * 25;

	var fitxaAudio = Ti.UI.createView({
		width : 270 * formulaWidth,
		left : 0 * formulaWidth,
		height : isAndroid ? 480 * formulaHeight - statusBarHeight - 44 * formulaHeight - 44 * formulaHeight - 220 * formulaHeight : 480 * formulaHeight - 44 - 44 - 220,
		top : isAndroid ? 220 * formulaHeight : 220,
		visible : false

	});
	if (tipusObjecte == 'punt') {
		fitxaAudio.top = isAndroid ? 220 * formulaHeight + 44 + 20 * formulaHeight : 220 + 44;
	}
	var viewAudio = Ti.UI.createView({
		width : 270 * formulaWidth,
		height : isAndroid ? 480 * formulaHeight - statusBarHeight - 44 * formulaHeight - 44 * formulaHeight - 220 * formulaHeight : 480 * formulaHeight - 44 - 220 - 44,
		top : isAndroid ? 0 * formulaHeight : 0,
		backgroundColor : '#4d4d4d',
		left : 0 * formulaWidth

	});
	fitxaAudio.add(viewAudio);

	var botoAudio = Ti.UI.createView({
		width : 80 * formulaWidth,
		height : isAndroid ? 44 * formulaHeight : 44,
		left : 95 * formulaWidth,
		bottom : isAndroid ? 88 * formulaHeight : 88,
		backgroundImage : '/images/playaudio.png'

	});
	viewAudio.add(botoAudio);

	var urlAudio = Ti.App.escenariDescarregat ? dataPunt.galeria.pathaudio : urlBase + '/' + dataPunt.galeria.pathaudio;

	var audioController = require('/controller/fitxesObjectes/audioController');
	audioControllerInstance = new audioController(urlAudio, botoAudio);

	botoAudio.addEventListener('click', function(e) {
		audioControllerInstance.click();
	});

	return fitxaAudio;

}

module.exports = createFitxaAudio;
