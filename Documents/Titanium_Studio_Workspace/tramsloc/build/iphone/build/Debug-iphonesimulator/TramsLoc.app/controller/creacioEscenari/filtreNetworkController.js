function createNetworkController(url, callback) {
	if (Titanium.App.Properties.getString("language", "undefined") == 'ca') {
		Titanium.include('/controller/translations.js');
	} else if (Titanium.App.Properties.getString("language", "undefined") == 'es') {
		Titanium.include('/controller/translations_es.js');
	} else if (Titanium.App.Properties.getString("language", "undefined") == 'en') {
		Titanium.include('/controller/translations_en.js');
	} else if (Titanium.App.Properties.getString("language", "undefined") == 'pt') {
		Titanium.include('/controller/translations_pt.js');
	} else if (Titanium.App.Properties.getString("language", "undefined") == 'pt-PT') {
		Titanium.include('/controller/translations_pt.js');
	} else if (Titanium.App.Properties.getString("language", "undefined") == 'eus') {
		Titanium.include('/controller/translations_eus.js');
	} else {
		Titanium.include('/controller/translations_en.js');
	}

	var firsttime = true;
	var error = false;
	var xhr = Titanium.Network.createHTTPClient();
var arrayCapesCategories=[];
	xhr.onload = function() {

		var data = JSON.parse(this.responseText);
		

//carrego el array global de capes i categories
// 
		// for (var i = 0; i < data.length; i++) {
// 
			// var capa = {
				// nomcapa : data[i].capa_nom,
				// activada : false,
				// categories : []
			// };
// 			
			// for (var j = 0; j < data[i].categories.length; j++) {
				// var categoria = {
					// nomcategoria : data[i].categories[j].nom,
					// activada : true
				// };
				 // capa.categories.push(categoria);
// 
			// }
// 			
// 			
			// arrayCapesCategories.push(capa);
// 
		// }
	// Ti.App.ArrayCapesCategories.push(arrayCapesCategories);
		//crida a createAnnotations
		
callback(data);
	};

	xhr.open('GET', url);

	xhr.send();

	xhr.onerror = function(e) {

		//this fires if Titanium/the native SDK cannot successfully retrieve a resource
		if (firsttime == true) {
			var alertDialog = Titanium.UI.createAlertDialog({
				title : xhr_error_title,
				message : xhr_error_message,
				buttonNames : ['ok']
			});
			alertDialog.show();
			firsttime = false;
			error = true;
		}
	};
	return error;
}

module.exports = createNetworkController;
