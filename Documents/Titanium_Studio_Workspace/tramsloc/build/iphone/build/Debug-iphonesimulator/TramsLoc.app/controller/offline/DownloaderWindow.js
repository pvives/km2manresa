function createDownloaderWindow(message) {
    
    var closeWindow = function(){
        self.close();
    }
if(!isAndroid){
	    var self = Ti.UI.createWindow({
        //backgroundColor : "black",
        opacity : 0.8,
            statusBarStyle: Titanium.UI.iPhone.StatusBar.LIGHT_CONTENT,

      backgroundColor :'#353535',
      
    });
}
else{
	    var self = Ti.UI.createWindow({

        opacity : 0.8,
navBarHidden:true,
      backgroundColor :'#353535'
      
    });
}


  
   	    var actInd = Ti.UI.createActivityIndicator({
        color : 'white',
        font : {
            fontFamily : 'Helvetica',
            fontSize : isAndroid?'17sp':'17sp',
            fontWeight : 'bold'
        },
        message : message,
        style : isAndroid?1:Ti.UI.iPhone.ActivityIndicatorStyle.BIG,
        top : 100,
        left : 0,
        textAlign:'center',
        height : 320,
        width : 320
    });
   
    	self.add(actInd);
    
    
    
    actInd.show();
   

    

    return self;
}

module.exports = createDownloaderWindow;
