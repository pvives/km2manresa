function llistat(viewLlistat, creoLlista, dataEscenaris, table, actInd) {

	if (Titanium.App.Properties.getString("language", "undefined") == 'ca') {
		Titanium.include('/controller/translations.js');
	} else if (Titanium.App.Properties.getString("language", "undefined") == 'es') {
		Titanium.include('/controller/translations_es.js');
	} else if (Titanium.App.Properties.getString("language", "undefined") == 'en') {
		Titanium.include('/controller/translations_en.js');
	} else if (Titanium.App.Properties.getString("language", "undefined") == 'pt') {
		Titanium.include('/controller/translations_pt.js');
	} else if (Titanium.App.Properties.getString("language", "undefined") == 'pt-PT') {
		Titanium.include('/controller/translations_pt.js');
	} else if (Titanium.App.Properties.getString("language", "undefined") == 'eus') {
		Titanium.include('/controller/translations_eus.js');
	} else {
		Titanium.include('/controller/translations_en.js');
	}
	var widthpantalla = Ti.Platform.displayCaps.platformWidth;
	var heightpantalla = Ti.Platform.displayCaps.platformHeight;
	var formulaWidth = widthpantalla / 320;
	var formulaHeight = heightpantalla / 480;
	var primeraVegada;
	var xhr = Titanium.Network.createHTTPClient();
	var statusBarHeight = Ti.Platform.displayCaps.logicalDensityFactor * 25;

	if (Titanium.Platform.osname === 'android') {
		var isAndroid = true;
	}
	Ti.App.clicat = false;

	var viewOrdenacio = Ti.UI.createView({
		width : 320 * formulaWidth,
		backgroundColor : Ti.App.colorPrincipal,
		zIndex : 4,
		height : isAndroid ? 44 * formulaHeight : 44,
		top : isAndroid ? 0 * formulaHeight : 0,
		left : 0 * formulaWidth

	});

	if (isAndroid) {
		taParaulaClau = Titanium.UI.createTextArea({
			editable : true,
			left : 10 * formulaWidth,
			value : Ti.App.paraula_clau,
			borderRadius : 5,
			// clearOnEdit:true,
			keyboardType: Titanium.UI.KEYBOARD_ASCII,
			enableReturnKey:true ,
			verticalAlign: Ti.UI.TEXT_VERTICAL_ALIGNMENT_CENTER,
			 returnKeyType :Titanium.UI.RETURNKEY_DEFAULT,
			backgroundColor : 'white',
			height : isAndroid ? 34 * formulaHeight : 34,
			width : 204 * formulaWidth,
			top : 5 * formulaHeight,
			font : {
					fontSize : 16+Ti.App.paddingLabels+'sp',
			},
			zIndex : 11,
			color : 'black',
			textAlign : 'left',
			suppressReturn : true
		});

		var botoRefresh = Ti.UI.createView({
			width : 44 * formulaWidth,
			backgroundImage : '/images/refresh.png',
			zIndex : 12,
			height : isAndroid ? 44 * formulaHeight : 44,
			top : isAndroid ? 0 * formulaHeight : 0,
			left : 170 * formulaWidth

		});
		viewOrdenacio.add(botoRefresh);
		botoRefresh.addEventListener('click', function(e) {
			fonsActInd.show();
			actInd.show();
			Ti.App.paraula_clau = taParaulaClau.value;
			Ti.App.nom_escenari = taParaulaClau.value;

			var dataBiblioteca = require('controller/llistesEscenaris/llistats_network_Controller');
			if (Ti.App.entorn == 'eduloc') {
				new dataBiblioteca(urlBase + '/mobile/scenario/library', callbackLlistatBiblioteca, actInd);

			} else {
				new dataBiblioteca(urlBase + '/api/scenario/library/' + Ti.App.entorn + '/es', callbackLlistatBiblioteca, actInd);

			}

			function callbackLlistatBiblioteca(data) {
taParaulaClau.blur();
			
				creoLlista(data);

			}

		});
		taParaulaClau.addEventListener('return', function(e) {
			fonsActInd.show();
			actInd.show();
			Ti.App.paraula_clau = taParaulaClau.value;
			Ti.App.nom_escenari = taParaulaClau.value;

			var dataBiblioteca = require('controller/llistesEscenaris/llistats_network_Controller');
			if (Ti.App.entorn == 'eduloc') {
				new dataBiblioteca(urlBase + '/mobile/scenario/library', callbackLlistatBiblioteca, actInd);

			} else {
				new dataBiblioteca(urlBase + '/api/scenario/library/' + Ti.App.entorn + '/es', callbackLlistatBiblioteca, actInd);

			}

			function callbackLlistatBiblioteca(data) {
taParaulaClau.blur();
			
				creoLlista(data);

			}

		});

		viewOrdenacio.add(taParaulaClau);

	}
	// var search_bar = Titanium.UI.createSearchBar({
	// showCancel : false,
	// top : 0 * formulaHeight,
	// zIndex : 3,
	// // backgroundImage : '/images/searchbar.png',
	// barColor : '#ffe132',
	// backgroundColor : '#ffe132',
	// left : 0 * formulaWidth,
	// height : isAndroid ? 44 * formulaHeight : 44,
	// width : Ti.App.vincDescarregats ? 229 * formulaWidth : 229 * formulaWidth,
	// hintText : buscar_titulo
	// });
	//
	// viewOrdenacio.add(search_bar);
	//
	// // table.setSearch(search_bar);
	//
	// search_bar.addEventListener('focus', function(e) {
	// search_bar.animate({
	// width : Ti.App.vincDescarregats ? 320 * formulaWidth : 320 * formulaWidth,
	// duration : 500
	// });
	// });
	// // search_bar.addEventListener('blur', function(e) {
	// // search_bar.animate({
	// // width : Ti.App.vincDescarregats ? 320 * formulaWidth : 320 * formulaWidth,
	// // duration : 500
	// // });
	// search_bar.addEventListener('cancel', function(e) {
	// search_bar.animate({
	// width : Ti.App.vincDescarregats ? 229 * formulaWidth : 229* formulaWidth,
	// duration : 500
	// });
	// });

	if (!Ti.App.vincDescarregats) {

		var popularsButton = Ti.UI.createButton({
			height : isAndroid ? 44 * formulaHeight : 44,
			width : 40 * formulaHeight,
			backgroundImage : '/images/valorados.png',
			color : 'black',
			zIndex : 10,
			top : 0 * formulaHeight,
			right : isAndroid ? 50 * formulaWidth : 45 * formulaWidth
		});
		if (!Ti.App.vincDestacats) {
			viewOrdenacio.add(popularsButton);
		}
		if (Ti.App.ordenacio == 'valoracio_mitjana') {
			popularsButton.backgroundImage = '/images/valoradoson.png';
		}

		popularsButton.addEventListener('click', function(e) {

			fonsActInd.show();
			actInd.show();

			if (table != null && !isAndroid) {

				table.data = null;

			}
			Ti.App.ordenacio = 'valoracio_mitjana';
			popularsButton.backgroundImage = '/images/valoradoson.png';

			recentsButton.backgroundImage = '/images/recientes.png';

			if (Ti.App.vincMeus) {

				var dataMisEscenarios = require('controller/llistesEscenaris/meusNetworkController');
				var dataMisEscenariosInstance = new dataMisEscenarios(urlBase + '/api/scenario/my-list', callbackMisEscenarios, actInd);
				function callbackMisEscenarios(data) {

					creoLlista(data);

				}

			} else {
				var dataBiblioteca = require('controller/llistesEscenaris/llistats_network_Controller');
				if (Ti.App.entorn == 'eduloc') {
					new dataBiblioteca(urlBase + '/mobile/scenario/library', callbackLlistatBiblioteca, actInd);

				} else {
					new dataBiblioteca(urlBase + '/api/scenario/library/' + Ti.App.entorn + '/es', callbackLlistatBiblioteca, actInd);

				}
				function callbackLlistatBiblioteca(data) {

					creoLlista(data);

				}

			}

		});
		var recentsButton = Ti.UI.createButton({
			height : isAndroid ? 44 * formulaHeight : 44,
			width : 40 * formulaHeight,
			backgroundImage : '/images/recientes.png',
			top : 0 * formulaHeight,
			right : 5 * formulaWidth
		});

		if (Ti.App.ordenacio == 'created') {
			recentsButton.backgroundImage = '/images/recienteson.png';
		}

		recentsButton.addEventListener('click', function(e) {
			fonsActInd.show();
			actInd.show();

			if (table != null && !isAndroid) {

				table.data = null;
			}
			Ti.App.ordenacio = 'created';
			popularsButton.backgroundImage = '/images/valorados.png';

			recentsButton.backgroundImage = '/images/recienteson.png';

			if (Ti.App.vincMeus) {

				var dataMisEscenarios = require('controller/llistesEscenaris/meusNetworkController');
				var dataMisEscenariosInstance = new dataMisEscenarios(urlBase + '/api/scenario/my-list', callbackMisEscenarios, actInd);
				function callbackMisEscenarios(data) {

					creoLlista(data);
				}

			} else {
			var dataBiblioteca = require('controller/llistesEscenaris/llistats_network_Controller');
				if (Ti.App.entorn == 'eduloc') {
					new dataBiblioteca(urlBase + '/mobile/scenario/library', callbackLlistatBiblioteca, actInd);

				} else {
					new dataBiblioteca(urlBase + '/api/scenario/library/' + Ti.App.entorn + '/es', callbackLlistatBiblioteca, actInd);

				}
				function callbackLlistatBiblioteca(data) {

					creoLlista(data);

				}

			}

		});
		if (!Ti.App.vincDestacats) {
			viewOrdenacio.add(recentsButton);
		}

	}

	return viewOrdenacio;

}

module.exports = llistat;
