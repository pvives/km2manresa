if (Titanium.App.Properties.getString("language", "undefined") == 'ca') {
	Titanium.include('/controller/translations.js');
} else if (Titanium.App.Properties.getString("language", "undefined") == 'es') {
	Titanium.include('/controller/translations_es.js');
} else if (Titanium.App.Properties.getString("language", "undefined") == 'en') {
	Titanium.include('/controller/translations_en.js');
} else if (Titanium.App.Properties.getString("language", "undefined") == 'pt') {
	Titanium.include('/controller/translations_pt.js');
} else if (Titanium.App.Properties.getString("language", "undefined") == 'pt-PT') {
	Titanium.include('/controller/translations_pt.js');
}
else if (Titanium.App.Properties.getString("language", "undefined") == 'eus') {
		Titanium.include('/controller/translations_eus.js');
	}  else {
	Titanium.include('/controller/translations_en.js');
}
if (Titanium.Platform.osname === 'android') {
	var isAndroid = true;
}

var widthpantalla = Ti.Platform.displayCaps.platformWidth;
var heightpantalla = Ti.Platform.displayCaps.platformHeight;
var formulaWidth = widthpantalla / 320;
var formulaHeight = heightpantalla / 480;

function createFitxaPare(data, imatgeDefecte, urlBase, rightView, tipus, viewEscenari) {

	var fitxaPare = Ti.UI.createView({
		
		height :isAndroid ? 480 * formulaHeight-40*formulaHeight-statusBarHeight : 480 * formulaHeight-40*formulaHeight-statusBarHeight ,
		left : 35 * formulaWidth,
		zIndex : 3,
		backgroundColor:'#4d4d4d',
		top:statusBarHeight+20*formulaHeight,
		borderRadius : 5,
		borderColor : Ti.App.colorPrincipal,
		borderWidth : 5,
		//opacity : 0.5,
		
		width : 250 * formulaWidth
	});

	// var imatgeView = Ti.UI.createView({
		// backgroundColor:'black',
		// // top : isAndroid ? 0 * formulaHeight : 0,
		// //image : '/images/defectotiempo.png',
		// width:250*formulaWidth,
		// height:200*formulaHeight,
		// left : 0 * formulaWidth,
		// top:0,
	// });
	// fitxaPare.add(imatgeView);
	
	var imatge = Ti.UI.createImageView({
		width:250*formulaWidth,
		height:200*formulaHeight,
		 top : isAndroid ? 0 * formulaHeight : 0,
		//image : '/images/defectotiempo.png',
		 left : 0 * formulaWidth,
		// top:0,
	});
	fitxaPare.add(imatge);


	if (data.foto != null && tipus == 'maquina') {
		imatge.image = Ti.App.escenariDescarregat ?data.foto:urlBase+'/'+data.foto;
	} else if (data.pathfoto != null && tipus == 'jocpista') {


imatge.image =Ti.App.escenariDescarregat ?data.pathfoto: urlBase+'/'+data.pathfoto;		
	} else {
		imatge.image = imatgeDefecte;
	}

var labelTitol = Ti.UI.createLabel({
		text : data.nom + '\n',
		top : 205*formulaHeight,
		left : 10 * formulaWidth,
		height : 30*formulaHeight,
		color : 'white',
		textAlign : 'center',
		width : 230 * formulaWidth,
		font : {
			fontWeight : 'bold',
			fontSize : 14+Ti.App.paddingLabels+'sp' 
		}
	});
	fitxaPare.add(labelTitol);


	var descripcio= ' ';

	if (tipus == 'trivial') {
		if(data.descripcion!=null){
			descripcio = data.descripcion;
		}
		else{
			 labelTitol.text = data.nom + '\n\n';
			
		}
		
	} else {
		if(data.descripcio!=null&&data.descripcio!=''){
			descripcio = data.descripcio;
		}
		else{
			
			labelTitol.text = data.nom+ '\n\n';
		}
		
		
	}
	

	
		var scrollViewDescripcio = Ti.UI.createScrollView({
			width : 250 * formulaWidth,
			// height : isAndroid ? 95 * formulaHeight : 166* formulaHeight,
			top : 240*formulaHeight,
			height:Ti.UI.FILL,
			backgroundColor:'#4d4d4d',
			//contentWidth:270,
		
			left : 0 * formulaWidth,
			contentHeight : Ti.UI.SIZE
		});
		fitxaPare.add(scrollViewDescripcio);
		
	var labelDescripcio = Ti.UI.createLabel({
		text : descripcio + '\n\n',
		top : isAndroid ? 0 * formulaHeight : 0,
		left : 10 * formulaWidth,
		height : Ti.UI.SIZE,
		
		color : 'white',
		textAlign : 'left',
		width : 230 * formulaWidth,
		font : {
			fontSize :  14+Ti.App.paddingLabels+'sp' 
		}
	});
		scrollViewDescripcio.add(labelDescripcio);


	

		
	



// if(!isAndroid){
	// botoInici = Ti.UI.createButton({
		// width : 40 * formulaWidth,
		// zIndex : 3,
		// height : isAndroid ? 50 * formulaHeight : 50,
		// right : 0 * formulaWidth,
		// backgroundImage : '/images/entrar.png'
// 
	// });
	// imatge.addEventListener("postlayout", function(e) {
// 		
			// botoInici.top= isAndroid ? fitxaPare.height / 2 - 25 * formulaHeight : fitxaPare.height / 2 - 25;
// 	
		// viewEscenari.add(botoInici);
	// });
// 	
// 
	// botoInici.addEventListener('click', function(e) {
// 
		// if (tipus == 'maquina') {
			// rightView.createFitxaMaquina(data);
		// } else if (tipus == 'trivial') {
			// rightView.createFitxaTrivial(data);
		// } else if (tipus == 'jocpista') {
			// rightView.createFitxaJocPistes(data);
		// }
// 
		// viewEscenari.remove(fitxaPare);
// 
		// rightView.rightView.animate({
			// right : 25,
			// duration : 500
		// });
		// viewEscenari.remove(botoInici);
// 
	// });
// }
// else{
	var viewEntrar = Ti.UI.createView({
		width : 65 * formulaWidth,
		height : isAndroid ? 30 * formulaHeight : 40,
		bottom : 10* formulaHeight,
		borderRadius : 10,
		backgroundColor : Ti.App.colorPrincipal,
		right : 20* formulaWidth
	});
	fitxaPare.add(viewEntrar);

	var labelEntrar = Ti.UI.createLabel({
		text : entrar,
		left : 0 * widthpantalla,

		textAlign : 'center',
		height : isAndroid ? 30 * formulaHeight : 40,
		width :65 * formulaWidth,
		color : 'white',
		top : 1 * formulaHeight,
		font : {
			fontSize :  18+Ti.App.paddingLabels+'sp' ,
			fontFamily : customFont
		}
	});
	viewEntrar.add(labelEntrar);
	
		viewEntrar.addEventListener('click', function(e) {

		if (tipus == 'maquina') {
			rightView.createFitxaMaquina(data);
		} else if (tipus == 'trivial') {
			rightView.createFitxaTrivial(data);
		} else if (tipus == 'jocpista') {
			rightView.createFitxaJocPistes(data);
		}

		viewEscenari.remove(fitxaPare);

		// rightView.rightView.animate({
			// right : 25* formulaWidth,
			// duration : 500
		// });
		rightView.rightView.right=25* formulaWidth;

	});
	
		var viewTransparent = Ti.UI.createView({
		width : 95 * formulaWidth,
		height : isAndroid ? 20 * formulaHeight : 40,
		top : 0* formulaHeight,
		borderRadius : 10,
		backgroundColor : 'transparent',
		right : 20* formulaWidth
	});
	fitxaPare.add(viewTransparent);
// }

if(!isAndroid){
	fitxaPare.top = (480 * formulaHeight - fitxaPare.height) / 2;
}
	
	viewEscenari.add(fitxaPare);

	


	return fitxaPare;

}

module.exports = createFitxaPare;
