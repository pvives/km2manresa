if (Titanium.App.Properties.getString("language", "undefined") == 'ca') {
	Titanium.include('/controller/translations.js');
} else if (Titanium.App.Properties.getString("language", "undefined") == 'es') {
	Titanium.include('/controller/translations_es.js');
} else if (Titanium.App.Properties.getString("language", "undefined") == 'en') {
	Titanium.include('/controller/translations_en.js');
} else if (Titanium.App.Properties.getString("language", "undefined") == 'pt') {
	Titanium.include('/controller/translations_pt.js');
} else if (Titanium.App.Properties.getString("language", "undefined") == 'pt-PT') {
	Titanium.include('/controller/translations_pt.js');
} else if (Titanium.App.Properties.getString("language", "undefined") == 'eus') {
	Titanium.include('/controller/translations_eus.js');
} else {
	Titanium.include('/controller/translations_en.js');
}
var widthpantalla = Ti.Platform.displayCaps.platformWidth;
var heightpantalla = Ti.Platform.displayCaps.platformHeight;
var formulaWidth = widthpantalla / 320;
var formulaHeight = heightpantalla / 480;
var statusBarHeight = isAndroid ? Ti.Platform.displayCaps.logicalDensityFactor * 25 : 20;

if (Titanium.Platform.osname === 'android') {
	var isAndroid = true;
}

function createFitxaJocPistes() {
	var pistaActual = 1;
	var scrollActual = 0;
	var pistaView;
	var respostaView;
	var scrollableViewJocPistes;
	var fitxaJocPistes = Ti.UI.createView({
		visible : false,
		height : isAndroid ? 480 * formulaHeight - statusBarHeight - 20 * formulaHeight : 480 * formulaHeight - statusBarHeight - 20,
		borderRadius : 10,
		bottom:0,
		backgroundColor : '#4d4d4d'
	});

	function refreshJocPistes(dataJocPistes, urlBase) {

		var viewTabs;
		var arrayPistesMetes = [];
		var arrayPistesResoltes = [];

		if (scrollableViewJocPistes != null) {
			fitxaJocPistes.remove(scrollableViewJocPistes);
		}

		for (var i = 0; i < dataJocPistes.pistes.length; i++) {
			arrayPistesResoltes[i] = false;
		}

		scrollableViewJocPistes = Ti.UI.createScrollableView({
			top : isAndroid ? 0 * formulaHeight : 0,
			height : isAndroid ? 480 * formulaHeight - statusBarHeight - 20 * formulaHeight : 480 * formulaHeight - statusBarHeight - 20,
			left : 0 * formulaWidth,
			width : 270 * formulaWidth,
			borderRadius : 10,
			views : [],
			scrollingEnabled : false,
			visible : true,
			showPagingControl : false,
			maxZoomScale : 2.0
		});

		fitxaJocPistes.add(scrollableViewJocPistes);

		var numPistaView = Ti.UI.createView({
			width : 270 * formulaWidth,
			height : isAndroid ? 44* formulaHeight  : 44,
			zIndex:10,
			bottom : isAndroid ? 0 * formulaHeight : 0,
			backgroundColor : '#1a1a19',
			left : 0 * formulaWidth
		});
		fitxaJocPistes.add(numPistaView);

		var labelPistaActual = Ti.UI.createLabel({
			text : scrollableViewJocPistes.currentPage + 1 + ' / ' + dataJocPistes.pistes.length,
			top : isAndroid ? 0 * formulaHeight : 0,
			left : 0 * formulaWidth,
			color : 'white',
			height : isAndroid ? 44 * formulaHeight : 44,
			textAlign : 'center',
			width : 270 * formulaWidth,
			font : {
				fontSize :  17+Ti.App.paddingLabels+'sp' 
			}
		});
		numPistaView.add(labelPistaActual);

		var fitxaPista = require('controller/objectes/fitxaPista');
		var fitxaPistaInstance = new fitxaPista(dataJocPistes.pistes[scrollActual], urlBase, passoPistaAResposta);

		arrayPistesMetes.push(fitxaPistaInstance);

		var fitxaMeta = require('controller/objectes/fitxaMeta');
		var fitxaMetaInstance = new fitxaMeta(dataJocPistes.pistes[scrollActual], urlBase);

		arrayPistesMetes.push(fitxaMetaInstance);

		scrollableViewJocPistes.views = arrayPistesMetes;

		var fletxaEsquerraPista = Ti.UI.createButton({
			width : 44 * formulaWidth,
			height : isAndroid ? 44 * formulaHeight : 44,
			left : 0 * formulaWidth,
			top : isAndroid ? 0 * formulaHeight : 0,
			backgroundImage : '/images/controlsWeb/anteriorweb.png'

		});
		numPistaView.add(fletxaEsquerraPista);

		fletxaEsquerraPista.addEventListener('click', function(e) {
			if (scrollActual > 0) {

				scrollableViewJocPistes.scrollToView(scrollActual - 1);
				if ((scrollActual % 2) == 0) {
					pistaActual--;
					labelPistaActual.text = pistaActual + ' / ' + dataJocPistes.pistes.length;

				}
				scrollActual--;
			}
		});

		var fletxaDretaPista = Ti.UI.createButton({
			width : 44 * formulaWidth,
			height : isAndroid ? 44 * formulaHeight : 44,
			right : 0 * formulaWidth,
			top : isAndroid ? 0 * formulaHeight : 0,
				backgroundImage : '/images/controlsWeb/siguienteweb.png'

		});
		numPistaView.add(fletxaDretaPista);

		fletxaDretaPista.addEventListener('click', function(e) {
			if (scrollActual < (dataJocPistes.pistes.length) * 2 - 1) {
				
				 if (dataJocPistes.continuar == true || arrayPistesResoltes[pistaActual] == true) {

					

					if ((scrollActual % 2) != 0) {
						if (arrayPistesResoltes[pistaActual + 1] != true) {

							var fitxaPista = require('controller/objectes/fitxaPista');
							var fitxaPistaInstance = new fitxaPista(dataJocPistes.pistes[pistaActual], urlBase, passoPistaAResposta);

							arrayPistesMetes.push(fitxaPistaInstance);

							var fitxaMeta = require('controller/objectes/fitxaMeta');
							var fitxaMetaInstance = new fitxaMeta(dataJocPistes.pistes[pistaActual], urlBase);

							arrayPistesMetes.push(fitxaMetaInstance);
							scrollableViewJocPistes.views = arrayPistesMetes;

						}

						pistaActual++;
						labelPistaActual.text = pistaActual + ' / ' + dataJocPistes.pistes.length;

					}
					scrollActual++;
					scrollableViewJocPistes.scrollToView(scrollActual);

				} else {
					Titanium.UI.createAlertDialog({
						title : pista_arribar
					}).show();
				}
			}
		});

		function passoPistaAResposta() {
		
			if ((scrollActual < (dataJocPistes.pistes.length) * 2 - 1)||scrollActual==0) {
				if ((scrollActual % 2) == 0) {
					scrollActual++;
					arrayPistesResoltes[pistaActual] = true;

					scrollableViewJocPistes.scrollToView(scrollActual);

				}
			
			}
		}

	}


	fitxaJocPistes.refreshJocPistes = refreshJocPistes;

	return fitxaJocPistes;

}

module.exports = createFitxaJocPistes;
