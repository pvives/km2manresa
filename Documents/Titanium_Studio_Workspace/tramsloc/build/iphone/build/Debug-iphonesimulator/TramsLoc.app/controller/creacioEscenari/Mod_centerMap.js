function centerMap(annotations, callback, routes) {
	//calcul del centre i la delta del mapa
	//retorna una region

	var maxLat, maxLon, minLat, minLon, latitudeDelta, longitudeDelta, centerLat, centerLon;
	var totalSumLat = 0;
	var totalSumLon = 0;

	for (var i = 0; i < annotations.length; i++) {
		//definim quins són els valors mínims i màxims de lat i lon
		if ((i == 0) || (annotations[i].latitude > maxLat))
			maxLat = annotations[i].latitude;
		if ((i == 0) || (annotations[i].longitude > maxLon))
			maxLon = annotations[i].longitude;
		if ((i == 0) || (annotations[i].latitude < minLat))
			minLat = annotations[i].latitude;
		if ((i == 0) || (annotations[i].longitude < minLon))
			minLon = annotations[i].longitude;
		//sumem els valors de les latituds i longituds per després poder calcular la mitja
		//i poder saber on tenim el centre del mapa
		totalSumLat += annotations[i].latitude;
		totalSumLon += annotations[i].longitude;
	}

	if (annotations.length != 0) {
		centerLat = parseFloat(totalSumLat) / parseFloat(annotations.length);
		centerLon = parseFloat(totalSumLon) / parseFloat(annotations.length);
	} else {
		centerLat = 0;
		centerLon = 0;
	}
	latitudeDelta = Math.abs(maxLat - minLat);
	longitudeDelta = Math.abs(maxLon - minLon);

	if (latitudeDelta < 0.001) {

		latitudeDelta = 0.05;

	}
	if (longitudeDelta < 0.001) {

		longitudeDelta = 0.05;

	}
	var region = {
		latitude : centerLat,
		longitude : centerLon,
		// animate : true,
		latitudeDelta : latitudeDelta,
		longitudeDelta : longitudeDelta
	};

	//crida a createMapView
	callback(annotations, region, routes);

	return region;

}

module.exports = centerMap;

