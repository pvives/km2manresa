function detallFiltre(fitxaFiltreAvancat, data, taEdatAutor, do_search, taParaulaClau, taPoblacio, taIdioma, taNomEscola, taAutor, labelSeleccio, botoBack, vinc) {

	if (Titanium.App.Properties.getString("language", "undefined") == 'ca') {
		Titanium.include('/controller/translations.js');

	} else if (Titanium.App.Properties.getString("language", "undefined") == 'es') {
		Titanium.include('/controller/translations_es.js');

	} else if (Titanium.App.Properties.getString("language", "undefined") == 'en') {
		Titanium.include('/controller/translations_en.js');

	} else if (Titanium.App.Properties.getString("language", "undefined") == 'pt') {
		Titanium.include('/controller/translations_pt.js');

	} else if (Titanium.App.Properties.getString("language", "undefined") == 'pt-PT') {
		Titanium.include('/controller/translations_pt.js');

	} else if (Titanium.App.Properties.getString("language", "undefined") == 'eus') {
		Titanium.include('/controller/translations_eus.js');

	} else {
		Titanium.include('/controller/translations_en.js');

	}

	var viewDetallFiltre = Ti.UI.createView({
		width : 320 * formulaWidth,
		height : 420 * formulaHeight,
		top : 0 * formulaHeight,
		bubbleParent : false,
		zIndex : 15,
		backgroundColor : 'red',
		backgroundColor : '#ebebeb',
		left : 320 * formulaWidth
	});

	var arrayRows = [];

	for (var i = 0; i < data.length; i++) {

		var row = Ti.UI.createTableViewRow({
			height : isAndroid ? 40 * formulaWidth : 40,
			width : 320 * formulaWidth,

			// rightImage:dataEdatAutor[i]==idioma?'/images/tik.png':'none',
			font : {
				fontSize : 16 + Ti.App.paddingLabels + 'sp' ,
			},

			selectedBackgroundColor : '#dbdbdb',
			backgroundSelectedColor : '#dbdbdb',
			borderRadius : 0,
			color : 'black',
			borderWidth : 0,
			id : data[i].id,
			title : data[i].title
		});
		arrayRows.push(row);
	}

	var table = Ti.UI.createTableView({
		separatorColor : '#c8c7cc',
		color : 'black',
		data : arrayRows,
		hideSearchOnSelection : false,
		selectedBackgroundColor : '#f6f6f6',
		backgroundSelectedColor : '#f6f6f6',
		width : 320 * formulaWidth,
		height : Ti.UI.SIZE,
		zIndex : 10,
		top : isAndroid ? 20 * formulaHeight : 20
	});

	table.addEventListener('click', function(e) {
		fitxaFiltreAvancat.animate({
			left : 0 * formulaWidth,
			duration : 500
		});

		if (vinc == 'areaConeixement') {
			
		
			Ti.App.area_coneixement = e.row.id;
		}
		else if (vinc == 'public') {
			Ti.App.public_objectiu = e.row.id;
		}

		else if (vinc == 'edatAutor') {
			
			Ti.App.edat_autor = e.row.id;
			
		}

		labelSeleccio.text = e.row.title;
		labelBarraSuperior.text = biblioteca;
		menuButton.show();
		botoBack.hide();
		do_search(taParaulaClau.value, taParaulaClau.value, taPoblacio.value, taIdioma.value, taNomEscola.value, taAutor.value);

	});
	viewDetallFiltre.add(table);

	fitxaFiltreAvancat.add(viewDetallFiltre);

	return viewDetallFiltre;
}

module.exports = detallFiltre;
