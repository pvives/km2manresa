// function inici() {

function isiOS7Plus() {
	// iOS-specific test
	if (Titanium.Platform.name == 'iPhone OS') {
		var version = Titanium.Platform.version.split(".");
		var major = parseInt(version[0], 10);

		// Can only test this support on a 3.2+ device
		if (major >= 7) {
			return true;
		}
	}
	return false;
}

var iOS7 = isiOS7Plus();

var llenguaGuardada = Titanium.App.Properties.getString("languageGuardada", "undefined");
if (llenguaGuardada == 'ca') {
	Titanium.App.Properties.setString("language", 'ca');
} else if (llenguaGuardada == 'es') {
	Titanium.App.Properties.setString("language", 'es');
} else if (llenguaGuardada == 'en') {
	Titanium.App.Properties.setString("language", 'en');
} else if (llenguaGuardada == 'pt') {
	Titanium.App.Properties.setString("language", 'pt');
} else if (llenguaGuardada == 'eus') {
	Titanium.App.Properties.setString("language", 'eus');
} else {
	var llengua = Titanium.Locale.getCurrentLanguage();
	Titanium.App.Properties.setString("language", llengua);

}

var urlBase = 'http://dev-diba.itinerarium.cat';
var idEscenari=50;
var url=urlBase+'/mobile/objectes/'+idEscenari+'/ca/geoemotion';
if (Titanium.Platform.osname === 'android') {
	var isAndroid = true;
}

var widthpantalla = Ti.Platform.displayCaps.platformWidth;
var heightpantalla = Ti.Platform.displayCaps.platformHeight;
var formulaWidth = widthpantalla / 320;
var formulaHeight = heightpantalla / 480;
var statusBarHeight = Ti.Platform.displayCaps.logicalDensityFactor * 25;
var loggedIn = Titanium.App.Properties.getString("loggedIn", "false");
var customFont = 'Anna Beta';

if (Ti.Platform.osname == 'android') {
	// on Android, use the "base name" of the file (name without extension)
	customFont = 'AnnaBeta-Regular';
}

if (Titanium.App.Properties.getString("language", "undefined") == 'ca') {
	Titanium.include('/controller/translations.js');
} else if (Titanium.App.Properties.getString("language", "undefined") == 'es') {
	Titanium.include('/controller/translations_es.js');
} else if (Titanium.App.Properties.getString("language", "undefined") == 'en') {
	Titanium.include('/controller/translations_en.js');
} else if (Titanium.App.Properties.getString("language", "undefined") == 'pt') {
	Titanium.include('/controller/translations_pt.js');
} else if (Titanium.App.Properties.getString("language", "undefined") == 'pt-PT') {
	Titanium.include('/controller/translations_pt.js');
} else if (Titanium.App.Properties.getString("language", "undefined") == 'eus') {
	Titanium.include('/controller/translations_eus.js');
} else {
	Titanium.include('/controller/translations_en.js');
}
var win = Titanium.UI.createWindow({
	left : 0,
	top : iOS7 ? 0 : 0,
	 navBarHidden : true,
	urlBase : urlBase,
	idEscenari:idEscenari,
	backgroundColor : '#f0efef'
});
win.open();

var downloaderWindow = require('/controller/offline/DownloaderWindow');
var downloaderWindowInstance = new downloaderWindow(carregant);
downloaderWindowInstance.open();
var data = require('/controller/creacioEscenari/Mod_networkController');
var dataInstance = new data(url, obreEscenari,downloaderWindowInstance);

function obreEscenari(data) {

	var escenariController = require('/controller/creacioEscenari/escenariController');
	new escenariController(win, data);
}
