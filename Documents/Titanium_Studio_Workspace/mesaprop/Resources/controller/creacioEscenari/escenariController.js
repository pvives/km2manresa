function escenariController(win, data) {

	if (Titanium.Platform.osname === 'android') {
		var isAndroid = true;
	}

	function isiOS7Plus() {
		// iOS-specific test
		if (Titanium.Platform.name == 'iPhone OS') {
			var version = Titanium.Platform.version.split(".");
			var major = parseInt(version[0], 10);

			// Can only test this support on a 3.2+ device
			if (major >= 7) {
				return true;
			}
		}
		return false;
	}

	var iOS7 = isiOS7Plus();

	var widthpantalla = Ti.Platform.displayCaps.platformWidth;
	var heightpantalla = Ti.Platform.displayCaps.platformHeight;
	var formulaWidth = widthpantalla / 320;
	var formulaHeight = heightpantalla / 480;
	var statusBarHeight = Ti.Platform.displayCaps.logicalDensityFactor * 25;

	var urlBase = win.urlBase;

	var viewEscenari = Ti.UI.createView({
		width : 320 * formulaWidth,
		height : isAndroid ? 480 * formulaHeight - statusBarHeight : 480 * formulaHeight - statusBarHeight,
		top : iOS7 ? 20 : 0 * formulaHeight,
		backgroundColor : '#3e3d40',
		left : 0 * formulaWidth
	});
	win.add(viewEscenari);

	var annotations = require('/controller/creacioEscenari/Mod_createAnnotations');
	new annotations(data, centerMap);

	function centerMap(annotations) {

		var region = require('/controller/creacioEscenari/Mod_centerMap');
		new region(annotations, createFitxaObjectes);

	}

	function createFitxaObjectes(annotations, region) {

		var fitxaObjectes = require('/controller/creacioEscenari/Mod_fitxaObjectes');
		new fitxaObjectes(annotations, region, urlBase, createMapView, viewEscenari, win);

	}

	function createMapView(annotations, region, rightView) {

		var mapview = require('/controller/creacioEscenari/Mod_createMap');
		new mapview(annotations, region, rightView, createWindow, viewEscenari, urlBase);

	}

	function createWindow(mapView, rightView, viewTornarMapa, showFitxaIncidencia) {

		var topBarMapa = Ti.UI.createView({
			width : 320 * formulaWidth,
			height : isAndroid ? 44 * formulaHeight : 44,
			left : 0 * formulaWidth,
			top : 0 * formulaHeight,
			// backgroundColor : '#353535'
			backgroundImage : '/images/topbar.png'
		});

		viewEscenari.add(topBarMapa);

		var labelBarraSuperiorMapa = Ti.UI.createLabel({
			text : 'Diba+Aprop',
			left : 44 * formulaWidth,

			textAlign : 'center',
			height : isAndroid ? 44 * formulaHeight : 44,
			width : 235 * formulaWidth,
			color : '#3e3e3e',
			top : 0 * formulaHeight,
			font : {
				fontSize : '16sp',
				fontWeight : 'bold',
				fontFamily : 'Helvetica'
			}
		});
		topBarMapa.add(labelBarraSuperiorMapa);
		if (!isAndroid) {
			labelBarraSuperiorMapa.setShadow({
				// shadowRadius:10,
				shadowColor : '#3e3e3e',
				shadowOffset : {
					x : 10,
					y : 10
				}
			});
		}


		var satmap = require('/controller/creacioEscenari/satmap');
		new satmap(viewEscenari, mapView);

		var filtre = require('/controller/creacioEscenari/filtreAnnotations');
		var filtreInstance = new filtre(mapView, viewEscenari, data);

		viewEscenari.add(mapView);
		viewEscenari.add(rightView.rightView);
		viewEscenari.add(viewTornarMapa);

		win.open();

	}

}

module.exports = escenariController;
