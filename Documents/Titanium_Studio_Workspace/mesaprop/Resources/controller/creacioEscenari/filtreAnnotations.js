function filtre(mapView, viewEscenari, data) {
	if (Titanium.Platform.osname === 'android') {
		var isAndroid = true;
	}
	Ti.App.ArrayCapesCategories = [];
	var widthpantalla = Ti.Platform.displayCaps.platformWidth;
	var heightpantalla = Ti.Platform.displayCaps.platformHeight;
	var formulaWidth = widthpantalla / 320;
	var formulaHeight = heightpantalla / 480;
	var statusBarHeight = Ti.Platform.displayCaps.logicalDensityFactor * 25;
	var table;
	var rowCategoria;
	var filtreObert = false;
	var row;
	var viewOK;
	var arrayRows = [];
	var rowCategoriaNova;

	firstTime = false;
	var dataFiltre = require('/controller/creacioEscenari/filtreNetworkController');
	var dataFiltreInstance = new dataFiltre(win.urlBase + '/mobile/grups/' + win.idEscenari, creaFiltre);

	function creaFiltre(data) {

		arrayRows = [];

		for (var i = 0; i < data.length; i++) {

			var viewSwitch = Ti.UI.createView({
				width : 68 * formulaWidth,
				height : isAndroid ? 45 * formulaHeight : 45,
				right : 10 * formulaWidth,
				activat : false,
				clicat : false,
				bubbleParent : true,
				top : 0 * formulaHeight,
				// backgroundColor : '#353535'
				backgroundImage : '/images/ondes.png'
			});

			viewSwitch.addEventListener('click', function(e) {

				e.source.clicat = true;
				if (e.source.activat) {
					e.source.activat = false;

					e.source.backgroundImage = '/images/ondes.png';
				} else {
					e.source.activat = true;

					e.source.backgroundImage = '/images/on.png';
				}

			});
			var masMenosView = Ti.UI.createView({
				width : 44 * formulaWidth,
				height : isAndroid ? 44 * formulaHeight : 44,
				left : 10 * formulaWidth,

				bubbleParent : true,
				top : 0 * formulaHeight,
				// backgroundColor : '#353535'
				backgroundImage : '/images/mas.png'
			});

			var labelNomCapa = Ti.UI.createLabel({
				text : data[i].capa_nom,
				top : isAndroid ? 0 * formulaHeight : 0,
				left : 44 * formulaWidth + 10 * formulaWidth,
				color : '#2f3e47',
				height : isAndroid ? 44 * formulaHeight : 44,
				width : 270 * formulaWidth - 44 * formulaWidth - 44 * formulaWidth,
				font : {
					fontSize : '17sp',
					fontWeight : 'bold'
				}
			});

			row = Ti.UI.createTableViewRow({
				height : isAndroid ? 44 * formulaWidth : 44,
				width : 320 * formulaWidth,
				borderRadius : 0,
				masMenosView : masMenosView,
				bubbleParent : true,
				primeraVegada : true,
				plegat : true,
				rowsCategories : [],
				viewSwitch : viewSwitch,
				selectedBackgroundColor : '#dedede',
				numRow : i,
				activat : false,
				font : {
					fontSize : '16sp'
				},

				numCategories : data[i].categories.length,
				isCategoria : false,
				color : '#2f3e47',
				// leftImage : '/images/mas.png',
				categories : data[i].categories,
				titol : data[i].capa_nom,
				// title : data[i].capa_nom,
				bubbleParent : true,
				borderWidth : 0,
				className : 'default'

			});

			row.add(masMenosView)
			row.add(viewSwitch)
			row.add(labelNomCapa);
			arrayRows.push(row);

			row.addEventListener('click', function(e) {
				var rowsCategoriesGuardades = [];
				var rowsCategoriesGuardadesNoves = [];
				if (e.row.plegat == true) {
					if (!e.row.viewSwitch.clicat || e.row.viewSwitch.activat) {
						if (e.row.primeraVegada) {
							e.row.primeraVegada = false;
							e.row.plegat = false;
							e.row.activat = true;
							e.row.masMenosView.backgroundImage = '/images/menos.png';
							for (var j = 0; j < e.row.numCategories; j++) {

								e.row.primeraVegada = false;
								var labelNomCategoria = Ti.UI.createLabel({
									text : e.row.categories[j].nom,
									top : isAndroid ? 0 * formulaHeight : 0,
									bubbleParent:true,
									left : 44 * formulaWidth + 10 * formulaWidth,
									color : '#2f3e47',
									height : isAndroid ? 44 * formulaHeight : 44,
									width : 270 * formulaWidth - 44 * formulaWidth - 44 * formulaWidth,
									font : {
										fontSize : '15sp'
										
									}
								});
								rowCategoria = Ti.UI.createTableViewRow({
									height : isAndroid ? 44 * formulaWidth : 44,
									width : 320 * formulaWidth,
									borderRadius : 1,
									rowCapa : row,
									selectedBackgroundColor : '#dedede',
									borderColor : '#999999',
									color : '#2f3e47',
									font : {
										fontSize : '14sp'
									},
									leftImage : '/images/transparent.png',
									rightImage : e.row.viewSwitch.activat ? '/images/ok.png' : 'none',
									isCategoria : true,
									categoriaActivada : true,
									titol : e.row.categories[j].nom,
									bubbleParent : true,
									borderWidth : 1,
									className : 'default'

								});
								
								rowCategoria.add(labelNomCategoria);
								// viewOK = Ti.UI.createView({
								// width : 44 * formulaWidth,
								// height : isAndroid ? 26 * formulaHeight : 44,
								// right : 10 * formulaWidth,
								// top : 0 * formulaHeight,
								// categoriaActivada:true,
								// // backgroundColor : '#353535'
								// backgroundImage : e.row.viewSwitch.activat ? '/images/ok.png' : 'none'
								// });
								// rowCategoria.add(viewOK);

								rowCategoria.addEventListener('click', function(f) {
									if (e.row.viewSwitch.activat) {
										if (e.row.categoriaActivada) {

											f.row.categoriaActivada = false;
											f.row.rightImage = '/images/okdes.png';
										} else {
											f.row.categoriaActivada = true;
											f.row.rightImage = '/images/ok.png';
										}
									}

								});
								rowsCategoriesGuardades.push(rowCategoria);

								table.insertRowAfter(e.index, rowCategoria, true);

							}

							e.row.rowsCategories = rowsCategoriesGuardades;
						} else {
							e.row.plegat = false;
							e.row.masMenosView.backgroundImage = '/images/menos.png';
							e.row.activat = true;
							for (var j = 0; j < e.row.numCategories; j++) {
								var labelNomCategoria = Ti.UI.createLabel({
									text : e.row.categories[j].nom,
									top : isAndroid ? 0 * formulaHeight : 0,
									bubbleParent:true,
									left : 44 * formulaWidth + 10 * formulaWidth,
									color : '#2f3e47',
									height : isAndroid ? 44 * formulaHeight : 44,
									width : 270 * formulaWidth - 44 * formulaWidth - 44 * formulaWidth,
									font : {
										fontSize : '15sp'
									}
								});

								var rowCategoriaNova = Ti.UI.createTableViewRow({
									height : isAndroid ? 44 * formulaWidth : 44,
									width : 320 * formulaWidth,
									borderRadius : 1,
									rowCapa : row,
									selectedBackgroundColor : '#dedede',
									borderColor : '#999999',
									color : '#2f3e47',
									leftImage :'/images/transparent.png',
									font : {
										fontSize : '16sp'
									},
									rightImage : e.row.rowsCategories[j].categoriaActivada ? '/images/ok.png' : '/images/okdes.png',
									isCategoria : true,
									categoriaActivada : e.row.rowsCategories[j].categoriaActivada,
									titol : e.row.categories[j].nom,
									bubbleParent : true,
									borderWidth : 1,
									className : 'default'

								});
								rowCategoriaNova.add(labelNomCategoria);
								if (!e.row.viewSwitch.activat) {
									rowCategoriaNova.rightImage = 'none';
								}
								rowCategoriaNova.addEventListener('click', function(g) {
									if (e.row.viewSwitch.activat) {
										if (g.source.categoriaActivada) {
											g.source.categoriaActivada = false;
											g.source.rightImage = '/images/okdes.png';
										} else {
											g.source.categoriaActivada = true;
											g.source.rightImage = '/images/ok.png';
										}
									}
								});
								rowsCategoriesGuardadesNoves.push(rowCategoriaNova);
								table.insertRowAfter(e.index, rowCategoriaNova, true);
							}
							e.row.rowsCategories = rowsCategoriesGuardadesNoves;
						}
					}
				} else {

					e.row.plegat = true;
					e.row.masMenosView.backgroundImage = '/images/mas.png';
					for (var z = 0; z < e.row.numCategories; z++) {

						table.deleteRow(e.index + 1, true);
					}
				}

				//inici prova
				if (!e.row.viewSwitch.activat) {
					e.row.activat = false;
				} else {
					e.row.activat = true;
				}
				//final prova
				e.row.viewSwitch.clicat = false;
			});
		}
		table = Ti.UI.createTableView({
			separatorColor : '#999999',
			left : isAndroid ? 10 * formulaWidth : 10,
			data : arrayRows,
			borderColor : '#999999',
			visible : false,
			width : isAndroid ? 300 * formulaWidth : 300,
			height : isAndroid ? 480 * formulaHeight - 44 * formulaHeight - statusBarHeight - 20 * formulaHeight : 480 * formulaHeight - 44 - 20 - 20,
			top : isAndroid ? 54 * formulaHeight : 44 + 10,
			zIndex : 10,
			left : isAndroid ? 10 * formulaWidth : 10,
			backgroundColor : 'white'
		});
		viewEscenari.add(table);

	}

	var filtreButton = Ti.UI.createButton({
		width : isAndroid ? 44 * formulaWidth : 44,
		height : isAndroid ? 44 * formulaHeight : 44,
		top : 0 * formulaHeight,
		zIndex : 10,
		backgroundImage : '/images/filtre.png',
		right : 5 * formulaWidth
	});
	viewEscenari.add(filtreButton);

	var fletxaPlegarDesplegar = Ti.UI.createView({
		width : 44 * formulaWidth,
		height : isAndroid ? 16 * formulaHeight : 16,
		right : 0 * formulaWidth,
		zIndex : 5,
		visible : false,
		bubbleParent : true,
		top : 0 * formulaHeight,

		backgroundImage : '/images/flecha_desplegar.png'

	});
	viewEscenari.add(fletxaPlegarDesplegar);

	filtreButton.addEventListener('click', function(e) {

		if (filtreObert) {
			table.hide();
			filtreObert = false;

			//si totes les capes estan desactivades no hi ha filtre i mostro totes les anotacions
			var totesCapesDesactivades = true;
			var trobat;
			for (var j = 0; j < arrayRows.length; j++) {
				if (arrayRows[j].activat == true && !trobat) {
					trobat = true;
					totesCapesDesactivades = false;
				}
			}
			if (totesCapesDesactivades == false) {
				mapView.removeAllAnnotations();

				var annotations = require('/controller/creacioEscenari/createAnnotationsFiltre');
				new annotations(data, afegirAnotacions, arrayRows);

				function afegirAnotacions(annotations) {
					mapView.addAnnotations(annotations);
				}

			} else {
				mapView.removeAllAnnotations();
				var annotations = require('/controller/creacioEscenari/Mod_createAnnotations');
				new annotations(data, afegirAnotacions);
				function afegirAnotacions(annotations) {
					mapView.addAnnotations(annotations);
				}

			}

		} else {
			filtreObert = true;
			table.show();
		}

	});

}

module.exports = filtre;
