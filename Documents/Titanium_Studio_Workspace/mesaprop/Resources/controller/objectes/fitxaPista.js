if (Titanium.App.Properties.getString("language", "undefined") == 'ca') {
	Titanium.include('/controller/translations.js');
} else if (Titanium.App.Properties.getString("language", "undefined") == 'es') {
	Titanium.include('/controller/translations_es.js');
} else if (Titanium.App.Properties.getString("language", "undefined") == 'en') {
	Titanium.include('/controller/translations_en.js');
} else if (Titanium.App.Properties.getString("language", "undefined") == 'pt') {
	Titanium.include('/controller/translations_pt.js');
} else if (Titanium.App.Properties.getString("language", "undefined") == 'pt-PT') {
	Titanium.include('/controller/translations_pt.js');
} else if (Titanium.App.Properties.getString("language", "undefined") == 'eus') {
	Titanium.include('/controller/translations_eus.js');
} else {
	Titanium.include('/controller/translations_en.js');
}

var widthpantalla = Ti.Platform.displayCaps.platformWidth;
var heightpantalla = Ti.Platform.displayCaps.platformHeight;
var formulaWidth = widthpantalla / 320;
var formulaHeight = heightpantalla / 480;
var statusBarHeight = isAndroid ? Ti.Platform.displayCaps.logicalDensityFactor * 25 : 20;

if (Titanium.Platform.osname === 'android') {
	var isAndroid = true;
}

function createfitxaPista(dataPista, urlBase, passoPistaAResposta) {

	var fitxaInfoInstance;
	var fitxaAudioInstance;
	var fitxaVideoInstance;
	var fitxaWebInstance;
	var numTabs = 0;

	var fitxaPistaView = Ti.UI.createView({
		width : 270 * formulaWidth,
		top : isAndroid ? 0 * formulaHeight : 0,
		left : 0 * formulaWidth,

		height : isAndroid ? 480 * formulaHeight - statusBarHeight - 20* formulaHeight : 480 * formulaHeight - 20 - 20
	});
	if (dataPista != null) {
		var barraSuperior = Ti.UI.createView({
			width : 270 * formulaWidth,
			height : isAndroid ? 44 * formulaHeight : 44,
			top : isAndroid ? 0 * formulaHeight : 0,
			zIndex : 5,
			backgroundColor : '#f0efef',
			right : 0 * formulaWidth
		});
		fitxaPistaView.add(barraSuperior);

		if (dataPista.nom != null && dataPista.nom != 'undefined') {
			var labelTitolPista = Ti.UI.createLabel({
				text : dataPista.nom,
				top : isAndroid ? 0 * formulaHeight : 0,
				left : 44 * formulaWidth,
				color : 'black',
				height : isAndroid ? 44 * formulaHeight : 44,
				textAlign : 'center',
		width : 270 * formulaWidth-44 * formulaWidth-44 * formulaWidth,
				font : {
					fontSize : '17sp'
				}
			});
			barraSuperior.add(labelTitolPista);
		}

		var fitxaBruixola = require('controller/objectes/Mod_bruixola');
		var fitxaBruixolaInstance = new fitxaBruixola(dataPista, urlBase, passoPistaAResposta);
		fitxaPistaView.add(fitxaBruixolaInstance);

		if (dataPista.descripcio != null) {
			//creo fitxa info
			var scrollViewDescripcio = Ti.UI.createScrollView({
				width : 270 * formulaWidth,
				backgroundColor : '#4d4d4d',
				height : isAndroid ? 480 * formulaHeight - statusBarHeight - 44 * formulaHeight - 44 * formulaHeight - 220 * formulaHeight - 44 * formulaHeight : 480 * formulaHeight - 44 - 220 - 44-5,
				top : isAndroid ? 220 * formulaHeight :  220 * formulaHeight,
				left : 0 * formulaWidth,
				contentHeight : Ti.UI.SIZE
			});

			var labelDescripcio = Ti.UI.createLabel({
				text : dataPista.descripcio,
				top : isAndroid ? 0 * formulaHeight : 10,
				left : 10 * formulaWidth,
				height : Ti.UI.SIZE,
				color : 'white',
				textAlign : 'left',
				width : 250 * formulaWidth,
				font : {
					fontSize : '14sp'
				}
			});

			scrollViewDescripcio.add(labelDescripcio);
			fitxaPistaView.add(scrollViewDescripcio);
			numTabs++;
		}
var scrollableGalleryView;
		//galeria imatges
		var galleryImageViews = [];
		if (dataPista.galeria != '') {

			if (dataPista.galeria.imatges != null && dataPista.galeria.imatges != '') {
				scrollableGalleryView = Ti.UI.createScrollableView({
					top : isAndroid ? 220 * formulaHeight  : 220* formulaHeight - 44,
					height : isAndroid ? 220 * formulaHeight-44* formulaHeight : 220* formulaHeight-44 ,
					width : 270 * formulaWidth,
					views : [],
					backgroundColor : '#4d4d4d',
					visible : false,
					showPagingControl : true,
					maxZoomScale : 2.0
				});
				if (dataPista.galeria.imatges.length != 0) {

					for (var i = 0; i < dataPista.galeria.imatges.length; i++) {

						var view = Ti.UI.createImageView({
						backgroundColor : '#4d4d4d',
							
							image :Ti.App.escenariDescarregat ? dataPista.galeria.imatges[i].pathImatge: urlBase+dataPista.galeria.imatges[i].thumb176x270
						});
					// view.addEventListener("load", function(e) {
// 						
						// if (e.source.size.width >= e.source.size.height) {
							// view.width = isAndroid ? 270 * formulaWidth : 270;
// 
						// } else {
							// view.height = isAndroid ? 220 * formulaHeight : 220;
// 
						// }
// 		
					// });
					
						galleryImageViews[i] = view;

					}
					scrollableGalleryView.views = galleryImageViews;

					numTabs++;
				} else {
					var view = Ti.UI.createImageView({
						width : 270 * formulaWidth,
						height : isAndroid ? 220 * formulaHeight : 220,
						image : '/images/defectopunto.png'
					});

					galleryImageViews[0] = view;

					scrollableGalleryView.views = scrollableGalleryView;

				}
				fitxaPistaView.add(scrollableGalleryView);
			}
		}

		if (dataPista.galeria != '') {
			//Creo fitxa audio
			if (dataPista.galeria.pathaudio != null && dataPista.galeria.pathaudio != '') {

				var fitxaAudio = require('/controller/fitxesObjectes/fitxaAudio');
				fitxaAudioInstance = new fitxaAudio(dataPista, urlBase, 'pista');
				fitxaPistaView.add(fitxaAudioInstance);
				numTabs++;
			}
		}

		if (dataPista.galeria != '') {
			//Creo fitxa video
			if (dataPista.galeria.pathvideo != null && dataPista.galeria.pathvideo != '') {

				var fitxaVideo = require('/controller/fitxesObjectes/fitxaVideo');
				fitxaVideoInstance = new fitxaVideo(dataPista, urlBase);
				fitxaPistaView.add(fitxaVideoInstance);
				numTabs++;
			}
		}

		if (dataPista.galeria != '') {
			//Creo fitxa web
			if (dataPista.galeria.urlGenerica != null && dataPista.galeria.urlGenerica != '') {

				var fitxaWeb = require('/controller/fitxesObjectes/fitxaWeb');
				fitxaWebInstance = new fitxaWeb(dataPista, urlBase);
				fitxaPistaView.add(fitxaWebInstance);
				numTabs++;
			}
		}
		if (numTabs == 1) {
			scrollViewDescripcio.height = isAndroid ? 480 * formulaHeight - statusBarHeight - 44 * formulaHeight - 220 * formulaHeight : 480 * formulaHeight - 220 - 44;
			scrollViewDescripcio.bottom = isAndroid ? 44 * formulaHeight : 44;
		}

		//Creo tabs
		if (numTabs != 1) {
			var viewtabs = require('/controller/objectes/mod_tabs');
			var viewTabsInstance = new viewtabs('/images/tabs/infosi.png', scrollViewDescripcio, '/images/tabs/fotosi.png', scrollableGalleryView, '/images/tabs/audiosi.png', fitxaAudioInstance, '/images/tabs/videosi.png', fitxaVideoInstance, '/images/tabs/linksi.png', fitxaWebInstance, numTabs, 'jocpistes');

			fitxaPistaView.add(viewTabsInstance);
		}
	}
	return fitxaPistaView;

}

module.exports = createfitxaPista;
