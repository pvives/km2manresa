if (Titanium.App.Properties.getString("language", "undefined") == 'ca') {
	Titanium.include('/controller/translations.js');
} else if (Titanium.App.Properties.getString("language", "undefined") == 'es') {
	Titanium.include('/controller/translations_es.js');
} else if (Titanium.App.Properties.getString("language", "undefined") == 'en') {
	Titanium.include('/controller/translations_en.js');
} else if (Titanium.App.Properties.getString("language", "undefined") == 'pt') {
	Titanium.include('/controller/translations_pt.js');
} else if (Titanium.App.Properties.getString("language", "undefined") == 'pt-PT') {
	Titanium.include('/controller/translations_pt.js');
} else if (Titanium.App.Properties.getString("language", "undefined") == 'eus') {
	Titanium.include('/controller/translations_eus.js');
} else {
	Titanium.include('/controller/translations_en.js');
}

var widthpantalla = Ti.Platform.displayCaps.platformWidth;
var heightpantalla = Ti.Platform.displayCaps.platformHeight;
var formulaWidth = widthpantalla / 320;
var formulaHeight = heightpantalla / 480;
var statusBarHeight = isAndroid ? Ti.Platform.displayCaps.logicalDensityFactor * 25 : 20;
if (Titanium.Platform.osname === 'android') {
	var isAndroid = true;
}

function createFitxaPunt(urlBase) {

	//creo fitxa base i barra superior
	var fitxaPunt = Ti.UI.createView({
		width : 270 * formulaWidth,
		visible : false,
	zIndex:10,
		borderRadius : 10,
		top : 0,
		height : isAndroid ? 480 * formulaHeight - statusBarHeight - 20 * formulaHeight : 480 * formulaHeight - statusBarHeight - 20,
		backgroundColor : '#4d4d4d'
	});
	var fitxaPuntView = null;
	var galleryImageViews = [];
	var scrollableGalleryView;
	var barraSuperior = Ti.UI.createView({
		width : 270 * formulaWidth,
		height : isAndroid ? 44 * formulaHeight : 44,
		top : isAndroid ? 0 * formulaHeight : 0,
		backgroundColor : '#f0efef',
		right : 0 * formulaWidth
	});
	fitxaPunt.add(barraSuperior);

	var labelTitolPunt = Ti.UI.createLabel({
		//text : dataPunt.nom,
		top : isAndroid ? 0 * formulaHeight : 0,
		left : 44 * formulaWidth,
		color : 'black',
		height : isAndroid ? 44 * formulaHeight : 44,
		textAlign : 'center',
		width : 270 * formulaWidth-44 * formulaWidth-44 * formulaWidth,
		font : {
			fontSize : '17sp'
		}
	});
	barraSuperior.add(labelTitolPunt);

	var scrollableGalleryView = Ti.UI.createScrollableView({
		top : isAndroid ? 0 * formulaHeight : 0,
		height : isAndroid ? 220 * formulaHeight : 220,
		width : 270 * formulaWidth,
		views : [],
		showPagingControl : true,
		maxZoomScale : 2.0
	});

	function createPunt(dataPunt, urlBase) {



		var fitxaInfoInstance;
		var fitxaAudioInstance;
		var fitxaVideoInstance;
		var fitxaWebInstance;
		var numTabs = 0;

		//esborro la fitxa del punt anterior i afegeixo contingut a la nova fitxa cada vegada que clico una anotacio
		if (fitxaPuntView != null) {

			fitxaPunt.remove(fitxaPuntView);
		}

		fitxaPuntView = null;

		fitxaPuntView = Ti.UI.createView({
			width : 270 * formulaWidth,
			top : isAndroid ? 0 * formulaHeight : 0,
			left : 0 * formulaWidth,
			height : isAndroid ? 480 * formulaHeight - statusBarHeight - 20 * formulaHeight : 480 * formulaHeight - statusBarHeight - 20,
		});

		fitxaPunt.add(fitxaPuntView);

		labelTitolPunt.text = dataPunt.nom;

		scrollableGalleryView = null;
		scrollableGalleryView = Ti.UI.createScrollableView({
			top : isAndroid ? 44 * formulaHeight : 44,
			height : isAndroid ? 220 * formulaHeight : 220,
			width : 270 * formulaWidth,
			views : [],
backgroundColor :'#4d4d4d',
			showPagingControl : true,
			maxZoomScale : 2.0
		});
		var view;
		//galeria imatges
		galleryImageViews = [];
		if (dataPunt.galeria.imatges != null) {

			if (dataPunt.galeria.imatges.length != 0) {

				for (var i = 0; i < dataPunt.galeria.imatges.length; i++) {

					view = Ti.UI.createImageView({
						backgroundColor : '#4d4d4d',
						i : i,

						image :Ti.App.escenariDescarregat ? dataPunt.galeria.imatges[i].pathImatge: urlBase+dataPunt.galeria.imatges[i].thumb220x270
					});

					// view.addEventListener("load", function(e) {
					//
					// if (e.source.size.width >= e.source.size.height) {
					// view.width = isAndroid ? 270 * formulaWidth : 270;
					//
					// } else {
					// view.height = isAndroid ? 220 * formulaHeight : 220;
					//
					// }
					//
					// });

					galleryImageViews[i] = view;

				}

				scrollableGalleryView.views = galleryImageViews;
				fitxaPuntView.add(scrollableGalleryView);

			} else {
				var view = Ti.UI.createImageView({
					width : 270 * formulaWidth,
					backgroundColor : 'black',
					height : isAndroid ? 220 * formulaHeight : 220,
					image : '/images/defectopunto.png'
				});

				galleryImageViews[0] = view;

				scrollableGalleryView.views = galleryImageViews;

				fitxaPuntView.add(scrollableGalleryView);
			}
		} else {
			var view = Ti.UI.createImageView({
				width : 270 * formulaWidth,
				height : isAndroid ? 220 * formulaHeight : 220,
				image : '/images/defectopunto.png'
			});

			galleryImageViews[0] = view;

			scrollableGalleryView.views = galleryImageViews;

			fitxaPuntView.add(scrollableGalleryView);
		}

		if (dataPunt.galeria.pathaudio != null && dataPunt.galeria.pathaudio != '') {
			//Creo fitxa audio
			var fitxaAudio = require('/controller/fitxesObjectes/fitxaAudio');
			fitxaAudioInstance = new fitxaAudio(dataPunt, urlBase, 'punt');
			fitxaPuntView.add(fitxaAudioInstance);
			numTabs++;
		}

		if (dataPunt.galeria.pathvideo != '' && dataPunt.galeria.pathvideo != null) {
			//Creo fitxa video
			var fitxaVideo = require('/controller/fitxesObjectes/fitxaVideo');
			fitxaVideoInstance = new fitxaVideo(dataPunt, urlBase);
			fitxaPuntView.add(fitxaVideoInstance);
			numTabs++;
		}

		if (dataPunt.galeria.urlGenerica != null && dataPunt.galeria.urlGenerica != '') {
			//Creo fitxa web
			var fitxaWeb = require('/controller/fitxesObjectes/fitxaWeb');
			fitxaWebInstance = new fitxaWeb(dataPunt, urlBase);
			fitxaPuntView.add(fitxaWebInstance);
			numTabs++;
		}
		
		//creo fitxa info
		numTabs++;
		var fitxaInfo = require('/controller/fitxesObjectes/fitxaInfo');
		fitxaInfoInstance = new fitxaInfo(dataPunt, urlBase, 'punt', numTabs);
		fitxaPuntView.add(fitxaInfoInstance);

		if (numTabs != 1) {
			var viewtabs = require('/controller/objectes/mod_tabs');
			var viewTabsInstance = new viewtabs('/images/tabs/infosi.png', fitxaInfoInstance, '/images/tabs/audiosi.png', fitxaAudioInstance, '/images/tabs/videosi.png', fitxaVideoInstance, '/images/tabs/linksi.png', fitxaWebInstance, null, null, numTabs, 'punt',dataPunt);

			fitxaPuntView.add(viewTabsInstance);
		}

	}


	fitxaPunt.createPunt = createPunt;

	return fitxaPunt;

}

module.exports = createFitxaPunt;
