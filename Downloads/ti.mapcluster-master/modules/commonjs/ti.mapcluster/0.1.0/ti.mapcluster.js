function TiMapCluster(a, b) {
	
	this.mapview = Titanium.Map.createView(a);
	this.markers = b || [];
	this.clustered = [];
	this.mapview.TiMapCluster = this;
	this.mapview.addEventListener("regionChanged", this._autoUpdate)
}
TiMapCluster.prototype.setMarkers = function(a) {
	this.markers = a;
	this.mapview.TiMapCluster = this;
	this._update()
};
TiMapCluster.prototype.addMarker = function(a) {
	this.markers.push(a);
	this.mapview.TiMapCluster = this;
	this._update()
};
TiMapCluster.prototype.resize = function(a, b) {
	this.mapview.width = a;
	this.mapview.height = b
};
TiMapCluster.prototype.setPosition = function(a, b) {
	this.mapview.left = a;
	this.mapview.top = b
};
TiMapCluster.prototype._pixelDistance = function(a, b, c, d) {
	var e = this.mapview.actualRegion || this.mapview.region, a = this.mapview.size.height * Math.abs(a - c) / e.latitudeDelta, b = this.mapview.size.width * Math.abs(b - d) / e.longitudeDelta;
	return Math.sqrt(Math.pow(a, 2) + Math.pow(b, 2))
};
TiMapCluster.prototype._cluster = function() {
	var a = [], b = [], c;
	for (c in this.markers)
	b.push(this.markers[c]);
	for (; 0 < b.length; ) {
		for (var d = b.pop(), e = [], f = b.length; -1 < f; f--)
			c = b[f], "undefined" != typeof c && 20 > this._pixelDistance(d.latitude, d.longitude, c.latitude, c.longitude) && (b.splice(f, 1), e.push(c));
		e.push(d);
		d = 10 > e.length ? e.length : 10;
		f = "";
		for ( c = 0; c < e.length; c++)
			f += e[c].title + " ";
		c = this._midPoint(e);
		a.push({
			latitude : c.latitude,
			longitude : c.longitude,
			title : f,
			cluster : e,
			image : "pins/" + d + ".png"
		})
	}
	return this.clustered = a
};
TiMapCluster.prototype._update = function() {
	var a = this._cluster();
	this.mapview.removeAllAnnotations();
	for (var b = 0; b < a.length; b++)
		this.mapview.addAnnotation(Titanium.Map.createAnnotation(a[b]))
};
TiMapCluster.prototype._autoUpdate = function(a) {
	var b = this.TiMapCluster;
	this.actualRegion = {
		latitude : a.latitude,
		longitude : a.longitude,
		latitudeDelta : a.latitudeDelta,
		longitudeDelta : a.longitudeDelta
	};
	b._update()
};
TiMapCluster.prototype._midPoint = function(a) {
	var b = 0, c = 0, d;
	for (d in a)b += a[d].latitude, c += a[d].longitude;
	return {
		latitude : b / a.length,
		longitude : c / a.length
	}
};
module.exports = TiMapCluster; 